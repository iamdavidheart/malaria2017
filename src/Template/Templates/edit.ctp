<section class="main-container">
    <div class="container">
        <div class="row">


        <div class="col-md-8">
            <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'templates']) ?>">Template List<i class="fa fa-list"></i></a>
   
        </div>

        <div class="col-md-12">
                <h3><?= __('EDIT TEMPLATE') ?></h3>





        <div class="col-xs-12  well well-sm">

           <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">SMS VALIDATION</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create($templateSettings); ?>

                        <?= $this->Form->input('method', ['type' => 'hidden', 'value' => 'm-validation']) ?>
                        <?php if($temp == 'reg'){ ?>
                            
                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">First Name</label>
                                     <?php echo $this->Form->input('first_name', [ 
                                        'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Last Name</label>
                                     <?php echo $this->Form->input('last_name', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Facilty Code</label>
                                     <?php echo $this->Form->input('facility_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">User Role</label>
                                     <?php echo $this->Form->input('user_role', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Designation</label>
                                     <?php echo $this->Form->input('designation', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Mobile One</label>
                                     <?php echo $this->Form->input('mobile_one', [ 'class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>

                            </div>

                            <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">Mobile One Invalid</label>
                                     <?php echo $this->Form->input('mobile_one_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Facility Code Invalid</label>
                                     <?php echo $this->Form->input('facility_code_invalid', ['class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Designation Invalid</label>
                                     <?php echo $this->Form->input('designation_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off','label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">User Role Invalid</label>
                                     <?php echo $this->Form->input('user_role_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Mobile Two Invalid</label>
                                     <?php echo $this->Form->input('mobile_two_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off','label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Mobile Three Invalid</label>
                                     <?php echo $this->Form->input('mobile_three_invalid', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Duplicate Name</label>
                                     <?php echo $this->Form->input('duplicate_name', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                            </div>



                        <?php } ?>

                        <?php if($temp == 'ost'){ ?>


                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">User Code</label>
                                     <?php echo $this->Form->input('user_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Report Date</label>
                                     <?php echo $this->Form->input('report_date', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Stock Code</label>
                                     <?php echo $this->Form->input('stock_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">User Code Invalid</label>
                                     <?php echo $this->Form->input('user_code_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                            </div>

                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">User Activation</label>
                                     <?php echo $this->Form->input('user_activation', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Error Report Date</label>
                                     <?php echo $this->Form->input('report_date', [ 'class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Invalid Report Date</label>
                                     <?php echo $this->Form->input('invalid_report_date', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                            </div>

                        <?php } ?>




                        <?php if($temp == 'dmw'){ ?>


                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">User Code</label>
                                     <?php echo $this->Form->input('user_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Report Date</label>
                                     <?php echo $this->Form->input('report_date', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Stock Code</label>
                                     <?php echo $this->Form->input('stock_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">User Code Invalid</label>
                                     <?php echo $this->Form->input('user_code_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                            </div>


                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">User Activation</label>
                                     <?php echo $this->Form->input('user_activation', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Error Report Date</label>
                                     <?php echo $this->Form->input('report_date', [ 'class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Invalid Report Date</label>
                                     <?php echo $this->Form->input('invalid_report_date', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                            </div>


                        <?php } ?>



                        <?php if($temp == 'dth'){ ?>



                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">User Code</label>
                                     <?php echo $this->Form->input('user_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Report Date</label>
                                     <?php echo $this->Form->input('report_date', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div> 
                                <div class="form-group has-error">
                                    <label for="">First Name</label>
                                     <?php echo $this->Form->input('first_name', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Last Name</label>
                                     <?php echo $this->Form->input('last_name', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">User Code Invalid</label>
                                     <?php echo $this->Form->input('user_code_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">User Activation</label>
                                     <?php echo $this->Form->input('user_activation', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                            </div>

                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">Facility Code</label>
                                     <?php echo $this->Form->input('facility_code', [ 'class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Error Report Date</label>
                                     <?php echo $this->Form->input('report_date', [ 'class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Invalid Report Date</label>
                                     <?php echo $this->Form->input('invalid_report_date', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>

                                <div class="form-group has-error">
                                    <label for="">Corresponding Adg</label>
                                     <?php echo $this->Form->input('corresponding_adg', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Duplicate record</label>
                                     <?php echo $this->Form->input('duplicate_name', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                            </div>

                        <?php } ?>

                        <?php if($temp == 'adg'){ ?>


                           <div class="col-md-6" >
                                <div class="form-group has-error">
                                    <label for="">User Code</label>
                                     <?php echo $this->Form->input('user_code', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false ]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Report Date</label>
                                     <?php echo $this->Form->input('report_date', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">First Name</label>
                                     <?php echo $this->Form->input('first_name', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Last Name</label>
                                     <?php echo $this->Form->input('last_name', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>

                                <div class="form-group has-error">
                                    <label for="">Age</label>
                                     <?php echo $this->Form->input('age', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>

                                <div class="form-group has-error">
                                    <label for="">Gender</label>
                                     <?php echo $this->Form->input('gender', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>

                                <div class="form-group has-error">
                                    <label for="">Gender</label>
                                     <?php echo $this->Form->input('mprf_case', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>

                                <div class="form-group has-error">
                                    <label for="">Exam Result</label>
                                     <?php echo $this->Form->input('exam_result', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>



                            </div>                            

                           <div class="col-md-6" >

                                <div class="form-group has-error">
                                    <label for="">Gender Code Invalid</label>
                                     <?php echo $this->Form->input('gender_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>

                                <div class="form-group has-error">
                                    <label for="">MPRF Case Invalid</label>
                                     <?php echo $this->Form->input('mprf_case_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>
                                <div class="form-group has-error">
                                    <label for="">Exam Result Invalid</label>
                                     <?php echo $this->Form->input('exam_result_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>


                                <div class="form-group has-error">
                                    <label for="">User Code Invalid</label>
                                     <?php echo $this->Form->input('user_code_invalid', [ 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                </div>


                                <div class="form-group has-error">
                                    <label for="">User Activation</label>
                                     <?php echo $this->Form->input('user_activation', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>


                                <div class="form-group has-error">
                                    <label for="">Corrensponding ADG</label>
                                     <?php echo $this->Form->input('corresponding_adg', ['class' => 'form-control', 'autocomplete' => 'off',  'label' => false]); ?>
                                </div>


                            </div>   

                        <?php } ?>

                            <div class="col-sm-12 ">
                            <hr>
                            <div class="form-group">
                                
                                    <button type="submit" class="btn btn-default btn-md">Update</button>
                                </div>
                            </div>

                    <?php echo $this->Form->end(); ?>  
                </div>
            </div>

        </div>


        <?php echo $this->Form->create($template); ?>
                 <?= $this->Form->input('method', ['type' => 'hidden', 'value' => 'm-template']) ?>



        <div class="col-xs-12 col-sm-6 well well-sm">
   
            <div class="row  ">
                <div class="col-xs-12">

                    <div class="form-group">
                        <label for="">Code</label>
                         <?php echo $this->Form->input('code', ['id' => 'code', 'disabled' => true, 'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                    </div>
                    
                
                    <div class="form-group">
                        <label for="">Description</label>
                          <?php echo $this->Form->input('description', ['id' => 'description',  'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                    </div>
                    
       
                    <div class="form-group">
                        <label for="">SMS Format</label>
                           <?php echo $this->Form->input('sms_format', ['id' => 'sms_format',  'class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                    </div>
                    

                    <hr>

                    <div class="form-group">
                        <label for="">Reply SMS Format</label>
                           <?php echo $this->Form->input('format', ['id' => 'sms_format','class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                    </div>
                    

                    <div class="form-group">
                        <label for="">Remarks</label>
                          <?php echo $this->Form->input('remarks', ['id' => 'remarks','class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                    </div>



                    <hr>
                    <div class="b-form-group b-form-group-inline">
                        <label class="b-form-horizontal__label" for="create_account_name">SEND ERROR MESSAGE </label>

                        <?php $error_on = ($template->send_error_msg == 1)? 'checked': '' ; ?>
                        <?php $error_off = ($template->send_error_msg == 0)? 'checked': '' ; ?>
                        <label for="radio_invert_off">
                            <input type="radio" class="b-form-radio b-form-radio-invert" value="0" name="send_error_msg" id="radio_invert_off" <?=$error_off?> >
                            <span>OFF</span>
                        </label>
                        <label for="radio_invert_on">
                            <input type="radio" class="b-form-radio b-form-radio-invert" value="1" name="send_error_msg" id="radio_invert_on" <?=$error_on?> >
                            <span>ON</span>
                        </label>
                    </div>
                    <hr>
                    <div class="clearfix"></div>

                        <?php $alert_on = ($template->send_alert_msg == 1)? 'checked': '' ; ?>
                        <?php $alert_off = ($template->send_alert_msg == 0)? 'checked': '' ; ?>

                    <div class="b-form-group b-form-group-inline">
                        <label class="b-form-horizontal__label" for="create_account_name">SEND ALERT MESSAGE</label>
                        <label for="radio_invert_off">
                            <input type="radio" class="b-form-radio b-form-radio-invert" value="0" name="blocked" id="radio_invert_off" <?=$alert_off?>  >
                            <span>OFF</span>
                        </label>
                        <label for="radio_invert_on">
                            <input type="radio" class="b-form-radio b-form-radio-invert" value="1" name="blocked" id="radio_invert_on" <?=$alert_on?> >
                            <span>ON</span>
                        </label>
                    </div>
                     <hr>

                     <div class="form-group">
                        <input type="submit" class="btn btn-default btn-md" value="Update">
                    </div>

                </div>


            </div>
        </div>
        <?php echo $this->Form->end(); ?>  

        <div class="col-xs-12 col-sm-6 well well-sm">
            <div class="b-form-row f-primary-l f-title-big c-secondary">Alert for Administrator</div>
            <hr class="b-hr">

             <?php echo $this->Form->create($template); ?>
                 <?= $this->Form->input('method', ['type' => 'hidden', 'value' => 'm-notifcation']) ?>
            <div class="row b-form-inline b-form-horizontal ">
                <div class="col-xs-12">
       
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Enter User</label>
                            <select  multiple="multiple" name="users[]" class="form-control chosen-select">
                                <?php foreach ($users as $key => $user) { ?>
                                <option value="<?=$user->id?>"> <?=strtoupper($user->full_name).' ('.$user->code.') ['.$user->role->name.']'?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group">
                        <input type="submit" class="btn btn-default btn-md" value="Update">
                    </div>
                </div>
          
            </div>
             <?php echo $this->Form->end(); ?>  


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ALert SMS receiver.</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped dataTable" id="inbox">
                            <thead>
                                <tr>
                                    <th class="text-center">NO</th>
                                    <th class="text-center">CODE</th>
                                    <th class="text-center">NAME</th>
                                    <th class="text-center">ROLE</th>
                                    <th class="actions text-center"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; foreach ($tempNotifcations as $key => $tempNotifcation) { $i++; ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$tempNotifcation->user->code?></td>
                                        <td><?=$tempNotifcation->user->full_name?></td>
                                        <td><?=$tempNotifcation->user->role->name?></td>
                                        <td>
            

          <?= $this->Form->postLink( __('<i class="fa fa-close"></i> Remove'), ['controller' => 'template-notifications', 'action' => 'delete', $tempNotifcation->id, $template->id],
                                    ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Are you sure you want to remove # {0}?', $tempNotifcation->user->full_name)]  ); ?>


                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>


        </div>
       




        </div>

  
        </div>
    </div>
</section>




<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/bootstrap-datepicker/css/bootstrap-datepicker.css'); ?>
    <?= $this->Html->css('../js/plugin/choosen/css/chosenOrange.css'); ?>
<?php $this->end() ?>




<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('../js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
    <?=  $this->Html->script('../js/plugin/choosen/js/chosen.jquery.min.js'); ?>
    <script type="text/javascript">


        (function($){
            $(document).ready(function(){

                var config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : {allow_single_deselect:true},
                    '.chosen-select-no-single' : {disable_search_threshold:10},
                    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                    '.chosen-select-width'     : {width:"95%"}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }


                        //$('.datepicker').datepicker();
        }); // End document ready

        })(this.jQuery);




    </script>
<?php $this->end() ?>


