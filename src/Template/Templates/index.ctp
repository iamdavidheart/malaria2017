
<section class="main-container">
    <div class="container">
        <div class="row">



             <div class="col-md-12">

                <h3><?= __('TEMPLATE LIST') ?></h3>
               <table class="table table-bordered table-striped " id="templates" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                     
                                    <th class="text-center"><?= $this->Paginator->sort('CODE') ?></th>
                                    <th class="text-center"><?= $this->Paginator->sort('DESCRIPTION') ?></th>
                                    <th class="text-center"><?= $this->Paginator->sort('SMS FORMAT') ?></th>
                                    <th class="text-center"><?= $this->Paginator->sort('SEND ERROR') ?></th>
                                    <th class="text-center"><?= $this->Paginator->sort('SEND REPLY') ?></th>
                                    <th class="actions text-center" style="width:100px;"><?= __('ACTIONS') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; foreach($templates as $template){ $i++; ?>
                                    <tr>
                        
                                        <td><?=$template->code?></td>
                                        <td><?=$template->description?></td>
                                        <td><?=$template->sms_format?></td>
                                        <td>
                                            <?php
                                            if($template->send_error_msg == 1){
                                                echo '<label class="label label-success">ON</label>';
                                            }else{
                                                echo '<label class="label label-danger">OFF</label>';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($template->send_alert_msg == 1){
                                                echo '<label class="label label-success">ON</label>';
                                            }else{
                                                echo '<label class="label label-danger">OFF</label>';
                                            }
                                            ?>
                                        </td>

                                        <td class="actions text-center">

                                              <?php 
                                                echo $this->Html->link(
                                                    __('<i class="fa fa-edit"></i> Edit'),
                                                    ['action' => 'edit', $template->id],
                                                    [
                                                        'class' => 'btn btn-gray-transparent btn-sm ',
                                                        'escape' => false
                                                    ]);
                                                ?>



                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                </table>
            </div>

        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#templates').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>
