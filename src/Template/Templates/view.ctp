<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Template'), ['action' => 'edit', $template->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Template'), ['action' => 'delete', $template->id], ['confirm' => __('Are you sure you want to delete # {0}?', $template->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Templates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Template Alerts'), ['controller' => 'TemplateAlerts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template Alert'), ['controller' => 'TemplateAlerts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Template Notifications'), ['controller' => 'TemplateNotifications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template Notification'), ['controller' => 'TemplateNotifications', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Template Replies'), ['controller' => 'TemplateReplies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template Reply'), ['controller' => 'TemplateReplies', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Template Settings'), ['controller' => 'TemplateSettings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template Setting'), ['controller' => 'TemplateSettings', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="templates view large-9 medium-8 columns content">
    <h3><?= h($template->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($template->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($template->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Template Alert') ?></th>
            <td><?= $template->has('template_alert') ? $this->Html->link($template->template_alert->id, ['controller' => 'TemplateAlerts', 'action' => 'view', $template->template_alert->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Globe Syntax') ?></th>
            <td><?= h($template->globe_syntax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Globe Send To') ?></th>
            <td><?= h($template->globe_send_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Globe Status') ?></th>
            <td><?= h($template->globe_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Smart Syntax') ?></th>
            <td><?= h($template->smart_syntax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Smart Send To') ?></th>
            <td><?= h($template->smart_send_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Smart Status') ?></th>
            <td><?= h($template->smart_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sun Syntax') ?></th>
            <td><?= h($template->sun_syntax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sun Send To') ?></th>
            <td><?= h($template->sun_send_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sun Status') ?></th>
            <td><?= h($template->sun_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tm Syntax') ?></th>
            <td><?= h($template->tm_syntax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tm Send To') ?></th>
            <td><?= h($template->tm_send_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tm Status') ?></th>
            <td><?= h($template->tm_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($template->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Send Error Msg') ?></th>
            <td><?= $this->Number->format($template->send_error_msg) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Send Alert Msg') ?></th>
            <td><?= $this->Number->format($template->send_alert_msg) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($template->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Globe Amount') ?></th>
            <td><?= $this->Number->format($template->globe_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Smart Amount') ?></th>
            <td><?= $this->Number->format($template->smart_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sun Amount') ?></th>
            <td><?= $this->Number->format($template->sun_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tm Amount') ?></th>
            <td><?= $this->Number->format($template->tm_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($template->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($template->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Sms Format') ?></h4>
        <?= $this->Text->autoParagraph(h($template->sms_format)); ?>
    </div>
    <div class="row">
        <h4><?= __('Format') ?></h4>
        <?= $this->Text->autoParagraph(h($template->format)); ?>
    </div>
    <div class="row">
        <h4><?= __('Remarks') ?></h4>
        <?= $this->Text->autoParagraph(h($template->remarks)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Template Notifications') ?></h4>
        <?php if (!empty($template->template_notifications)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Template Id') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($template->template_notifications as $templateNotifications): ?>
            <tr>
                <td><?= h($templateNotifications->id) ?></td>
                <td><?= h($templateNotifications->created) ?></td>
                <td><?= h($templateNotifications->user_id) ?></td>
                <td><?= h($templateNotifications->template_id) ?></td>
                <td><?= h($templateNotifications->remarks) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TemplateNotifications', 'action' => 'view', $templateNotifications->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TemplateNotifications', 'action' => 'edit', $templateNotifications->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TemplateNotifications', 'action' => 'delete', $templateNotifications->id], ['confirm' => __('Are you sure you want to delete # {0}?', $templateNotifications->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Template Replies') ?></h4>
        <?php if (!empty($template->template_replies)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Template Id') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Format') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($template->template_replies as $templateReplies): ?>
            <tr>
                <td><?= h($templateReplies->id) ?></td>
                <td><?= h($templateReplies->created) ?></td>
                <td><?= h($templateReplies->modified) ?></td>
                <td><?= h($templateReplies->template_id) ?></td>
                <td><?= h($templateReplies->code) ?></td>
                <td><?= h($templateReplies->description) ?></td>
                <td><?= h($templateReplies->format) ?></td>
                <td><?= h($templateReplies->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TemplateReplies', 'action' => 'view', $templateReplies->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TemplateReplies', 'action' => 'edit', $templateReplies->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TemplateReplies', 'action' => 'delete', $templateReplies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $templateReplies->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Template Settings') ?></h4>
        <?php if (!empty($template->template_settings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Template Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Report Date') ?></th>
                <th scope="col"><?= __('Error Report Date') ?></th>
                <th scope="col"><?= __('Invalid Report Date') ?></th>
                <th scope="col"><?= __('User Code') ?></th>
                <th scope="col"><?= __('User Code Invalid') ?></th>
                <th scope="col"><?= __('User Activation') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Facility Code') ?></th>
                <th scope="col"><?= __('User Role') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Mobile One') ?></th>
                <th scope="col"><?= __('Mobile One Invalid') ?></th>
                <th scope="col"><?= __('Facility Code Invalid') ?></th>
                <th scope="col"><?= __('Designation Invalid') ?></th>
                <th scope="col"><?= __('Mobile Two Invalid') ?></th>
                <th scope="col"><?= __('Mobile Three Invalid') ?></th>
                <th scope="col"><?= __('Duplicate Name') ?></th>
                <th scope="col"><?= __('User Role Invalid') ?></th>
                <th scope="col"><?= __('Stock Code') ?></th>
                <th scope="col"><?= __('Corresponding Adg') ?></th>
                <th scope="col"><?= __('Age') ?></th>
                <th scope="col"><?= __('Gender') ?></th>
                <th scope="col"><?= __('Gender Invalid') ?></th>
                <th scope="col"><?= __('Mprf Case') ?></th>
                <th scope="col"><?= __('Mprf Case Invalid') ?></th>
                <th scope="col"><?= __('Exam Result') ?></th>
                <th scope="col"><?= __('Exam Result Invalid') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($template->template_settings as $templateSettings): ?>
            <tr>
                <td><?= h($templateSettings->id) ?></td>
                <td><?= h($templateSettings->template_id) ?></td>
                <td><?= h($templateSettings->name) ?></td>
                <td><?= h($templateSettings->report_date) ?></td>
                <td><?= h($templateSettings->error_report_date) ?></td>
                <td><?= h($templateSettings->invalid_report_date) ?></td>
                <td><?= h($templateSettings->user_code) ?></td>
                <td><?= h($templateSettings->user_code_invalid) ?></td>
                <td><?= h($templateSettings->user_activation) ?></td>
                <td><?= h($templateSettings->last_name) ?></td>
                <td><?= h($templateSettings->first_name) ?></td>
                <td><?= h($templateSettings->facility_code) ?></td>
                <td><?= h($templateSettings->user_role) ?></td>
                <td><?= h($templateSettings->designation) ?></td>
                <td><?= h($templateSettings->mobile_one) ?></td>
                <td><?= h($templateSettings->mobile_one_invalid) ?></td>
                <td><?= h($templateSettings->facility_code_invalid) ?></td>
                <td><?= h($templateSettings->designation_invalid) ?></td>
                <td><?= h($templateSettings->mobile_two_invalid) ?></td>
                <td><?= h($templateSettings->mobile_three_invalid) ?></td>
                <td><?= h($templateSettings->duplicate_name) ?></td>
                <td><?= h($templateSettings->user_role_invalid) ?></td>
                <td><?= h($templateSettings->stock_code) ?></td>
                <td><?= h($templateSettings->corresponding_adg) ?></td>
                <td><?= h($templateSettings->age) ?></td>
                <td><?= h($templateSettings->gender) ?></td>
                <td><?= h($templateSettings->gender_invalid) ?></td>
                <td><?= h($templateSettings->mprf_case) ?></td>
                <td><?= h($templateSettings->mprf_case_invalid) ?></td>
                <td><?= h($templateSettings->exam_result) ?></td>
                <td><?= h($templateSettings->exam_result_invalid) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TemplateSettings', 'action' => 'view', $templateSettings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TemplateSettings', 'action' => 'edit', $templateSettings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TemplateSettings', 'action' => 'delete', $templateSettings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $templateSettings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
