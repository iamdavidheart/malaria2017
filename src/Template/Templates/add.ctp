<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Templates'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Template Alerts'), ['controller' => 'TemplateAlerts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Template Alert'), ['controller' => 'TemplateAlerts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Template Notifications'), ['controller' => 'TemplateNotifications', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Template Notification'), ['controller' => 'TemplateNotifications', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Template Replies'), ['controller' => 'TemplateReplies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Template Reply'), ['controller' => 'TemplateReplies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Template Settings'), ['controller' => 'TemplateSettings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Template Setting'), ['controller' => 'TemplateSettings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="templates form large-9 medium-8 columns content">
    <?= $this->Form->create($template) ?>
    <fieldset>
        <legend><?= __('Add Template') ?></legend>
        <?php
            echo $this->Form->input('code');
            echo $this->Form->input('description');
            echo $this->Form->input('sms_format');
            echo $this->Form->input('send_error_msg');
            echo $this->Form->input('send_alert_msg');
            echo $this->Form->input('template_alert_id', ['options' => $templateAlerts, 'empty' => true]);
            echo $this->Form->input('format');
            echo $this->Form->input('remarks');
            echo $this->Form->input('status');
            echo $this->Form->input('globe_amount');
            echo $this->Form->input('globe_syntax');
            echo $this->Form->input('globe_send_to');
            echo $this->Form->input('globe_status');
            echo $this->Form->input('smart_amount');
            echo $this->Form->input('smart_syntax');
            echo $this->Form->input('smart_send_to');
            echo $this->Form->input('smart_status');
            echo $this->Form->input('sun_amount');
            echo $this->Form->input('sun_syntax');
            echo $this->Form->input('sun_send_to');
            echo $this->Form->input('sun_status');
            echo $this->Form->input('tm_amount');
            echo $this->Form->input('tm_syntax');
            echo $this->Form->input('tm_send_to');
            echo $this->Form->input('tm_status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
