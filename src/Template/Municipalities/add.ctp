<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Municipalities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Regions'), ['controller' => 'Regions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Regions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Provinces'), ['controller' => 'Provinces', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Province'), ['controller' => 'Provinces', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Barangays'), ['controller' => 'Barangays', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Barangay'), ['controller' => 'Barangays', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Facilities'), ['controller' => 'Facilities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Facility'), ['controller' => 'Facilities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mprf Cases'), ['controller' => 'MprfCases', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mprf Case'), ['controller' => 'MprfCases', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sitios'), ['controller' => 'Sitios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sitio'), ['controller' => 'Sitios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipalities form large-9 medium-8 columns content">
    <?= $this->Form->create($municipality) ?>
    <fieldset>
        <legend><?= __('Add Municipality') ?></legend>
        <?php
            echo $this->Form->input('psgc_code');
            echo $this->Form->input('description');
            echo $this->Form->input('region_code');
            echo $this->Form->input('province_code');
            echo $this->Form->input('municipality_code');
            echo $this->Form->input('region_id', ['options' => $regions, 'empty' => true]);
            echo $this->Form->input('province_id', ['options' => $provinces, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
