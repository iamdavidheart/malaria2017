

<section class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <h3><?= __('Municipalities') ?></h3>
                <table class="table table-bordered table-striped " id="mun" cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            <th class="text-center">PROVINCE</th>
                            <th class="text-center">MUNICIPALITY</th>
                            <th class="text-center">CODE</th>
                            <th class="text-center" style="width:100px">ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($municipalities as $municipality): ?>
                        <tr>
                        <td><?= $municipality->province_code. ' ('.$municipality->province->description.')' ?></td>
                            <td><?= $municipality->description ?></td>
                            <td><?= $municipality->municipality_code ?></td>
                            
                            <td class="actions text-center">
              
                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-edit"></i> Edit'),
                                        ['action' => 'edit', $municipality->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm ',
                                            'escape' => false
                                        ]);
                                    ?>


                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-plus"></i> Add Brgy.'),
                                        ['action' => 'barangayAdd', $municipality->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm hide',
                                            'escape' => false
                                        ]);
                                    ?>

                            </td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>




            </div>
        </div>
    </div>
</section>





<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#mun').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>
