

<section class="main-container">


			<section class=" " >
				<div class="container">
					<div class="row">

						<div class="col-md-4 ">
							<div class="pv-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
								<span class="icon default-bg circle hide" style="margin-bottom:0px"><i class="fa fa-connectdevelop"></i></span>
								<?php echo $this->Html->image("globe.png", ["alt" => "GLOBE" , "class" => "image-responsive", "style" => "width:200px; margin:0px auto"]); ?>
								<h2 style="margin-top:5px;">09175743321</h2>
								<div class="separator clearfix"></div>
								<a class="btn btn-gray-transparent radius-50 btn-animated hide" href="#">Read More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
						<div class="col-md-4 ">
							<div class="pv-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="200">
								<span class="icon default-bg circle hide" style="margin-bottom:0px"><i class="fa fa-connectdevelop"></i></span>
								<?php echo $this->Html->image("smart.png", ["alt" => "SMART" , "class" => "image-responsive", "style" => "width:200px; margin:0px auto"]); ?>
								<h2 style="margin-top:5px;">09192035657</h2>
								<div class="separator clearfix"></div>
								<a class="btn btn-gray-transparent radius-50 btn-animated hide" href="#">Read More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
						<div class="col-md-4 ">
							<div class="pv-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="300">
								<span class="icon default-bg circle hide" style="margin-bottom:0px"><i class="fa fa-connectdevelop"></i></span>
								<?php echo $this->Html->image("sun.png", ["alt" => "SUN" , "class" => "image-responsive", "style" => "width:200px; margin:0px auto"]); ?>
								<h2 style="margin-top:5px;">09231951352</h2>
								<div class="separator clearfix"></div>
								<a class="btn btn-gray-transparent radius-50 btn-animated hide" href="#">Read More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</section>






				<div class="container">
					<div class="row">
                            <div class="col-lg-12">
								<div class="panel-group collapse-style-2" id="accordion-2">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2" style="background:#ea540d">
													<i class="fa fa-bold pr-10"></i>Case Trend Stacked Bar Chart
												</a>
											</h4>
										</div>
										<div id="collapseOne-2" class="panel-collapse collapse in">
											<div class="panel-body">
		                           				<div class="portlet"><!-- /primary heading -->
		                                            <div class="portlet-heading">
		                                                <div class="portlet-widgets">
		                                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
		                                                    <span class="divider"></span>
		                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet3"><i class="ion-minus-round"></i></a>
		                                                    <span class="divider"></span>
		                                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
		                                                </div>
		                                                <div class="clearfix"></div>
		                                            </div>
		                                            <div id="portlet3" class="panel-collapse collapse in">
		                                                <div class="portlet-body">
		                                                    <div id="combine-chart1"></div>
		                                                </div>
		                                            </div>
		                                        </div>


											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed" style="background:#ea540d">
													<i class="fa fa-leaf pr-10"></i>CASE TREND
												</a>
											</h4>
										</div>
										<div id="collapseTwo-2" class="panel-collapse collapse">
											<div class="panel-body">
		                                        <div class="table-responsive">

		                                            <!-- .table .table-striped .cases-summary-table -->
		                                            <table class="table table-bordered table-striped cases-summary-table dataTable" >
		                                                <thead>
		                                                <tr >
		                                                    <th style="text-align:center">Provinces</th>
		                                                    <th style="text-align:center">Jan</th>
		                                                    <th style="text-align:center">Feb</th>
		                                                    <th style="text-align:center">Mar</th>
		                                                    <th style="text-align:center">Apr</th>
		                                                    <th style="text-align:center">May</th>
		                                                    <th style="text-align:center">Jun</th>
		                                                    <th style="text-align:center">Jul</th>
		                                                    <th style="text-align:center">Aug</th>
		                                                    <th style="text-align:center">Sep</th>
		                                                    <th style="text-align:center">Oct</th>
		                                                    <th style="text-align:center">Nov</th>
		                                                    <th style="text-align:center">Dec</th>
		                                                </tr>
		                                                </thead>
		                                                <tbody>
		                                                <?php foreach ($query as $province) { ?>

		                                                    <tr style="text-align:center">
		                                                        <td style="text-align:center"><?= $province->province ?></td>
		                                                        <td><?= ($province->jan)? '<span class="badge">'.$province->jan.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->feb)? '<span class="badge">'.$province->feb.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->mar)? '<span class="badge">'.$province->mar.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->apr)? '<span class="badge">'.$province->apr.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->may)? '<span class="badge">'.$province->may.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->jun)? '<span class="badge">'.$province->jun.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->jul)? '<span class="badge">'.$province->jul.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->aug)? '<span class="badge">'.$province->aug.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->sep)? '<span class="badge">'.$province->sep.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->oct)? '<span class="badge">'.$province->oct.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->nov)? '<span class="badge">'.$province->nov.'</span>' : 0 ; ?></td>
		                                                        <td><?= ($province->dec)? '<span class="badge">'.$province->dec.'</span>' : 0 ; ?></td>
		                                                    </tr>
		                                                <?php } ?>
		                                                </tbody>
		                                            </table><!--/end .cases-summary-table -->
		                                        </div><!--/end .table-responsive -->
											</div>
										</div>
									</div>
								</div>

                            </div>

                                <h5 class="text-center">Through the initiative of </h5>
                                <div class="col-md-12 text-center">
                                    <?php echo $this->Html->image("mtrsLogo.png", ["alt" => "MTRS" , "class" => "image-responsive"]); ?>
                                </div>

                        </div>
					</div>


</div>

<?php $this->start('scriptCss'); ?>

    <?= $this->Html->css('../plugin/c3-chart/c3.css') ?>


<?php $this->end() ?>
<?php $this->start('scriptBottom'); ?>

    <?= $this->Html->script('../plugin/c3-chart/d3.v3.min.js') ?>
    <?= $this->Html->script('../plugin/c3-chart/c3.js') ?>



<script type="text/javascript">
    $(document).ready(function(){
		      



		var chart = c3.generate({
		    bindto: '#combine-chart1',
		 data: {
                x: 'x',
                columns: [
                    ['x', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'NOV', 'DEC'],


		        <?php foreach ($uQuery as $query) { ?>
		        ['U', "<?=(!empty($query->jan))? $query->jan : 0 ;?>",
		              "<?=(!empty($query->feb))? $query->feb : 0 ;?>", 
		              "<?=(!empty($query->mar))? $query->mar : 0 ;?>",
		              "<?=(!empty($query->apr))? $query->apr : 0 ;?>",
		              "<?=(!empty($query->may))? $query->may : 0 ;?>",
		              "<?=(!empty($query->jun))? $query->jun : 0 ;?>",
		              "<?=(!empty($query->jul))? $query->jul : 0 ;?>",
		              "<?=(!empty($query->aug))? $query->aug : 0 ;?>",
		              "<?=(!empty($query->sep))? $query->sep : 0 ;?>",
		              "<?=(!empty($query->oct))? $query->oct : 0 ;?>",
		              "<?=(!empty($query->nov))? $query->nov : 0 ;?>",
		              "<?=(!empty($query->dec))? $query->dec : 0 ;?>"
		        ],

		        ['C', "<?=(!empty($query->janc))? $query->janc : 0 ;?>",
		              "<?=(!empty($query->febc))? $query->febc : 0 ;?>", 
		              "<?=(!empty($query->marc))? $query->marc : 0 ;?>",
		              "<?=(!empty($query->aprc))? $query->aprc : 0 ;?>",
		              "<?=(!empty($query->mayc))? $query->mayc : 0 ;?>",
		              "<?=(!empty($query->junc))? $query->junc : 0 ;?>",
		              "<?=(!empty($query->julc))? $query->julc : 0 ;?>",
		              "<?=(!empty($query->augc))? $query->augc : 0 ;?>",
		              "<?=(!empty($query->sepc))? $query->sepc : 0 ;?>",
		              "<?=(!empty($query->octc))? $query->octc : 0 ;?>",
		              "<?=(!empty($query->novc))? $query->novc : 0 ;?>",
		              "<?=(!empty($query->decc))? $query->decc : 0 ;?>"
		        ],


		        ['S', "<?=(!empty($query->jans))? $query->jans : 0 ;?>",
		              "<?=(!empty($query->febs))? $query->febs : 0 ;?>", 
		              "<?=(!empty($query->mars))? $query->mars : 0 ;?>",
		              "<?=(!empty($query->aprs))? $query->aprs : 0 ;?>",
		              "<?=(!empty($query->mays))? $query->mays : 0 ;?>",
		              "<?=(!empty($query->juns))? $query->juns : 0 ;?>",
		              "<?=(!empty($query->juls))? $query->juls : 0 ;?>",
		              "<?=(!empty($query->augs))? $query->augs : 0 ;?>",
		              "<?=(!empty($query->seps))? $query->seps : 0 ;?>",
		              "<?=(!empty($query->octs))? $query->octs : 0 ;?>",
		              "<?=(!empty($query->novs))? $query->novs : 0 ;?>",
		              "<?=(!empty($query->decs))? $query->decs : 0 ;?>"
		        ],


		        ['D', "<?=(!empty($query->jand))? $query->jand : 0 ;?>",
		              "<?=(!empty($query->febd))? $query->febd : 0 ;?>", 
		              "<?=(!empty($query->mard))? $query->mard : 0 ;?>",
		              "<?=(!empty($query->aprd))? $query->aprd : 0 ;?>",
		              "<?=(!empty($query->mayd))? $query->mayd : 0 ;?>",
		              "<?=(!empty($query->jund))? $query->jund : 0 ;?>",
		              "<?=(!empty($query->juld))? $query->juld : 0 ;?>",
		              "<?=(!empty($query->augd))? $query->augd : 0 ;?>",
		              "<?=(!empty($query->sepd))? $query->sepd : 0 ;?>",
		              "<?=(!empty($query->octd))? $query->octd : 0 ;?>",
		              "<?=(!empty($query->novd))? $query->novd : 0 ;?>",
		              "<?=(!empty($query->decd))? $query->decd : 0 ;?>"
		        ],

		        <?php } ?>

		    ],
		    type: 'bar',
		        groups: [
		            ['U', 'C', 'S', 'D']
		        ]

		},

		    axis: {
		        x: {
		            type: 'category'
		        }
		    }
		});



    });
</script>
<?php $this->end() ?>