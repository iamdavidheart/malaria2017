<section class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'facilities', 'action' => 'add']) ?>">Add Facility <i class="fa fa-plus"></i></a>
            </div>


            <div class="col-md-12">
                <h3><?= __('FACILITIES') ?></h3>


                    <table class="table table-bordered table-striped dataTable" id="facilities">
                        <thead>
                            <tr>
         
                                <th   class="text-center"> <?= $this->Paginator->sort('ID') ?></th>
                                <th class="text-center"><?= $this->Paginator->sort('NAME') ?></th>
                                <th class="text-center"> <?= $this->Paginator->sort('ADDRESS') ?></th>
                                <th class="actions text-center" style="width:200px;"><?= __('ACTIONS') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; foreach($facilities as $facility){ $i++; ?>
                                <tr>
                                    <td><?=$facility->id?></td>
                                    <td><?=$facility->facility_category->description?> </td>
                                    <td>
                                        <?=$facility->region->description?>, 
                                        <?=$facility->province->description?>
                                        <?=$facility->municipality->description?>,
                                        <?php
                                            if($facility->barangay){
                                               echo $facility->barangay->description;
                                            }
                                        ?>
                                        <?php
                                            if($facility->sitio){
                                               echo $facility->sitio->description;
                                            }
                                        ?>                                        
                                     </td>
                           
                                    <td class="actions text-center">
                       
                                  <?php 
                                        echo $this->Html->link(
                                            __('<i class="fa fa-edit"></i> Edit'),
                                            ['action' => 'edit', $facility->id],
                                            [
                                                'class' => 'btn btn-gray-transparent btn-sm ',
                                                'escape' => false
                                            ]);
                                        ?>

                                        <?= $this->Form->postLink( __('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $facility->id],
                                            ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $facility->id )]  ); ?>

                                    
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>





            </div>




        </div>
    </div>
</section>
<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#facilities').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>
