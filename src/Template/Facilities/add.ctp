<section class="main-container">
    <div class="container">
        <div class="row">


        <div class="col-md-8">
            <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'facilities']) ?>">Facility List<i class="fa fa-list"></i></a>
        </div>

        <div class="col-md-12">
                <h3><?= __('ADD FACILITY') ?></h3>

                    <?php echo $this->Form->create($facility); ?>
                        <div class="row">
       
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Facility Name *</label>
                                    <?php echo $this->Form->input('facility_category_id', ['id' => 'facility_category_id',  'class' => 'form-control ', 
                                    'options' => $facilityCategories, 'label' => false, 'empty' => '(Choose Facility Name)']); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Region *</label>
                                    <select class="form-control" name="region_id" id="region_id">
                                        <option>(Choose Region)</option>
                                        <?php foreach ($regions as $key => $region) { ?>
                                        <option value="<?php echo $region->id?>" data-id="<?php echo $region->code?>"><?php echo $region->description?> <?php echo $region->code?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Province *</label>
                                    <select class="form-control" name="province_id" id="province_id">
                                        <option>(Choose Province)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Municipality/City *</label>
                                    <select class="form-control" name="municipality_id" id="municipality_id">
                                        <option>(Choose Municipality/City)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Barangay *</label>
                                    <select class="form-control" name="barangay_id" id="barangay_id">
                                        <option>(Choose Barangay)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Sitio </label>
                                    <select class="form-control" name="sitio_id" id="sitio_id">
                                        <option>(Choose Sitio)</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                     
               
                         <button type="submit" class="btn btn-default">Submit</button>
                   <?= $this->Form->end() ?>

        </div>

  
        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/bootstrap-datepicker/css/bootstrap-datepicker.css'); ?>
    <?= $this->Html->css('../js/plugin/choosen/css/chosenOrange.css'); ?>
<?php $this->end() ?>




<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('../js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
    <?=  $this->Html->script('../js/plugin/choosen/js/chosen.jquery.min.js'); ?>
    <script type="text/javascript">


        (function($){
            $(document).ready(function(){

                var config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : {allow_single_deselect:true},
                    '.chosen-select-no-single' : {disable_search_threshold:10},
                    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                    '.chosen-select-width'     : {width:"95%"}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }



            
                    $('#region_id').on('change',function(){
                        var id    = $(this).val();
                        //var code    = $(this).find('option:selected').data('id');
                        $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-province']) ?>",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                region_code : id
                            },
                            success: function(response){

                                if(response.json.error === false){
                                    $('#province_id option').remove();
                                    $('#municipality_id option').remove();
                                    $('#barangay_id option').remove();
                                    $('#sitio_id option').remove();

                                    var html = '<option value="">(Choose Province)</option>';
                                    $.map( response.json.provinces, function( value, index ) {
                                        html += '<option value="'+ value.id +'" data-id="'+ value.province_code +'">' + value.description + '</option>';
                                    });
                                    $('#province_id').append(html);
                                }else{
                                    alert(response.json.message);
                                }
                            }
                        });
                    });

                    $('#province_id').on('change',function(){

                        var id    = $(this).val();


                        $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-municipality']) ?>",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                province_code : id
                            },
                            success: function(response){


                                if(response.json.error === false){

                                    $('#municipality_id option').remove();
                                    var html = '<option value="">(Choose Municipality/City)</option>';
                                    $.map( response.json.municipalities, function( value, index ) {
                                        html += '<option value="' + value.id + '" data-id="'+ value.municipality_code +'">' + value.description + '</option>';
                                    });
                                    $('#municipality_id').append(html);
                                }else{
                                    alert(response.json.message);
                                }
                            }
                        });
                    });

                    $('#municipality_id').on('change',function(){

                        var id    = $(this).val();
                        //var code    = $(this).find('option:selected').data('id');
                        $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-barangay']) ?>",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                municipality_code : id
                            },
                            success: function(response){


                                if(response.json.error === false){

                                    $('#barangay_id option').remove();
                                    var html = '<option value="">(Choose Barangay)</option>';
                                    $.map( response.json.barangays, function( value, index ) {
                                        html += '<option value="'+ value.id +'" data-id="'+ value.barangay_code +'">' + value.description + '</option>';
                                    });
                                    $('#barangay_id').append(html);
                                }else{
                                    alert(response.json.message);
                                }
                            }
                        });
                    });

                    $('#barangay_id').on('change',function(){
                        var id    = $(this).val();
                        //var code    = $(this).find('option:selected').data('id');
                        $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-sitio']) ?>",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                barangay_code : id
                            },
                            success: function(response){

                                if(response.json.error === false){

                                    $('#sitio_id option').remove();
                                    var html = '<option value="">(Choose Sitio)</option>';
                                    $.map( response.json.sitios, function( value, index ) {
                                        html += '<option value="'+ value.id +'" data-id="'+ value.sitio_id +'">' + value.description + '</option>';
                                    });
                                    $('#sitio_id').append(html);
                                }else{
                                    //alert(response.json.message);
                                }
                            }
                        });
                    });


        }); // End document ready


        })(this.jQuery);




    </script>
<?php $this->end() ?>




