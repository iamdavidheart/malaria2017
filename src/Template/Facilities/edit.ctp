<section class="main-container">
    <div class="container">
        <div class="row">


        <div class="col-md-8">
            <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'facilities']) ?>">Facility List<i class="fa fa-list"></i></a>
             <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'facilities', 'action' =>'add']) ?>">Add Facility <i class="fa fa-plus"></i></a>
        </div>

        <div class="col-md-12">
                <h3><?= __('EDIT FACILITY') ?></h3>

                    <?php echo $this->Form->create($facility); ?>
                        <div class="row">
       

                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Facility Name *</label>
                                    <?php echo $this->Form->input('facility_category_id', ['id' => 'facility_category_id',  'class' => 'form-control ', 
                                    'options' => $facilityCategories, 'label' => false, 'empty' => '(Choose Facility Name)']); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Region *</label>
                                    <?php echo $this->Form->input('region_id', ['id' => 'region_id',  'class' => 'form-control', 
                                    'options' => $regions, 'label' => false, 'empty' => '(Choose Region)']); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Province *</label>

                                    <?php echo $this->Form->input('province_id', ['id' => 'province_id',  'class' => 'form-control', 
                                    'options' => $provinces, 'label' => false, 'empty' => '(Choose Provinces)']); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Municipality/City *</label>
                                    <?php echo $this->Form->input('municipality_id', ['id' => 'municipality_id',  'class' => 'form-control', 
                                    'options' => $municipalities, 'label' => false, 'empty' => '(Choose Municipality/City)']); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Barangay *</label>
                                    <?php echo $this->Form->input('barangay_id', ['id' => 'barangay_id',  'class' => 'form-control', 
                                    'options' => $barangays, 'label' => false, 'empty' => '(Choose Barangay)']); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Sitio *</label>
                                    <?php echo $this->Form->input('sitio_id', ['id' => 'sitio_id',  'class' => 'form-control', 
                                    'options' => $sitios, 'label' => false, 'empty' => '(Choose Sitio)']); ?>
                                </div>
                            </div>
            


                        </div>
                     
               
                         <button type="submit" class="btn btn-default">Submit</button>
                   <?= $this->Form->end() ?>

        </div>

  
        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>

<?php $this->end() ?>




<?php $this->start('scriptFooter'); ?>

    <script type="text/javascript">


        (function($){
            $(document).ready(function(){


     

                $('#region_id').on('change',function(){
                    var id    = $(this).val();
                    // var code    = $(this).find('option:selected').data('id');
                    $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-province']) ?>",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            region_code : id
                        },
                        success: function(response){
                            if(response.json.error === false){
                                $('#province_id option').remove();
                                $('#municipality_id option').remove();
                                $('#barangay_id option').remove();
                                $('#sitio_id option').remove();

                                var html = '<option value="">(Choose Province)</option>';
                                $.map( response.json.provinces, function( value, index ) {
                                    html += '<option value="'+ value.id +'" data-id="'+ value.province_code +'">' + value.description + '</option>';
                                });
                                $('#province_id').append(html);
                            }else{
                                alert(response.json.message);
                            }
                        }
                    });
                });





                $('#province_id').on('change',function(){
                    var id    = $(this).val();
                    //var code    = $(this).find('option:selected').data('id');
                    $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-municipality']) ?>",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            province_code : id
                        },
                        success: function(response){
                            if(response.json.error === false){
                                $('#municipality_id option').remove();
                                var html = '<option value="">(Choose Municipality/City)</option>';
                                $.map( response.json.municipalities, function( value, index ) {
                                    html += '<option value="' + value.id + '" data-id="'+ value.municipality_code +'">' + value.description + '</option>';
                                });
                                $('#municipality_id').append(html);
                            }else{
                                alert(response.json.message);
                            }
                        }
                    });
                });

                $('#municipality_id').on('change',function(){
                    var id    = $(this).val();
                    //var code    = $(this).find('option:selected').data('id');
                    $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-barangay']) ?>",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            municipality_code : id
                        },
                        success: function(response){


                            if(response.json.error === false){

                                $('#barangay_id option').remove();
                                var html = '<option value="">(Choose Barangay)</option>';
                                $.map( response.json.barangays, function( value, index ) {
                                    html += '<option value="'+ value.id +'" data-id="'+ value.barangay_code +'">' + value.description + '</option>';
                                });
                                $('#barangay_id').append(html);
                            }else{
                                alert(response.json.message);
                            }
                        }
                    });
                });

                $('#barangay_id').on('change',function(){
                    var id    = $(this).val();
                   // var code    = $(this).find('option:selected').data('id');
                    $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-sitio']) ?>",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            barangay_code : id
                        },
                        success: function(response){

                            if(response.json.error === false){

                                $('#sitio_id option').remove();
                                var html = '<option value="">(Choose Sitio)</option>';
                                $.map( response.json.sitios, function( value, index ) {
                                    html += '<option value="'+ value.sitio_id +'" data-id="'+ value.sitio_id +'">' + value.description + '</option>';
                                });
                                $('#sitio_id').append(html);
                            }else{
                                alert(response.json.message);
                            }
                        }
                    });
                });












            }); // End document ready


        })(this.jQuery);




    </script>
<?php $this->end() ?>




