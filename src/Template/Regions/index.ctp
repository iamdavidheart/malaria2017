
<section class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <h3><?= __('Regions') ?></h3>
                <table class="table table-bordered table-striped " id="region" cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('CODE') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('DESCRIPTION') ?></th>
                            <th scope="col" class="actions text-center" width="200px"><?= __('ACTIONS') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($regions as $region): ?>
                        <tr>
                            <td><?= h($region->code) ?></td>
                            <td><?= h($region->description) ?></td>
                            
                            <td class="actions text-center">

                              <?php 
                              //['action' => 'delete', $region->id]
                                echo $this->Html->link(
                                    __('<i class="fa fa-edit"></i> Edit'),
                                    ['action' => 'edit', $region->id],
                                    [
                                        'class' => 'btn btn-gray-transparent btn-sm ',
                                        'escape' => false
                                    ]);
                                ?>

                                <?= $this->Form->postLink( __('<i class="fa fa-trash"></i> Delete'), '#',
                                    ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Unable to delete' )]  ); ?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
      

            </div>
        </div>
    </div>
</section>


