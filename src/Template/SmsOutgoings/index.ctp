<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sms Outgoing'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['controller' => 'SmsIncomings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['controller' => 'SmsIncomings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsOutgoings index large-9 medium-8 columns content">
    <h3><?= __('Sms Outgoings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sms_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('group_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('source') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sms_incoming_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('receiver_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('receiver_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sender_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('box') ?></th>
                <th scope="col"><?= $this->Paginator->sort('network') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('view') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($smsOutgoings as $smsOutgoing): ?>
            <tr>
                <td><?= $this->Number->format($smsOutgoing->id) ?></td>
                <td><?= h($smsOutgoing->created) ?></td>
                <td><?= h($smsOutgoing->modified) ?></td>
                <td><?= h($smsOutgoing->sms_code) ?></td>
                <td><?= h($smsOutgoing->group_id) ?></td>
                <td><?= h($smsOutgoing->source) ?></td>
                <td><?= $smsOutgoing->has('sms_incoming') ? $this->Html->link($smsOutgoing->sms_incoming->id, ['controller' => 'SmsIncomings', 'action' => 'view', $smsOutgoing->sms_incoming->id]) : '' ?></td>
                <td><?= h($smsOutgoing->receiver_no) ?></td>
                <td><?= $this->Number->format($smsOutgoing->receiver_id) ?></td>
                <td><?= h($smsOutgoing->sender_no) ?></td>
                <td><?= $smsOutgoing->has('user') ? $this->Html->link($smsOutgoing->user->id, ['controller' => 'Users', 'action' => 'view', $smsOutgoing->user->id]) : '' ?></td>
                <td><?= h($smsOutgoing->status) ?></td>
                <td><?= $this->Number->format($smsOutgoing->box) ?></td>
                <td><?= h($smsOutgoing->network) ?></td>
                <td><?= h($smsOutgoing->type) ?></td>
                <td><?= $this->Number->format($smsOutgoing->view) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $smsOutgoing->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smsOutgoing->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smsOutgoing->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsOutgoing->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
