<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sms Outgoing'), ['action' => 'edit', $smsOutgoing->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sms Outgoing'), ['action' => 'delete', $smsOutgoing->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsOutgoing->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sms Outgoings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Outgoing'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['controller' => 'SmsIncomings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['controller' => 'SmsIncomings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="smsOutgoings view large-9 medium-8 columns content">
    <h3><?= h($smsOutgoing->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Sms Code') ?></th>
            <td><?= h($smsOutgoing->sms_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Group Id') ?></th>
            <td><?= h($smsOutgoing->group_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Source') ?></th>
            <td><?= h($smsOutgoing->source) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sms Incoming') ?></th>
            <td><?= $smsOutgoing->has('sms_incoming') ? $this->Html->link($smsOutgoing->sms_incoming->id, ['controller' => 'SmsIncomings', 'action' => 'view', $smsOutgoing->sms_incoming->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Receiver No') ?></th>
            <td><?= h($smsOutgoing->receiver_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sender No') ?></th>
            <td><?= h($smsOutgoing->sender_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $smsOutgoing->has('user') ? $this->Html->link($smsOutgoing->user->id, ['controller' => 'Users', 'action' => 'view', $smsOutgoing->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($smsOutgoing->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Network') ?></th>
            <td><?= h($smsOutgoing->network) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($smsOutgoing->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($smsOutgoing->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Receiver Id') ?></th>
            <td><?= $this->Number->format($smsOutgoing->receiver_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Box') ?></th>
            <td><?= $this->Number->format($smsOutgoing->box) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('View') ?></th>
            <td><?= $this->Number->format($smsOutgoing->view) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($smsOutgoing->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($smsOutgoing->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($smsOutgoing->message)); ?>
    </div>
    <div class="row">
        <h4><?= __('Error') ?></h4>
        <?= $this->Text->autoParagraph(h($smsOutgoing->error)); ?>
    </div>
</div>
