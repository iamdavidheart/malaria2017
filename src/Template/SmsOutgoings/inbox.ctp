<section class="main-container">

				<div class="container">
					<div class="row">




						<!-- sidebar start -->
						<!-- ================ -->
						<aside class="col-md-2" >
							<div class="sidebar">
								<div class="block clearfix">
									<h4 class="title text-center">TXT REPORTS</h4>
									<div class="separator-2"></div>
									<nav>
										<ul class="nav nav-pills nav-stacked">
											<li class="active"><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'inbox']) ?>"><i class="fa fa-inbox"></i> INBOX</a></li>
											<li><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'outbox']) ?>"><i class="fa fa-envelope"></i>  OUTBOX  </a></li>
                                            <li><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'sent']) ?>"><i class="fa fa-envelope-o"></i> SENT  </a></li>
											<li><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'spam']) ?>"><i class="fa fa-file-text-o"></i> SPAM  </a></li>
										</ul>
									</nav>
								</div>
						
							</div>
						</aside>
						<!-- sidebar end -->



						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-10" >
							
			                <div id="loading"></div>
                                <div class="pull-right">
                                    <button class="btn btn-animated btn-gray-transparent btn-sm t" id="delete">Delete <i class="fa fa-trash"></i></button>
                                    <button class="btn btn-animated btn-gray-transparent btn-sm" id="refresh">Reload <i class="fa fa-refresh"></i></button>
                                </div>
			                 <div class="clearfix"></div>
			                
			                <div class="table-responsive" id="inboxContainer">
			                    <center>
			                    <button class="btn btn-animated btn-gray-transparent btn-sm"> <i class="fa fa-circle-o-notch fa-spin"></i>Loading..</button>
			                    </center>
			                </div>
					
						</div>
						<!-- main end -->


					</div>
				</div>
</section>



    <div class="modal fade custom-width" id="modal-inbox">
        <div class="modal-dialog" >
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">VIEW INBOX</h4>
                </div>
                <form  id="modal-add-swatch" class="modal-add-swatch" action=""  method="post">
                    <div class="modal-body">
             
                        <div class="panel panel-body">
                            <div class="row ">
                                <div class="col-md-4">


                                    <div class="form-group has-success has-feedback">
                                        <label for="field-name" class="control-label">CODE</label>
                                        <input type="text" class="form-control"  id="code" autocomplete="off"  >
                                    </div>  
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group has-success has-feedback">
                                        <label for="" class="control-label">SENDER</label>
                                        <input type="text" class="form-control"  id="sender" autocomplete="off" >
                                    </div>  
                                </div>
		                        <div class="col-md-12">
		                            <div class="form-group has-success has-feedback">
                                        <label  class="control-label">MESSAGE</label>
                                        <textarea type="text" class="form-control" id="message" rows="5" autocomplete="off" ></textarea>
		                            </div>  
		                        </div>
                                <div class="col-md-6">
                                    <div class="form-group has-success has-feedback">
                                        <label class="control-label">DATE RECEIVED</label>
                                        <input type="text" class="form-control"  id="date" autocomplete="off"  >
                                    </div>  
                                </div>
                            </div>
                        </div>
                  
               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            function load() {
                $('#inboxContainer').load("<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'load-inbox']) ?>");
            }

            $('#refresh').on('click', function(){
            	load();

            });





            load();

        });

    </script>
<?php $this->end() ?>