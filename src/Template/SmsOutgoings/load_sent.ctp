
<table class="sent table table-bordered table-striped" id="sent-d"  >
    <thead>
        <tr>
            <th ></th>
            <th class="text-center">RECIPIENT</th>
            <th class="text-center">MESSAGE</th>
            <th class="text-center">DATETIME</th>
            <th class="actions text-center"><?= __('ACTION') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php  $i = 0; foreach ($smsOutgoings as $smsOutgoing){ $i++; ?>

            <?php if($smsOutgoing->view == 0) {?>
                 <tr style="background:#eaeaea">
            <?php }else{ ?>
                 <tr>
            <?php } ?>


            <td><?=$smsOutgoing->id?></td>
            <td>
                <?php

                    if(!empty($smsOutgoing->user)){
                        echo $sender = $smsOutgoing->user->full_name;
                    }else{
                        echo $sender = $smsOutgoing->receiver_no;
                    }
                ?>
            </td>
            <td>
                <?php 
                echo $this->Text->truncate(
                    $smsOutgoing->message,
                    45,
                    [
                        'ellipsis' => '...',
                        'exact' => false
                    ]
                );
                ?>
            </td>
            <td><small><?php echo date('Y/m/d h:i A', strtotime($smsOutgoing->created)); ?></small></td>
            <td class="actions text-center">


                <a href="javascript:void(0);" 
              		class="btn btn-gray-transparent btn-sm view" 
              		data-id="<?=$smsOutgoing->id?>" 
                    data-code="<?= $smsOutgoing->template_code; ?>"
                    data-sender="<?= $sender; ?>"
                    data-message="<?= $smsOutgoing->message; ?>"
                    data-date="<?= date('Y/m/d h:i A', strtotime($smsOutgoing->created)); ?>"
                    >
                    <i class="fa fa-eye"></i> View
                </a>


            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>


    <div class="modal fade custom-width" id="modal-sent">
        <div class="modal-dialog" >
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">VIEW SENT</h4>
                </div>
                <form  id="modal-add-swatch" class="modal-add-swatch" action=""  method="post">
                    <div class="modal-body">
             
                        <div class="panel panel-body">
                            <div class="row ">
                
                                <div class="col-md-12">
                                    <div class="form-group has-success has-feedback">
                                        <label for="" class="control-label">RECIPIENT</label>
                                        <input type="text" class="form-control"  id="sender" autocomplete="off" >
                                    </div>  
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group has-success has-feedback">
                                        <label  class="control-label">MESSAGE</label>
                                        <textarea type="text" class="form-control" id="message" rows="5" autocomplete="off" ></textarea>
                                    </div>  
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-success has-feedback">
                                        <label class="control-label">DATE CREATED</label>
                                        <input type="text" class="form-control"  id="date" autocomplete="off"  >
                                    </div>  
                                </div>
                            </div>
                        </div>
                  
               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



<script type="text/javascript"> 
        $(document).ready(function () {

            var table = $('#sent-d').DataTable({
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                            selectRow: true
                            }
                        }
                    ],
                        select: {
                        style: 'multi'
                    },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });


					    $('.view').on('click',function(e){
					        e.preventDefault();
					        $('#modal-sent').modal('show');
					        $('#code').val($(this).data('code'));
					        $('#sender').val($(this).data('sender'));
					        $('#message').val($(this).data('message'));
					        $('#date').val($(this).data('date'));

					    });


                        
                    }
            });


            //ACTIVATION
           $('#delete').on('click', function(e){
                var ids = [];

                var rows_selected = table.column(0).checkboxes.selected();
                $.each(rows_selected, function(index, rowId){
                     ids.push(rowId);
                });


                if(ids.length == 0) {
                   var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                        html += 'You have no selected message.';
                        html += '</div>';
                          $('#loading').html(html);
                    return;
                }


                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'delete-sms']) ?>",
                    cache: false,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        id: ids
                    },
                    beforeSend: function() {
                        $('#send i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>DELETING  Loading..');
                        $('#send').prop('disabled', true);
                    },
                    complete: function() {
                        $('#send').html('Delete <i class="fa fa-trash"></i>');
                        $('#send').prop('disabled', false);
                    },
                    success: function(response) {
                        var error = response.json.error,
                            message = response.json.message;
                        if(error){
                        }else{
                            var html = '';
                            html += '<div class="alert alert-success alert-dismissible" role="alert">';
                            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                            html += 'Sent message has been deleted.';
                            html += '</div>';

                            $('#loading').html(html);
                            $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".alert-success").slideUp(500);
                             });     

                        }
                       $('#sentContainer').load("<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'load-sent']) ?>");
                    }
                });

                return;
           });


        });
</script>

