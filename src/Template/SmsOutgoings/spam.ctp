<section class="main-container">
    <div class="container">
        <div class="row">


            <!-- sidebar start -->
            <!-- ================ -->
            <aside class="col-md-2" >
                <div class="sidebar">
                    <div class="block clearfix">
                        <h4 class="title text-center">TXT REPORTS</h4>
                        <div class="separator-2"></div>
                        <nav>
                            <ul class="nav nav-pills nav-stacked">
                                <li ><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'inbox']) ?>"><i class="fa fa-inbox"></i> INBOX  </a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'outbox']) ?>"><i class="fa fa-envelope"></i>  OUTBOX </a></li>
                                <li ><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'sent']) ?>"><i class="fa fa-envelope-o"></i> SENT  </a></li>
                                <li class="active"><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'spam']) ?>"><i class="fa fa-file-text-o"></i> SPAM </a></li>
                            </ul>
                        </nav>
                    </div>
            
                </div>
            </aside>
            <!-- sidebar end -->



            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-10" >

                <div id="loading"></div>
                <div class="pull-right">
                    <button class="btn btn-animated btn-gray-transparent btn-sm" id="delete">Delete <i class="fa fa-trash"></i></button>
                    <button class="btn btn-animated btn-gray-transparent btn-sm" id="refresh">Reload <i class="fa fa-refresh"></i></button>
                </div>
                <div class="clearfix"></div>  


                <div class="table-responsive" id="spamContainer">
                    <center>
                    <button class="btn btn-animated btn-gray-transparent btn-sm"> <i class="fa fa-circle-o-notch fa-spin"></i>Loading..</button>
                    </center>
                </div>
        
            </div>
            <!-- main end -->


        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

     
            function load() {
                $('#spamContainer').load("<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'load-spam']) ?>");
            }

            $('#refresh').on('click', function(){
                load();
                
            });
            load();


        });

    </script>
<?php $this->end() ?>