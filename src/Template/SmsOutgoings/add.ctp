<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sms Outgoings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['controller' => 'SmsIncomings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['controller' => 'SmsIncomings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsOutgoings form large-9 medium-8 columns content">
    <?= $this->Form->create($smsOutgoing) ?>
    <fieldset>
        <legend><?= __('Add Sms Outgoing') ?></legend>
        <?php
            echo $this->Form->input('sms_code');
            echo $this->Form->input('group_id');
            echo $this->Form->input('source');
            echo $this->Form->input('sms_incoming_id', ['options' => $smsIncomings, 'empty' => true]);
            echo $this->Form->input('receiver_no');
            echo $this->Form->input('receiver_id');
            echo $this->Form->input('sender_no');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('message');
            echo $this->Form->input('error');
            echo $this->Form->input('status');
            echo $this->Form->input('box');
            echo $this->Form->input('network');
            echo $this->Form->input('type');
            echo $this->Form->input('view');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
