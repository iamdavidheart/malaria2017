<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $smsIncoming->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $smsIncoming->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mprf Cases'), ['controller' => 'MprfCases', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mprf Case'), ['controller' => 'MprfCases', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sms Outgoings'), ['controller' => 'SmsOutgoings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Outgoing'), ['controller' => 'SmsOutgoings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsIncomings form large-9 medium-8 columns content">
    <?= $this->Form->create($smsIncoming) ?>
    <fieldset>
        <legend><?= __('Edit Sms Incoming') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('template_code');
            echo $this->Form->input('sms_code');
            echo $this->Form->input('sender_no');
            echo $this->Form->input('phone_no');
            echo $this->Form->input('message');
            echo $this->Form->input('status');
            echo $this->Form->input('message_type');
            echo $this->Form->input('error_message');
            echo $this->Form->input('validate');
            echo $this->Form->input('box');
            echo $this->Form->input('view');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
