<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mprf Cases'), ['controller' => 'MprfCases', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mprf Case'), ['controller' => 'MprfCases', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sms Outgoings'), ['controller' => 'SmsOutgoings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Outgoing'), ['controller' => 'SmsOutgoings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsIncomings index large-9 medium-8 columns content">
    <h3><?= __('Sms Incomings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('template_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sms_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sender_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('message_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('validate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('box') ?></th>
                <th scope="col"><?= $this->Paginator->sort('view') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($smsIncomings as $smsIncoming): ?>
            <tr>
                <td><?= $this->Number->format($smsIncoming->id) ?></td>
                <td><?= h($smsIncoming->created) ?></td>
                <td><?= h($smsIncoming->modified) ?></td>
                <td><?= $smsIncoming->has('user') ? $this->Html->link($smsIncoming->user->id, ['controller' => 'Users', 'action' => 'view', $smsIncoming->user->id]) : '' ?></td>
                <td><?= h($smsIncoming->template_code) ?></td>
                <td><?= h($smsIncoming->sms_code) ?></td>
                <td><?= h($smsIncoming->sender_no) ?></td>
                <td><?= h($smsIncoming->phone_no) ?></td>
                <td><?= h($smsIncoming->status) ?></td>
                <td><?= h($smsIncoming->message_type) ?></td>
                <td><?= $this->Number->format($smsIncoming->validate) ?></td>
                <td><?= $this->Number->format($smsIncoming->box) ?></td>
                <td><?= $this->Number->format($smsIncoming->view) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $smsIncoming->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smsIncoming->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smsIncoming->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsIncoming->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
