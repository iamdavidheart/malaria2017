<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sms Incoming'), ['action' => 'edit', $smsIncoming->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sms Incoming'), ['action' => 'delete', $smsIncoming->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsIncoming->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mprf Cases'), ['controller' => 'MprfCases', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mprf Case'), ['controller' => 'MprfCases', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Outgoings'), ['controller' => 'SmsOutgoings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Outgoing'), ['controller' => 'SmsOutgoings', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="smsIncomings view large-9 medium-8 columns content">
    <h3><?= h($smsIncoming->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $smsIncoming->has('user') ? $this->Html->link($smsIncoming->user->id, ['controller' => 'Users', 'action' => 'view', $smsIncoming->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Template Code') ?></th>
            <td><?= h($smsIncoming->template_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sms Code') ?></th>
            <td><?= h($smsIncoming->sms_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sender No') ?></th>
            <td><?= h($smsIncoming->sender_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone No') ?></th>
            <td><?= h($smsIncoming->phone_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($smsIncoming->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Message Type') ?></th>
            <td><?= h($smsIncoming->message_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($smsIncoming->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Validate') ?></th>
            <td><?= $this->Number->format($smsIncoming->validate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Box') ?></th>
            <td><?= $this->Number->format($smsIncoming->box) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('View') ?></th>
            <td><?= $this->Number->format($smsIncoming->view) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($smsIncoming->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($smsIncoming->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($smsIncoming->message)); ?>
    </div>
    <div class="row">
        <h4><?= __('Error Message') ?></h4>
        <?= $this->Text->autoParagraph(h($smsIncoming->error_message)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Items') ?></h4>
        <?php if (!empty($smsIncoming->items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Template Code') ?></th>
                <th scope="col"><?= __('Source') ?></th>
                <th scope="col"><?= __('Report Date') ?></th>
                <th scope="col"><?= __('Facility Id') ?></th>
                <th scope="col"><?= __('Sms Incoming Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col"><?= __('Al Qty') ?></th>
                <th scope="col"><?= __('Pq Qty') ?></th>
                <th scope="col"><?= __('Cq Qty') ?></th>
                <th scope="col"><?= __('Qt Qty') ?></th>
                <th scope="col"><?= __('Qa Qty') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($smsIncoming->items as $items): ?>
            <tr>
                <td><?= h($items->id) ?></td>
                <td><?= h($items->template_code) ?></td>
                <td><?= h($items->source) ?></td>
                <td><?= h($items->report_date) ?></td>
                <td><?= h($items->facility_id) ?></td>
                <td><?= h($items->sms_incoming_id) ?></td>
                <td><?= h($items->user_id) ?></td>
                <td><?= h($items->remarks) ?></td>
                <td><?= h($items->al_qty) ?></td>
                <td><?= h($items->pq_qty) ?></td>
                <td><?= h($items->cq_qty) ?></td>
                <td><?= h($items->qt_qty) ?></td>
                <td><?= h($items->qa_qty) ?></td>
                <td><?= h($items->created) ?></td>
                <td><?= h($items->modified) ?></td>
                <td><?= h($items->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Items', 'action' => 'view', $items->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Items', 'action' => 'edit', $items->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Items', 'action' => 'delete', $items->id], ['confirm' => __('Are you sure you want to delete # {0}?', $items->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Mprf Cases') ?></h4>
        <?php if (!empty($smsIncoming->mprf_cases)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Sms Incoming Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Source') ?></th>
                <th scope="col"><?= __('Facility Id') ?></th>
                <th scope="col"><?= __('Template Code') ?></th>
                <th scope="col"><?= __('Consult Date') ?></th>
                <th scope="col"><?= __('Death Date') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Middle Name') ?></th>
                <th scope="col"><?= __('Age') ?></th>
                <th scope="col"><?= __('Weight') ?></th>
                <th scope="col"><?= __('Birthdate') ?></th>
                <th scope="col"><?= __('Gender') ?></th>
                <th scope="col"><?= __('Lessmonth') ?></th>
                <th scope="col"><?= __('Pregnant') ?></th>
                <th scope="col"><?= __('Ip Group') ?></th>
                <th scope="col"><?= __('Occupation') ?></th>
                <th scope="col"><?= __('Case Type') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Municipality Id') ?></th>
                <th scope="col"><?= __('Barangay Id') ?></th>
                <th scope="col"><?= __('Sitio Id') ?></th>
                <th scope="col"><?= __('Blood Exam Date') ?></th>
                <th scope="col"><?= __('Blood Exam Result Date') ?></th>
                <th scope="col"><?= __('Chief Complaint') ?></th>
                <th scope="col"><?= __('Temprature') ?></th>
                <th scope="col"><?= __('C Other Complaint') ?></th>
                <th scope="col"><?= __('Other Complaint') ?></th>
                <th scope="col"><?= __('Microscopy') ?></th>
                <th scope="col"><?= __('Rdt') ?></th>
                <th scope="col"><?= __('R Pf') ?></th>
                <th scope="col"><?= __('M Pf') ?></th>
                <th scope="col"><?= __('Pm') ?></th>
                <th scope="col"><?= __('M Pfpv') ?></th>
                <th scope="col"><?= __('R Pfpv') ?></th>
                <th scope="col"><?= __('Pfpm') ?></th>
                <th scope="col"><?= __('Pvpm') ?></th>
                <th scope="col"><?= __('M Pv') ?></th>
                <th scope="col"><?= __('R Pv') ?></th>
                <th scope="col"><?= __('Po') ?></th>
                <th scope="col"><?= __('Nf') ?></th>
                <th scope="col"><?= __('Cdx') ?></th>
                <th scope="col"><?= __('Pfnf') ?></th>
                <th scope="col"><?= __('Rdt No') ?></th>
                <th scope="col"><?= __('Parasaite Blood') ?></th>
                <th scope="col"><?= __('Clinical Diagnosis') ?></th>
                <th scope="col"><?= __('Disease Start Date') ?></th>
                <th scope="col"><?= __('Artemether Tab') ?></th>
                <th scope="col"><?= __('Artemether Qty') ?></th>
                <th scope="col"><?= __('Artemether Started') ?></th>
                <th scope="col"><?= __('Artemether Prep') ?></th>
                <th scope="col"><?= __('Chloroquine Tab') ?></th>
                <th scope="col"><?= __('Chloroquine Qty') ?></th>
                <th scope="col"><?= __('Chloroquine Started') ?></th>
                <th scope="col"><?= __('Chloroquine Prep') ?></th>
                <th scope="col"><?= __('Primaquine Tab') ?></th>
                <th scope="col"><?= __('Primaquine Qty') ?></th>
                <th scope="col"><?= __('Primaquine Started') ?></th>
                <th scope="col"><?= __('Primaquine Prep') ?></th>
                <th scope="col"><?= __('Quinine Tab') ?></th>
                <th scope="col"><?= __('Quinine Qty') ?></th>
                <th scope="col"><?= __('Quinine Started') ?></th>
                <th scope="col"><?= __('Quinine Prep') ?></th>
                <th scope="col"><?= __('Quinine Ampules Tab') ?></th>
                <th scope="col"><?= __('Quinine Ampules Qty') ?></th>
                <th scope="col"><?= __('Quinine Ampules Started') ?></th>
                <th scope="col"><?= __('Quinine Ampules Prep') ?></th>
                <th scope="col"><?= __('Tetracycline Tab') ?></th>
                <th scope="col"><?= __('Tetracycline Qty') ?></th>
                <th scope="col"><?= __('Tetracycline Started') ?></th>
                <th scope="col"><?= __('Tetracycline Prep') ?></th>
                <th scope="col"><?= __('Doxycycline Tab') ?></th>
                <th scope="col"><?= __('Doxycycline Qty') ?></th>
                <th scope="col"><?= __('Doxycycline Started') ?></th>
                <th scope="col"><?= __('Doxycycline Prep') ?></th>
                <th scope="col"><?= __('Clindamycin Tab') ?></th>
                <th scope="col"><?= __('Clindamycin Qty') ?></th>
                <th scope="col"><?= __('Clindamycin Started') ?></th>
                <th scope="col"><?= __('Clindamycin Prep') ?></th>
                <th scope="col"><?= __('No Medecine') ?></th>
                <th scope="col"><?= __('Travel History') ?></th>
                <th scope="col"><?= __('Travel History Place') ?></th>
                <th scope="col"><?= __('History Blood Trans') ?></th>
                <th scope="col"><?= __('History Illness') ?></th>
                <th scope="col"><?= __('Drug Intake Date') ?></th>
                <th scope="col"><?= __('Supervised Initial Intake') ?></th>
                <th scope="col"><?= __('Hw First Name') ?></th>
                <th scope="col"><?= __('Hw Last Name') ?></th>
                <th scope="col"><?= __('Hw Middle Name') ?></th>
                <th scope="col"><?= __('Hw Designation') ?></th>
                <th scope="col"><?= __('Disposition') ?></th>
                <th scope="col"><?= __('Referred To') ?></th>
                <th scope="col"><?= __('Referral Reason') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col"><?= __('Province Text') ?></th>
                <th scope="col"><?= __('Municipality Text') ?></th>
                <th scope="col"><?= __('Barangay Text') ?></th>
                <th scope="col"><?= __('Sitio Text') ?></th>
                <th scope="col"><?= __('Validated') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($smsIncoming->mprf_cases as $mprfCases): ?>
            <tr>
                <td><?= h($mprfCases->id) ?></td>
                <td><?= h($mprfCases->created) ?></td>
                <td><?= h($mprfCases->modified) ?></td>
                <td><?= h($mprfCases->sms_incoming_id) ?></td>
                <td><?= h($mprfCases->user_id) ?></td>
                <td><?= h($mprfCases->source) ?></td>
                <td><?= h($mprfCases->facility_id) ?></td>
                <td><?= h($mprfCases->template_code) ?></td>
                <td><?= h($mprfCases->consult_date) ?></td>
                <td><?= h($mprfCases->death_date) ?></td>
                <td><?= h($mprfCases->first_name) ?></td>
                <td><?= h($mprfCases->last_name) ?></td>
                <td><?= h($mprfCases->middle_name) ?></td>
                <td><?= h($mprfCases->age) ?></td>
                <td><?= h($mprfCases->weight) ?></td>
                <td><?= h($mprfCases->birthdate) ?></td>
                <td><?= h($mprfCases->gender) ?></td>
                <td><?= h($mprfCases->lessmonth) ?></td>
                <td><?= h($mprfCases->pregnant) ?></td>
                <td><?= h($mprfCases->ip_group) ?></td>
                <td><?= h($mprfCases->occupation) ?></td>
                <td><?= h($mprfCases->case_type) ?></td>
                <td><?= h($mprfCases->region_id) ?></td>
                <td><?= h($mprfCases->province_id) ?></td>
                <td><?= h($mprfCases->municipality_id) ?></td>
                <td><?= h($mprfCases->barangay_id) ?></td>
                <td><?= h($mprfCases->sitio_id) ?></td>
                <td><?= h($mprfCases->blood_exam_date) ?></td>
                <td><?= h($mprfCases->blood_exam_result_date) ?></td>
                <td><?= h($mprfCases->chief_complaint) ?></td>
                <td><?= h($mprfCases->temprature) ?></td>
                <td><?= h($mprfCases->c_other_complaint) ?></td>
                <td><?= h($mprfCases->other_complaint) ?></td>
                <td><?= h($mprfCases->microscopy) ?></td>
                <td><?= h($mprfCases->rdt) ?></td>
                <td><?= h($mprfCases->r_pf) ?></td>
                <td><?= h($mprfCases->m_pf) ?></td>
                <td><?= h($mprfCases->pm) ?></td>
                <td><?= h($mprfCases->m_pfpv) ?></td>
                <td><?= h($mprfCases->r_pfpv) ?></td>
                <td><?= h($mprfCases->pfpm) ?></td>
                <td><?= h($mprfCases->pvpm) ?></td>
                <td><?= h($mprfCases->m_pv) ?></td>
                <td><?= h($mprfCases->r_pv) ?></td>
                <td><?= h($mprfCases->po) ?></td>
                <td><?= h($mprfCases->nf) ?></td>
                <td><?= h($mprfCases->cdx) ?></td>
                <td><?= h($mprfCases->pfnf) ?></td>
                <td><?= h($mprfCases->rdt_no) ?></td>
                <td><?= h($mprfCases->parasaite_blood) ?></td>
                <td><?= h($mprfCases->clinical_diagnosis) ?></td>
                <td><?= h($mprfCases->disease_start_date) ?></td>
                <td><?= h($mprfCases->artemether_tab) ?></td>
                <td><?= h($mprfCases->artemether_qty) ?></td>
                <td><?= h($mprfCases->artemether_started) ?></td>
                <td><?= h($mprfCases->artemether_prep) ?></td>
                <td><?= h($mprfCases->chloroquine_tab) ?></td>
                <td><?= h($mprfCases->chloroquine_qty) ?></td>
                <td><?= h($mprfCases->chloroquine_started) ?></td>
                <td><?= h($mprfCases->chloroquine_prep) ?></td>
                <td><?= h($mprfCases->primaquine_tab) ?></td>
                <td><?= h($mprfCases->primaquine_qty) ?></td>
                <td><?= h($mprfCases->primaquine_started) ?></td>
                <td><?= h($mprfCases->primaquine_prep) ?></td>
                <td><?= h($mprfCases->quinine_tab) ?></td>
                <td><?= h($mprfCases->quinine_qty) ?></td>
                <td><?= h($mprfCases->quinine_started) ?></td>
                <td><?= h($mprfCases->quinine_prep) ?></td>
                <td><?= h($mprfCases->quinine_ampules_tab) ?></td>
                <td><?= h($mprfCases->quinine_ampules_qty) ?></td>
                <td><?= h($mprfCases->quinine_ampules_started) ?></td>
                <td><?= h($mprfCases->quinine_ampules_prep) ?></td>
                <td><?= h($mprfCases->tetracycline_tab) ?></td>
                <td><?= h($mprfCases->tetracycline_qty) ?></td>
                <td><?= h($mprfCases->tetracycline_started) ?></td>
                <td><?= h($mprfCases->tetracycline_prep) ?></td>
                <td><?= h($mprfCases->doxycycline_tab) ?></td>
                <td><?= h($mprfCases->doxycycline_qty) ?></td>
                <td><?= h($mprfCases->doxycycline_started) ?></td>
                <td><?= h($mprfCases->doxycycline_prep) ?></td>
                <td><?= h($mprfCases->clindamycin_tab) ?></td>
                <td><?= h($mprfCases->clindamycin_qty) ?></td>
                <td><?= h($mprfCases->clindamycin_started) ?></td>
                <td><?= h($mprfCases->clindamycin_prep) ?></td>
                <td><?= h($mprfCases->no_medecine) ?></td>
                <td><?= h($mprfCases->travel_history) ?></td>
                <td><?= h($mprfCases->travel_history_place) ?></td>
                <td><?= h($mprfCases->history_blood_trans) ?></td>
                <td><?= h($mprfCases->history_illness) ?></td>
                <td><?= h($mprfCases->drug_intake_date) ?></td>
                <td><?= h($mprfCases->supervised_initial_intake) ?></td>
                <td><?= h($mprfCases->hw_first_name) ?></td>
                <td><?= h($mprfCases->hw_last_name) ?></td>
                <td><?= h($mprfCases->hw_middle_name) ?></td>
                <td><?= h($mprfCases->hw_designation) ?></td>
                <td><?= h($mprfCases->disposition) ?></td>
                <td><?= h($mprfCases->referred_to) ?></td>
                <td><?= h($mprfCases->referral_reason) ?></td>
                <td><?= h($mprfCases->remarks) ?></td>
                <td><?= h($mprfCases->province_text) ?></td>
                <td><?= h($mprfCases->municipality_text) ?></td>
                <td><?= h($mprfCases->barangay_text) ?></td>
                <td><?= h($mprfCases->sitio_text) ?></td>
                <td><?= h($mprfCases->validated) ?></td>
                <td><?= h($mprfCases->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MprfCases', 'action' => 'view', $mprfCases->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MprfCases', 'action' => 'edit', $mprfCases->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MprfCases', 'action' => 'delete', $mprfCases->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mprfCases->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sms Outgoings') ?></h4>
        <?php if (!empty($smsIncoming->sms_outgoings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Sms Code') ?></th>
                <th scope="col"><?= __('Group Id') ?></th>
                <th scope="col"><?= __('Source') ?></th>
                <th scope="col"><?= __('Sms Incoming Id') ?></th>
                <th scope="col"><?= __('Receiver No') ?></th>
                <th scope="col"><?= __('Receiver Id') ?></th>
                <th scope="col"><?= __('Sender No') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Message') ?></th>
                <th scope="col"><?= __('Error') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Box') ?></th>
                <th scope="col"><?= __('Network') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('View') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($smsIncoming->sms_outgoings as $smsOutgoings): ?>
            <tr>
                <td><?= h($smsOutgoings->id) ?></td>
                <td><?= h($smsOutgoings->created) ?></td>
                <td><?= h($smsOutgoings->modified) ?></td>
                <td><?= h($smsOutgoings->sms_code) ?></td>
                <td><?= h($smsOutgoings->group_id) ?></td>
                <td><?= h($smsOutgoings->source) ?></td>
                <td><?= h($smsOutgoings->sms_incoming_id) ?></td>
                <td><?= h($smsOutgoings->receiver_no) ?></td>
                <td><?= h($smsOutgoings->receiver_id) ?></td>
                <td><?= h($smsOutgoings->sender_no) ?></td>
                <td><?= h($smsOutgoings->user_id) ?></td>
                <td><?= h($smsOutgoings->message) ?></td>
                <td><?= h($smsOutgoings->error) ?></td>
                <td><?= h($smsOutgoings->status) ?></td>
                <td><?= h($smsOutgoings->box) ?></td>
                <td><?= h($smsOutgoings->network) ?></td>
                <td><?= h($smsOutgoings->type) ?></td>
                <td><?= h($smsOutgoings->view) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SmsOutgoings', 'action' => 'view', $smsOutgoings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SmsOutgoings', 'action' => 'edit', $smsOutgoings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SmsOutgoings', 'action' => 'delete', $smsOutgoings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsOutgoings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
