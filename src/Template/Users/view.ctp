<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Designations'), ['controller' => 'Designations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Designation'), ['controller' => 'Designations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Facilities'), ['controller' => 'Facilities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Facility'), ['controller' => 'Facilities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Barangays'), ['controller' => 'Barangays', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Barangay'), ['controller' => 'Barangays', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Company Types'), ['controller' => 'CompanyTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company Type'), ['controller' => 'CompanyTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Distributions'), ['controller' => 'Distributions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Distribution'), ['controller' => 'Distributions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Item Details'), ['controller' => 'ItemDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item Detail'), ['controller' => 'ItemDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mprf Cases'), ['controller' => 'MprfCases', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mprf Case'), ['controller' => 'MprfCases', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mprf Excels'), ['controller' => 'MprfExcels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mprf Excel'), ['controller' => 'MprfExcels', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Networks'), ['controller' => 'Networks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Network'), ['controller' => 'Networks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sitios'), ['controller' => 'Sitios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sitio'), ['controller' => 'Sitios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['controller' => 'SmsIncomings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['controller' => 'SmsIncomings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Outgoings'), ['controller' => 'SmsOutgoings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Outgoing'), ['controller' => 'SmsOutgoings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Template Notifications'), ['controller' => 'TemplateNotifications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template Notification'), ['controller' => 'TemplateNotifications', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Contacts'), ['controller' => 'UserContacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Contact'), ['controller' => 'UserContacts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($user->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Middle Name') ?></th>
            <td><?= h($user->middle_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile No One') ?></th>
            <td><?= h($user->mobile_no_one) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile No Two') ?></th>
            <td><?= h($user->mobile_no_two) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile No Three') ?></th>
            <td><?= h($user->mobile_no_three) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Designation') ?></th>
            <td><?= $user->has('designation') ? $this->Html->link($user->designation->name, ['controller' => 'Designations', 'action' => 'view', $user->designation->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Blocked') ?></th>
            <td><?= h($user->blocked) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($user->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Source') ?></th>
            <td><?= h($user->source) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Facility Id') ?></th>
            <td><?= $this->Number->format($user->facility_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activated') ?></th>
            <td><?= $this->Number->format($user->activated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Facilities') ?></h4>
        <?php if (!empty($user->facilities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Facility Category Id') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Municipality Id') ?></th>
                <th scope="col"><?= __('Barangay Id') ?></th>
                <th scope="col"><?= __('Sitio Id') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Cut Off Date') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->facilities as $facilities): ?>
            <tr>
                <td><?= h($facilities->id) ?></td>
                <td><?= h($facilities->created) ?></td>
                <td><?= h($facilities->modified) ?></td>
                <td><?= h($facilities->facility_category_id) ?></td>
                <td><?= h($facilities->region_id) ?></td>
                <td><?= h($facilities->province_id) ?></td>
                <td><?= h($facilities->municipality_id) ?></td>
                <td><?= h($facilities->barangay_id) ?></td>
                <td><?= h($facilities->sitio_id) ?></td>
                <td><?= h($facilities->description) ?></td>
                <td><?= h($facilities->remarks) ?></td>
                <td><?= h($facilities->user_id) ?></td>
                <td><?= h($facilities->cut_off_date) ?></td>
                <td><?= h($facilities->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Facilities', 'action' => 'view', $facilities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Facilities', 'action' => 'edit', $facilities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Facilities', 'action' => 'delete', $facilities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $facilities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Barangays') ?></h4>
        <?php if (!empty($user->barangays)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Barangay Code') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Region Code') ?></th>
                <th scope="col"><?= __('Province Code') ?></th>
                <th scope="col"><?= __('Municipality Code') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Municipality Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Modified User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->barangays as $barangays): ?>
            <tr>
                <td><?= h($barangays->id) ?></td>
                <td><?= h($barangays->barangay_code) ?></td>
                <td><?= h($barangays->description) ?></td>
                <td><?= h($barangays->region_code) ?></td>
                <td><?= h($barangays->province_code) ?></td>
                <td><?= h($barangays->municipality_code) ?></td>
                <td><?= h($barangays->region_id) ?></td>
                <td><?= h($barangays->province_id) ?></td>
                <td><?= h($barangays->municipality_id) ?></td>
                <td><?= h($barangays->user_id) ?></td>
                <td><?= h($barangays->modified_user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Barangays', 'action' => 'view', $barangays->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Barangays', 'action' => 'edit', $barangays->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Barangays', 'action' => 'delete', $barangays->id], ['confirm' => __('Are you sure you want to delete # {0}?', $barangays->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Company Types') ?></h4>
        <?php if (!empty($user->company_types)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->company_types as $companyTypes): ?>
            <tr>
                <td><?= h($companyTypes->id) ?></td>
                <td><?= h($companyTypes->name) ?></td>
                <td><?= h($companyTypes->created) ?></td>
                <td><?= h($companyTypes->modified) ?></td>
                <td><?= h($companyTypes->user_id) ?></td>
                <td><?= h($companyTypes->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CompanyTypes', 'action' => 'view', $companyTypes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CompanyTypes', 'action' => 'edit', $companyTypes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CompanyTypes', 'action' => 'delete', $companyTypes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $companyTypes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Distributions') ?></h4>
        <?php if (!empty($user->distributions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->distributions as $distributions): ?>
            <tr>
                <td><?= h($distributions->id) ?></td>
                <td><?= h($distributions->created) ?></td>
                <td><?= h($distributions->modified) ?></td>
                <td><?= h($distributions->name) ?></td>
                <td><?= h($distributions->user_id) ?></td>
                <td><?= h($distributions->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Distributions', 'action' => 'view', $distributions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Distributions', 'action' => 'edit', $distributions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Distributions', 'action' => 'delete', $distributions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $distributions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Item Details') ?></h4>
        <?php if (!empty($user->item_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Item Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Item Unit Id') ?></th>
                <th scope="col"><?= __('Sku') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->item_details as $itemDetails): ?>
            <tr>
                <td><?= h($itemDetails->id) ?></td>
                <td><?= h($itemDetails->created) ?></td>
                <td><?= h($itemDetails->modified) ?></td>
                <td><?= h($itemDetails->item_id) ?></td>
                <td><?= h($itemDetails->user_id) ?></td>
                <td><?= h($itemDetails->code) ?></td>
                <td><?= h($itemDetails->quantity) ?></td>
                <td><?= h($itemDetails->item_unit_id) ?></td>
                <td><?= h($itemDetails->sku) ?></td>
                <td><?= h($itemDetails->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ItemDetails', 'action' => 'view', $itemDetails->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ItemDetails', 'action' => 'edit', $itemDetails->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ItemDetails', 'action' => 'delete', $itemDetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemDetails->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Items') ?></h4>
        <?php if (!empty($user->items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Template Code') ?></th>
                <th scope="col"><?= __('Source') ?></th>
                <th scope="col"><?= __('Report Date') ?></th>
                <th scope="col"><?= __('Facility Id') ?></th>
                <th scope="col"><?= __('Sms Incoming Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col"><?= __('Al Qty') ?></th>
                <th scope="col"><?= __('Pq Qty') ?></th>
                <th scope="col"><?= __('Cq Qty') ?></th>
                <th scope="col"><?= __('Qt Qty') ?></th>
                <th scope="col"><?= __('Qa Qty') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->items as $items): ?>
            <tr>
                <td><?= h($items->id) ?></td>
                <td><?= h($items->template_code) ?></td>
                <td><?= h($items->source) ?></td>
                <td><?= h($items->report_date) ?></td>
                <td><?= h($items->facility_id) ?></td>
                <td><?= h($items->sms_incoming_id) ?></td>
                <td><?= h($items->user_id) ?></td>
                <td><?= h($items->remarks) ?></td>
                <td><?= h($items->al_qty) ?></td>
                <td><?= h($items->pq_qty) ?></td>
                <td><?= h($items->cq_qty) ?></td>
                <td><?= h($items->qt_qty) ?></td>
                <td><?= h($items->qa_qty) ?></td>
                <td><?= h($items->created) ?></td>
                <td><?= h($items->modified) ?></td>
                <td><?= h($items->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Items', 'action' => 'view', $items->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Items', 'action' => 'edit', $items->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Items', 'action' => 'delete', $items->id], ['confirm' => __('Are you sure you want to delete # {0}?', $items->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Mprf Cases') ?></h4>
        <?php if (!empty($user->mprf_cases)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Sms Incoming Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Source') ?></th>
                <th scope="col"><?= __('Facility Id') ?></th>
                <th scope="col"><?= __('Template Code') ?></th>
                <th scope="col"><?= __('Consult Date') ?></th>
                <th scope="col"><?= __('Death Date') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Middle Name') ?></th>
                <th scope="col"><?= __('Age') ?></th>
                <th scope="col"><?= __('Weight') ?></th>
                <th scope="col"><?= __('Birthdate') ?></th>
                <th scope="col"><?= __('Gender') ?></th>
                <th scope="col"><?= __('Lessmonth') ?></th>
                <th scope="col"><?= __('Pregnant') ?></th>
                <th scope="col"><?= __('Ip Group') ?></th>
                <th scope="col"><?= __('Occupation') ?></th>
                <th scope="col"><?= __('Case Type') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Municipality Id') ?></th>
                <th scope="col"><?= __('Barangay Id') ?></th>
                <th scope="col"><?= __('Sitio Id') ?></th>
                <th scope="col"><?= __('Blood Exam Date') ?></th>
                <th scope="col"><?= __('Blood Exam Result Date') ?></th>
                <th scope="col"><?= __('Chief Complaint') ?></th>
                <th scope="col"><?= __('Temprature') ?></th>
                <th scope="col"><?= __('C Other Complaint') ?></th>
                <th scope="col"><?= __('Other Complaint') ?></th>
                <th scope="col"><?= __('Microscopy') ?></th>
                <th scope="col"><?= __('Rdt') ?></th>
                <th scope="col"><?= __('R Pf') ?></th>
                <th scope="col"><?= __('M Pf') ?></th>
                <th scope="col"><?= __('Pm') ?></th>
                <th scope="col"><?= __('M Pfpv') ?></th>
                <th scope="col"><?= __('R Pfpv') ?></th>
                <th scope="col"><?= __('Pfpm') ?></th>
                <th scope="col"><?= __('Pvpm') ?></th>
                <th scope="col"><?= __('M Pv') ?></th>
                <th scope="col"><?= __('R Pv') ?></th>
                <th scope="col"><?= __('Po') ?></th>
                <th scope="col"><?= __('Nf') ?></th>
                <th scope="col"><?= __('Cdx') ?></th>
                <th scope="col"><?= __('Pfnf') ?></th>
                <th scope="col"><?= __('Rdt No') ?></th>
                <th scope="col"><?= __('Parasaite Blood') ?></th>
                <th scope="col"><?= __('Clinical Diagnosis') ?></th>
                <th scope="col"><?= __('Disease Start Date') ?></th>
                <th scope="col"><?= __('Artemether Tab') ?></th>
                <th scope="col"><?= __('Artemether Qty') ?></th>
                <th scope="col"><?= __('Artemether Started') ?></th>
                <th scope="col"><?= __('Artemether Prep') ?></th>
                <th scope="col"><?= __('Chloroquine Tab') ?></th>
                <th scope="col"><?= __('Chloroquine Qty') ?></th>
                <th scope="col"><?= __('Chloroquine Started') ?></th>
                <th scope="col"><?= __('Chloroquine Prep') ?></th>
                <th scope="col"><?= __('Primaquine Tab') ?></th>
                <th scope="col"><?= __('Primaquine Qty') ?></th>
                <th scope="col"><?= __('Primaquine Started') ?></th>
                <th scope="col"><?= __('Primaquine Prep') ?></th>
                <th scope="col"><?= __('Quinine Tab') ?></th>
                <th scope="col"><?= __('Quinine Qty') ?></th>
                <th scope="col"><?= __('Quinine Started') ?></th>
                <th scope="col"><?= __('Quinine Prep') ?></th>
                <th scope="col"><?= __('Quinine Ampules Tab') ?></th>
                <th scope="col"><?= __('Quinine Ampules Qty') ?></th>
                <th scope="col"><?= __('Quinine Ampules Started') ?></th>
                <th scope="col"><?= __('Quinine Ampules Prep') ?></th>
                <th scope="col"><?= __('Tetracycline Tab') ?></th>
                <th scope="col"><?= __('Tetracycline Qty') ?></th>
                <th scope="col"><?= __('Tetracycline Started') ?></th>
                <th scope="col"><?= __('Tetracycline Prep') ?></th>
                <th scope="col"><?= __('Doxycycline Tab') ?></th>
                <th scope="col"><?= __('Doxycycline Qty') ?></th>
                <th scope="col"><?= __('Doxycycline Started') ?></th>
                <th scope="col"><?= __('Doxycycline Prep') ?></th>
                <th scope="col"><?= __('Clindamycin Tab') ?></th>
                <th scope="col"><?= __('Clindamycin Qty') ?></th>
                <th scope="col"><?= __('Clindamycin Started') ?></th>
                <th scope="col"><?= __('Clindamycin Prep') ?></th>
                <th scope="col"><?= __('No Medecine') ?></th>
                <th scope="col"><?= __('Travel History') ?></th>
                <th scope="col"><?= __('Travel History Place') ?></th>
                <th scope="col"><?= __('History Blood Trans') ?></th>
                <th scope="col"><?= __('History Illness') ?></th>
                <th scope="col"><?= __('Drug Intake Date') ?></th>
                <th scope="col"><?= __('Supervised Initial Intake') ?></th>
                <th scope="col"><?= __('Hw First Name') ?></th>
                <th scope="col"><?= __('Hw Last Name') ?></th>
                <th scope="col"><?= __('Hw Middle Name') ?></th>
                <th scope="col"><?= __('Hw Designation') ?></th>
                <th scope="col"><?= __('Disposition') ?></th>
                <th scope="col"><?= __('Referred To') ?></th>
                <th scope="col"><?= __('Referral Reason') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col"><?= __('Province Text') ?></th>
                <th scope="col"><?= __('Municipality Text') ?></th>
                <th scope="col"><?= __('Barangay Text') ?></th>
                <th scope="col"><?= __('Sitio Text') ?></th>
                <th scope="col"><?= __('Validated') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->mprf_cases as $mprfCases): ?>
            <tr>
                <td><?= h($mprfCases->id) ?></td>
                <td><?= h($mprfCases->created) ?></td>
                <td><?= h($mprfCases->modified) ?></td>
                <td><?= h($mprfCases->sms_incoming_id) ?></td>
                <td><?= h($mprfCases->user_id) ?></td>
                <td><?= h($mprfCases->source) ?></td>
                <td><?= h($mprfCases->facility_id) ?></td>
                <td><?= h($mprfCases->template_code) ?></td>
                <td><?= h($mprfCases->consult_date) ?></td>
                <td><?= h($mprfCases->death_date) ?></td>
                <td><?= h($mprfCases->first_name) ?></td>
                <td><?= h($mprfCases->last_name) ?></td>
                <td><?= h($mprfCases->middle_name) ?></td>
                <td><?= h($mprfCases->age) ?></td>
                <td><?= h($mprfCases->weight) ?></td>
                <td><?= h($mprfCases->birthdate) ?></td>
                <td><?= h($mprfCases->gender) ?></td>
                <td><?= h($mprfCases->lessmonth) ?></td>
                <td><?= h($mprfCases->pregnant) ?></td>
                <td><?= h($mprfCases->ip_group) ?></td>
                <td><?= h($mprfCases->occupation) ?></td>
                <td><?= h($mprfCases->case_type) ?></td>
                <td><?= h($mprfCases->region_id) ?></td>
                <td><?= h($mprfCases->province_id) ?></td>
                <td><?= h($mprfCases->municipality_id) ?></td>
                <td><?= h($mprfCases->barangay_id) ?></td>
                <td><?= h($mprfCases->sitio_id) ?></td>
                <td><?= h($mprfCases->blood_exam_date) ?></td>
                <td><?= h($mprfCases->blood_exam_result_date) ?></td>
                <td><?= h($mprfCases->chief_complaint) ?></td>
                <td><?= h($mprfCases->temprature) ?></td>
                <td><?= h($mprfCases->c_other_complaint) ?></td>
                <td><?= h($mprfCases->other_complaint) ?></td>
                <td><?= h($mprfCases->microscopy) ?></td>
                <td><?= h($mprfCases->rdt) ?></td>
                <td><?= h($mprfCases->r_pf) ?></td>
                <td><?= h($mprfCases->m_pf) ?></td>
                <td><?= h($mprfCases->pm) ?></td>
                <td><?= h($mprfCases->m_pfpv) ?></td>
                <td><?= h($mprfCases->r_pfpv) ?></td>
                <td><?= h($mprfCases->pfpm) ?></td>
                <td><?= h($mprfCases->pvpm) ?></td>
                <td><?= h($mprfCases->m_pv) ?></td>
                <td><?= h($mprfCases->r_pv) ?></td>
                <td><?= h($mprfCases->po) ?></td>
                <td><?= h($mprfCases->nf) ?></td>
                <td><?= h($mprfCases->cdx) ?></td>
                <td><?= h($mprfCases->pfnf) ?></td>
                <td><?= h($mprfCases->rdt_no) ?></td>
                <td><?= h($mprfCases->parasaite_blood) ?></td>
                <td><?= h($mprfCases->clinical_diagnosis) ?></td>
                <td><?= h($mprfCases->disease_start_date) ?></td>
                <td><?= h($mprfCases->artemether_tab) ?></td>
                <td><?= h($mprfCases->artemether_qty) ?></td>
                <td><?= h($mprfCases->artemether_started) ?></td>
                <td><?= h($mprfCases->artemether_prep) ?></td>
                <td><?= h($mprfCases->chloroquine_tab) ?></td>
                <td><?= h($mprfCases->chloroquine_qty) ?></td>
                <td><?= h($mprfCases->chloroquine_started) ?></td>
                <td><?= h($mprfCases->chloroquine_prep) ?></td>
                <td><?= h($mprfCases->primaquine_tab) ?></td>
                <td><?= h($mprfCases->primaquine_qty) ?></td>
                <td><?= h($mprfCases->primaquine_started) ?></td>
                <td><?= h($mprfCases->primaquine_prep) ?></td>
                <td><?= h($mprfCases->quinine_tab) ?></td>
                <td><?= h($mprfCases->quinine_qty) ?></td>
                <td><?= h($mprfCases->quinine_started) ?></td>
                <td><?= h($mprfCases->quinine_prep) ?></td>
                <td><?= h($mprfCases->quinine_ampules_tab) ?></td>
                <td><?= h($mprfCases->quinine_ampules_qty) ?></td>
                <td><?= h($mprfCases->quinine_ampules_started) ?></td>
                <td><?= h($mprfCases->quinine_ampules_prep) ?></td>
                <td><?= h($mprfCases->tetracycline_tab) ?></td>
                <td><?= h($mprfCases->tetracycline_qty) ?></td>
                <td><?= h($mprfCases->tetracycline_started) ?></td>
                <td><?= h($mprfCases->tetracycline_prep) ?></td>
                <td><?= h($mprfCases->doxycycline_tab) ?></td>
                <td><?= h($mprfCases->doxycycline_qty) ?></td>
                <td><?= h($mprfCases->doxycycline_started) ?></td>
                <td><?= h($mprfCases->doxycycline_prep) ?></td>
                <td><?= h($mprfCases->clindamycin_tab) ?></td>
                <td><?= h($mprfCases->clindamycin_qty) ?></td>
                <td><?= h($mprfCases->clindamycin_started) ?></td>
                <td><?= h($mprfCases->clindamycin_prep) ?></td>
                <td><?= h($mprfCases->no_medecine) ?></td>
                <td><?= h($mprfCases->travel_history) ?></td>
                <td><?= h($mprfCases->travel_history_place) ?></td>
                <td><?= h($mprfCases->history_blood_trans) ?></td>
                <td><?= h($mprfCases->history_illness) ?></td>
                <td><?= h($mprfCases->drug_intake_date) ?></td>
                <td><?= h($mprfCases->supervised_initial_intake) ?></td>
                <td><?= h($mprfCases->hw_first_name) ?></td>
                <td><?= h($mprfCases->hw_last_name) ?></td>
                <td><?= h($mprfCases->hw_middle_name) ?></td>
                <td><?= h($mprfCases->hw_designation) ?></td>
                <td><?= h($mprfCases->disposition) ?></td>
                <td><?= h($mprfCases->referred_to) ?></td>
                <td><?= h($mprfCases->referral_reason) ?></td>
                <td><?= h($mprfCases->remarks) ?></td>
                <td><?= h($mprfCases->province_text) ?></td>
                <td><?= h($mprfCases->municipality_text) ?></td>
                <td><?= h($mprfCases->barangay_text) ?></td>
                <td><?= h($mprfCases->sitio_text) ?></td>
                <td><?= h($mprfCases->validated) ?></td>
                <td><?= h($mprfCases->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MprfCases', 'action' => 'view', $mprfCases->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MprfCases', 'action' => 'edit', $mprfCases->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MprfCases', 'action' => 'delete', $mprfCases->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mprfCases->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Mprf Excels') ?></h4>
        <?php if (!empty($user->mprf_excels)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Filename') ?></th>
                <th scope="col"><?= __('Path') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->mprf_excels as $mprfExcels): ?>
            <tr>
                <td><?= h($mprfExcels->id) ?></td>
                <td><?= h($mprfExcels->created) ?></td>
                <td><?= h($mprfExcels->user_id) ?></td>
                <td><?= h($mprfExcels->filename) ?></td>
                <td><?= h($mprfExcels->path) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MprfExcels', 'action' => 'view', $mprfExcels->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MprfExcels', 'action' => 'edit', $mprfExcels->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MprfExcels', 'action' => 'delete', $mprfExcels->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mprfExcels->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Networks') ?></h4>
        <?php if (!empty($user->networks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Prefix') ?></th>
                <th scope="col"><?= __('Network') ?></th>
                <th scope="col"><?= __('Sub Network') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->networks as $networks): ?>
            <tr>
                <td><?= h($networks->id) ?></td>
                <td><?= h($networks->prefix) ?></td>
                <td><?= h($networks->network) ?></td>
                <td><?= h($networks->sub_network) ?></td>
                <td><?= h($networks->created) ?></td>
                <td><?= h($networks->modified) ?></td>
                <td><?= h($networks->user_id) ?></td>
                <td><?= h($networks->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Networks', 'action' => 'view', $networks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Networks', 'action' => 'edit', $networks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Networks', 'action' => 'delete', $networks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $networks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sitios') ?></h4>
        <?php if (!empty($user->sitios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Barangay Code') ?></th>
                <th scope="col"><?= __('Municipality Code') ?></th>
                <th scope="col"><?= __('Province Code') ?></th>
                <th scope="col"><?= __('Region Code') ?></th>
                <th scope="col"><?= __('Barangay Id') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Municipality Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->sitios as $sitios): ?>
            <tr>
                <td><?= h($sitios->id) ?></td>
                <td><?= h($sitios->created) ?></td>
                <td><?= h($sitios->modified) ?></td>
                <td><?= h($sitios->user_id) ?></td>
                <td><?= h($sitios->description) ?></td>
                <td><?= h($sitios->barangay_code) ?></td>
                <td><?= h($sitios->municipality_code) ?></td>
                <td><?= h($sitios->province_code) ?></td>
                <td><?= h($sitios->region_code) ?></td>
                <td><?= h($sitios->barangay_id) ?></td>
                <td><?= h($sitios->region_id) ?></td>
                <td><?= h($sitios->province_id) ?></td>
                <td><?= h($sitios->municipality_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Sitios', 'action' => 'view', $sitios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Sitios', 'action' => 'edit', $sitios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sitios', 'action' => 'delete', $sitios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sitios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sms Incomings') ?></h4>
        <?php if (!empty($user->sms_incomings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Template Code') ?></th>
                <th scope="col"><?= __('Sms Code') ?></th>
                <th scope="col"><?= __('Sender No') ?></th>
                <th scope="col"><?= __('Phone No') ?></th>
                <th scope="col"><?= __('Message') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Message Type') ?></th>
                <th scope="col"><?= __('Error Message') ?></th>
                <th scope="col"><?= __('Validate') ?></th>
                <th scope="col"><?= __('Box') ?></th>
                <th scope="col"><?= __('View') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->sms_incomings as $smsIncomings): ?>
            <tr>
                <td><?= h($smsIncomings->id) ?></td>
                <td><?= h($smsIncomings->created) ?></td>
                <td><?= h($smsIncomings->modified) ?></td>
                <td><?= h($smsIncomings->user_id) ?></td>
                <td><?= h($smsIncomings->template_code) ?></td>
                <td><?= h($smsIncomings->sms_code) ?></td>
                <td><?= h($smsIncomings->sender_no) ?></td>
                <td><?= h($smsIncomings->phone_no) ?></td>
                <td><?= h($smsIncomings->message) ?></td>
                <td><?= h($smsIncomings->status) ?></td>
                <td><?= h($smsIncomings->message_type) ?></td>
                <td><?= h($smsIncomings->error_message) ?></td>
                <td><?= h($smsIncomings->validate) ?></td>
                <td><?= h($smsIncomings->box) ?></td>
                <td><?= h($smsIncomings->view) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SmsIncomings', 'action' => 'view', $smsIncomings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SmsIncomings', 'action' => 'edit', $smsIncomings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SmsIncomings', 'action' => 'delete', $smsIncomings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsIncomings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sms Outgoings') ?></h4>
        <?php if (!empty($user->sms_outgoings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Sms Code') ?></th>
                <th scope="col"><?= __('Group Id') ?></th>
                <th scope="col"><?= __('Source') ?></th>
                <th scope="col"><?= __('Sms Incoming Id') ?></th>
                <th scope="col"><?= __('Receiver No') ?></th>
                <th scope="col"><?= __('Receiver Id') ?></th>
                <th scope="col"><?= __('Sender No') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Message') ?></th>
                <th scope="col"><?= __('Error') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Box') ?></th>
                <th scope="col"><?= __('Network') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('View') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->sms_outgoings as $smsOutgoings): ?>
            <tr>
                <td><?= h($smsOutgoings->id) ?></td>
                <td><?= h($smsOutgoings->created) ?></td>
                <td><?= h($smsOutgoings->modified) ?></td>
                <td><?= h($smsOutgoings->sms_code) ?></td>
                <td><?= h($smsOutgoings->group_id) ?></td>
                <td><?= h($smsOutgoings->source) ?></td>
                <td><?= h($smsOutgoings->sms_incoming_id) ?></td>
                <td><?= h($smsOutgoings->receiver_no) ?></td>
                <td><?= h($smsOutgoings->receiver_id) ?></td>
                <td><?= h($smsOutgoings->sender_no) ?></td>
                <td><?= h($smsOutgoings->user_id) ?></td>
                <td><?= h($smsOutgoings->message) ?></td>
                <td><?= h($smsOutgoings->error) ?></td>
                <td><?= h($smsOutgoings->status) ?></td>
                <td><?= h($smsOutgoings->box) ?></td>
                <td><?= h($smsOutgoings->network) ?></td>
                <td><?= h($smsOutgoings->type) ?></td>
                <td><?= h($smsOutgoings->view) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SmsOutgoings', 'action' => 'view', $smsOutgoings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SmsOutgoings', 'action' => 'edit', $smsOutgoings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SmsOutgoings', 'action' => 'delete', $smsOutgoings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsOutgoings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Template Notifications') ?></h4>
        <?php if (!empty($user->template_notifications)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Template Id') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->template_notifications as $templateNotifications): ?>
            <tr>
                <td><?= h($templateNotifications->id) ?></td>
                <td><?= h($templateNotifications->created) ?></td>
                <td><?= h($templateNotifications->user_id) ?></td>
                <td><?= h($templateNotifications->template_id) ?></td>
                <td><?= h($templateNotifications->remarks) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TemplateNotifications', 'action' => 'view', $templateNotifications->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TemplateNotifications', 'action' => 'edit', $templateNotifications->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TemplateNotifications', 'action' => 'delete', $templateNotifications->id], ['confirm' => __('Are you sure you want to delete # {0}?', $templateNotifications->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Contacts') ?></h4>
        <?php if (!empty($user->user_contacts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Value') ?></th>
                <th scope="col"><?= __('Prime Setup') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->user_contacts as $userContacts): ?>
            <tr>
                <td><?= h($userContacts->id) ?></td>
                <td><?= h($userContacts->created) ?></td>
                <td><?= h($userContacts->modified) ?></td>
                <td><?= h($userContacts->user_id) ?></td>
                <td><?= h($userContacts->type) ?></td>
                <td><?= h($userContacts->value) ?></td>
                <td><?= h($userContacts->prime_setup) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserContacts', 'action' => 'view', $userContacts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserContacts', 'action' => 'edit', $userContacts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserContacts', 'action' => 'delete', $userContacts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userContacts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
