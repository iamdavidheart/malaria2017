
<section class="main-container">

    <div class="container">

        <div class="row">


            <div class="col-md-8">
                <button class="btn btn-animated btn-gray-transparent btn-sm" id="activate">Activate <i class="fa fa-check"></i></button>
                <button class="btn btn-animated btn-gray-transparent btn-sm" id="deactivate">Deactivate <i class="fa fa-close"></i></button>
                <div id="loading"></div>
            </div>


            <div class="users col-md-12">
                <h3><?= __('Users') ?></h3>
                <div class="table-responsive">
                <table class="table table-bordered" id="users">
                    <thead>
                        <tr>
                            <th></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('ACT') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('ROLE') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('CODE') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('NAME') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('FACILITY') ?></th>

                            <th scope="col" class="actions text-center"><?= __('ACTION')  ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                        <tr>
                        

                            <td ><?=$user->id?> </td>
                            <td >
                            <?php 
                            if($user->activated == 1){
                                echo '<label class="label label-success">ACT</label>';
                            }else{
                                echo '<label class="label label-danger">DCT</label>';
                            }
                            ?>
                            </td>
          
                            <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
                            <td><?= strtoupper($user->code) ?></td>
                            <td><?= h($user->first_name) ?> <?= h($user->last_name) ?></td>
                            <td>
                                
                                <?php

                                 if(!empty($user->facility)){
                             
                                    echo $user->facility->province->description. ', '.$user->facility->municipality->description.',';
                                    echo (!empty($user->facility->barangay))? $user->facility->barangay->description.', ' : 'No Barangay,' ;
                                    echo (!empty($user->facility->sitio))? $user->facility->sitio->description.', ' : ' No Sitio, ' ;
                                 }else{
                                      echo 'No Facility';
                                 }
                 
                                ?>
                            </td>
                
                            <td class="actions text-center">
                        


                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-edit"></i> Edit'),
                                        ['action' => 'edit', $user->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm ',
                                            'escape' => false
                                        ]);
                                    ?>

                                    <?= $this->Form->postLink( __('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id],
                                        ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->full_name.'('.$user->code.')')]  ); ?>


                         
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                </div>
                <div class="paginator hide">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            

        </div>
    </div>
</div>

<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
   <!--  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css"/> -->
   <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script> -->
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {



            var table = $('#users').DataTable({
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                            selectRow: true
                            }
                        }
                    ],
                        select: {
                        style: 'multi'
                    },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });






            //ACTIVATION
           $('#activate').on('click', function(e){
            
                var userIds = [];
                var rows_selected = table.column(0).checkboxes.selected();

                $.each(rows_selected, function(index, rowId){
                     userIds.push(rowId);
                });
                if(userIds.length == 0) {
                   var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                        html += 'You have no selected users.';
                        html += '</div>';
                          $('#loading').html(html);
                    return;
                }


                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'users', 'action' => 'activate-user']) ?>",
                    cache: false,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        user_id: userIds
                    },
                    beforeSend: function() {
                        $('#activate i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>  Loading..');
                        $('#activate').prop('disabled', true);
                    },
                    complete: function() {
                        $('#activate').html('Activate <i class="fa fa-check"></i>');
                        $('#activate').prop('disabled', false);
                    },
                    success: function(response) {
                        var error = response.json.error,
                            message = response.json.message;
                        if(error){
                        }else{
                            var html = '';
                            html += '<div class="alert alert-success alert-dismissible" role="alert">';
                            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                            html += 'All selected users has been activated';
                            html += '</div>';

                            $('#loading').html(html);

                        }
                    }
                });

                return;
           });


            //DEACTIVATION
           $('#deactivate').on('click', function(e){
                var userIds = [];
                var rows_selected = table.column(0).checkboxes.selected();

                $.each(rows_selected, function(index, rowId){
                     userIds.push(rowId);
                });
                if(userIds.length == 0) {
                   var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                        html += 'You have no selected users.';
                        html += '</div>';
                          $('#loading').html(html);
                    return;
                }


                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'users', 'action' => 'deactivate-user']) ?>",
                    cache: false,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        user_id: userIds
                    },
                    beforeSend: function() {
                        $('#deactivate i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>  Loading..');
                        $('#deactivate').prop('disabled', true);
                    },
                    complete: function() {
                        $('#deactivate').html('Activate <i class="fa fa-close"></i>');
                        $('#deactivate').prop('disabled', false);
                    },
                    success: function(response) {
                        var error = response.json.error,
                            message = response.json.message;
                        if(error){
                        }else{
                            var html = '';
                            html += '<div class="alert alert-success alert-dismissible" role="alert">';
                            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                            html += 'All selected users has been deactivated';
                            html += '</div>';

                            $('#loading').html(html);
                        }
                    }
                });

                return;
           });




        });

    </script>
<?php $this->end() ?>