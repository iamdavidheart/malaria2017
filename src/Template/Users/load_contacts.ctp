


<table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col" class="text-center"><?= $this->Paginator->sort('ACT') ?></th>
            </tr>
        </thead>
        <tbody>

        <?php foreach ($contacts as $key => $contact) { ?>
        <tr>
            <td>
                <?php if($contact->prime_setup == 1){ ?>
                    <button href="#" class="btn btn-animated btn-success btn-sm btn-block " data-id="<?=$contact->id?>" >

                      <?php

                      if($contact->value == ''){
                        echo 'The default mobile number is empty.';
                      }else{
                        echo $contact->value;
                      }
                      ?>

                     <i class="fa fa-send-o"></i>
                <?php }else{ ?>
                      <button href="#" class="btn btn-animated btn-gray-transparent btn-sm btn-block setter" data-id="<?=$contact->id?>"  >
                      <?php

                      if($contact->value == ''){
                        echo 'Other mobile number is Empty.';
                      }else{
                        echo $contact->value;
                      }
                      ?>

                      <i class="fa fa-check"></i>
                <?php } ?>
                </button>

            </td>
          
        <?php  } ?>
    </tbody>
</table>


