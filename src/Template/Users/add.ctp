

<section class="main-container">
    <div class="container">
        <div class="row">


            <div class="users col-md-12">

                <?php echo $this->Form->create($user); ?>


                    <div class="row">
                                            <h3>Person Info</h3>
                        <input type="hidden" id="id" value="<?=$user->id?>" >
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">First Name</label>
                                <?= $this->Form->input('first_name', ['class' => 'form-control', 'label' => false ]) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Last Name</label>
                                <?= $this->Form->input('last_name', ['class' => 'form-control', 'label' => false ]) ?>
                              
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Designation</label>
                                <?= $this->Form->input('designation_id', ['options' => $designations, 'class' => 'form-control',  'empty' => true, 'label' => false]); ?>
                            
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Role</label>
                                <?=  $this->Form->input('role_id', ['options' => $roles, 'class' => 'form-control', 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Email</label>
                                 <?= $this->Form->input('email', ['class' => 'form-control', 'label' => false ]) ?>
                                <i class="fa fa-envelope-o form-control-feedback"></i>
                            </div>
                        </div>
                       
                    </div>


                        <hr>
                    <div class="row">

                        <h3>Mobile Config</h3>

                        <div class="small">Atleast One Mobile No.</div>

                       <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Mobile No. 1 (Primary Number)</label>
                                <?php echo $this->Form->input('user_contacts.1.value', ['id' => 'mobile_no_one','class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                                <?php echo $this->Form->input('user_contacts.1.created', ['type'=> 'hidden', 'value' => date('Y-m-d H:i:s') ]); ?>
                                <?php echo $this->Form->input('user_contacts.1.type', ['type'=> 'hidden', 'value' => 'mobile' ]); ?>
                                <?php echo $this->Form->input('user_contacts.1.prime_setup', ['type'=> 'hidden', 'value' => '1' ]); ?>
                            </div>
                        </div>

                        <div class="cleafix"></div>
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Mobile No. 2</label>
                            <?php echo $this->Form->input('user_contacts.2.value', ['required' => false, 'id' => 'mobile_no_two','class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>
                            <?php echo $this->Form->input('user_contacts.2.created', ['required' => false, 'type'=> 'hidden', 'value' => date('Y-m-d H:i:s') ]); ?>
                            <?php echo $this->Form->input('user_contacts.2.type', ['required' => false, 'type'=> 'hidden', 'value' => 'mobile' ]); ?>
                            </div>
                        </div>

                        <div class="cleafix"></div>
                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Mobile No. 3</label>
                             <?php echo $this->Form->input('user_contacts.3.value', ['required' => false, 'id' => 'mobile_no_three','class' => 'form-control', 'autocomplete' => 'off', 'label' => false]); ?>

                            <?php echo $this->Form->input('user_contacts.3.created', ['required' => false, 'type'=> 'hidden', 'value' => date('Y-m-d H:i:s') ]); ?>
                            <?php echo $this->Form->input('user_contacts.3.type', ['required' => false, 'type'=> 'hidden', 'value' => 'mobile' ]); ?>

                            </div>
                        </div>
                        </div>

                        <hr>

                    <div class="row">
                        <h3>Facility Config</h3>

                        <div class="col-md-4">
                            <div class="form-group has-success  has-feedback">

                                <select class="form-control" name="facility_id" id="facility_id">
                                <option>(Choose facility)</option>
                                <?php foreach ($facilities as $key => $facility) { ?>
                                <option value="<?php echo $facility->id?>" ><?php echo $facility->facility_category->description?>
                                - <?php echo $facility->region->description?>, <?php echo $facility->province->description?>
                                </option>
                                <?php } ?>
                                </select>

                            </div>
                        </div>
                    </div>

                                   <button type="submit" class="btn btn-default">SUBMIT</button>

                 <?= $this->Form->end() ?>
            </div>

        </div>
    </div>
</div>

