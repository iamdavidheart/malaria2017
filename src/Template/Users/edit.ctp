


<section class="main-container">


    <div class="section light-gray-bg">
        <div class="container">


            <h2><?=$user->first_name?> <?=$user->last_name?> (<?=$user->code?>)</h2>
            <?= $this->Form->create($user) ?>
                             <?= $this->Form->input('method', ['type' => 'hidden', 'value' => 'm-users']) ?>
                <div class="row">
                    <input type="hidden" id="id" value="<?=$user->id?>" >
                    <div class="col-md-4">
                        <div class="form-group  has-success has-feedback">
                            <label class="control-label" >User Code</label>
                            <?= $this->Form->input('code', ['class' => 'form-control', 'label' => false, 'disabled' => true ]) ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group has-success  has-feedback">
                            <label class="control-label">First Name</label>
                            <?= $this->Form->input('first_name', ['class' => 'form-control', 'label' => false ]) ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group has-success  has-feedback">
                            <label class="control-label">Last Name</label>
                            <?= $this->Form->input('last_name', ['class' => 'form-control', 'label' => false ]) ?>
                          
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group has-success  has-feedback">
                            <label class="control-label">Designation</label>
                            <?= $this->Form->input('designation_id', ['options' => $designations, 'class' => 'form-control',  'empty' => true, 'label' => false]); ?>
                        
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group has-success  has-feedback">
                            <label class="control-label">Role</label>
                            <?=  $this->Form->input('role_id', ['options' => $roles, 'class' => 'form-control', 'label' => false]); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group has-success  has-feedback">
                            <label class="control-label">Email</label>
                             <?= $this->Form->input('email', ['class' => 'form-control', 'label' => false ]) ?>
                            <i class="fa fa-envelope-o form-control-feedback"></i>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group has-success  has-feedback">
                            <label class="control-label">Activate(1)/Deactivate(0)</label>
                             <?= $this->Form->input('activated', ['class' => 'form-control', 'label' => false ]) ?>
                        </div>
                    </div>
                   
                </div>
                 <button type="submit" class="btn btn-default">UPDATE INFO</button>
     


           <?= $this->Form->end() ?>


                <div class="col-md-6">
                     

                    <form role="form">
                    <hr>
                    <h3>Mobile Config</h3>
                    <div class="col-md-8 col-md-offset-2 text-center">
                    <button class="btn btn-animated btn-gray-transparent btn-sm " id="contact">Add Contacts <i class="fa fa-check"></i></button>
                    <div class="mobile"></div>

                    </div>
                    <div class="clearfix"></div>

                    <!-- <button type="submit" class="btn btn-default">Submit</button> -->
                    <?php echo $this->Form->end(); ?>
                    <hr>

                     
                 </div>

                <div class="col-md-6">
                     
                         <hr>
                    <h3>Facility Config</h3>
                    <?php echo $this->Form->create($user); ?>
                    <?= $this->Form->input('method', ['type' => 'hidden', 'value' => 'm-facility']) ?>

                    <div class="form-group has-success  has-feedback">
                    <label class="control-label">Facility *</label>
                    <select class="form-control chosen-select" name="facility_id" id="facility_id">
                        <option>(Choose facility)</option>
                        <?php foreach ($facilities as $key => $facility) { ?>
                        <option value="<?php echo $facility->id?>" 
                        <?php if($user->facility_id == $facility->id){ echo 'selected'; } ?> > 
                        <?php echo $facility->province->description?>,
                        <?php echo $facility->municipality->description?>,
                        <?php echo (!empty($facility->barangay))? $facility->barangay->description.', ' : 'No Barangay,' ;?> 
                        <?php echo (!empty($facility->sitio))? $facility->sitio->description.', ' : ' No Sitio, ' ;?> 
                        <?php echo $facility->facility_category->name?> 
                        </option>

                        <?php } ?>
                    </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr>
                    LOCATION : <div id="facility_location" style="padding:10px;font-weight:300;font-size:18px"></div>

                    <div class="b-form-row">
                        <div class="b-form-horizontal__label"></div>
                        <div class="b-form-horizontal__input">
                            <button type="submit" class="btn btn-default ">UPDATE FACILITY</button>
                        </div>
                    </div>
                 


                 </div>

                 <div class="clearfix"></div>
                <div class="col-md-6">
     
                    <hr>
                          <h3>Change Password</h3>
                        <?php echo $this->Form->create($user); ?>
                        <?= $this->Form->input('method', ['type' => 'hidden', 'value' => 'm-changepass']) ?>
   


                        <div class="col-md-12">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">UserCode</label>
                                  <?php echo $this->Form->input('code', ['id' => 'code', 'disabled' => 'disabled', 'class' => 'form-control', 'placeholder' => 'Autogenerated', 'label' => false]); ?>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Current Password *</label>
                                  <input type="password" name="current_password" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">New Password *</label>
                                  <input type="password" name="password" class="form-control">
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group has-success  has-feedback">
                                <label class="control-label">Confirm Password *</label>
                                  <input type="password" name="confirm_password" class="form-control">
                            </div>
                        </div>


                        <button type="submit" class="btn btn-default">UPDATE PASSWORD</button>
         


                        <?php echo $this->Form->end(); ?>




                     
                </div>






        </div>
    </div>


</div>





<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/bootstrap-datepicker/css/bootstrap-datepicker.css'); ?>
    <?= $this->Html->css('../js/plugin/choosen/css/chosenOrange.css'); ?>
<?php $this->end() ?>




<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('../js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
    <?=  $this->Html->script('../js/plugin/choosen/js/chosen.jquery.min.js'); ?>
    <script type="text/javascript">
        function mobileLoad(){
             var id = $('#id').val();
            $(".mobile").load("<?= $this->Url->build(['controller' => 'users', 'action' => 'load-contacts']) ?>", {id : id, btn: 'edit'}, function(responseTxt, statusTxt, xhr){
                if(statusTxt == "success"){
                    console.log(" mobile loaded successfully! " + statusTxt);
                }
                if(statusTxt == "error"){
                    console.log("Error: mobile " + xhr.status + ": " + xhr.statusText);
                }
            });
        }

        mobileLoad();


        (function($){
            $(document).ready(function(){







                $(document).on('click','.mobile .setter',function(e){
                    e.preventDefault();
                    if (confirm("Are sure want to set as default Mobile Number.")) {
                        var id = $(this).data('id');
                        $.ajax({
                            url:  "<?= $this->Url->build(['controller' => 'users', 'action' => 'ajax-set-mobile']) ?>",
                            cache: false,
                            method: 'post',
                            dataType: 'json',
                            data: {
                                id : id, user_id: $('#id').val()
                            },
                            success: function(response) {
                                var error = response.json.error,
                                    message = response.json.message;
                                if(error){
                               
                                }else{
                                  mobileLoad();
                                }
                            }
                        });
                    } else {
                        return false;
                    }

                });







                var config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : {allow_single_deselect:true},
                    '.chosen-select-no-single' : {disable_search_threshold:10},
                    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                    '.chosen-select-width'     : {width:"95%"}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }


                        //$('.datepicker').datepicker();
        }); // End document ready

        })(this.jQuery);




    </script>
<?php $this->end() ?>





