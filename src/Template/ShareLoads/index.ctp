<section class="main-container">
    <div class="container">
        <div class="row">

        <div class="col-md-12">

                <h3><?= __('Share A Load') ?></h3>
               <table class="table table-bordered table-striped " id="shareload" cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            <th scope="col" class="text-center"></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('created') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('user_id') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('load_quantity') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('status') ?></th>
                            <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($shareLoads as $shareLoad): ?>
                        <tr>
                            <td><?= $this->Number->format($shareLoad->id) ?></td>
                            <td><?= h($shareLoad->created) ?></td>
                            <td><?= $shareLoad->has('user') ? $this->Html->link($shareLoad->user->id, ['controller' => 'Users', 'action' => 'view', $shareLoad->user->id]) : '' ?></td>
                            <td><?= $this->Number->format($shareLoad->load_quantity) ?></td>
                            <td><?= h($shareLoad->status) ?></td>
                            <td class="actions text-center">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $shareLoad->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $shareLoad->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $shareLoad->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shareLoad->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#shareload').DataTable({
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                            selectRow: true
                            }
                        }
                    ],
                        select: {
                        style: 'multi'
                    },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>
