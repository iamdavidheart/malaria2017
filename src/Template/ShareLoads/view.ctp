<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Share Load'), ['action' => 'edit', $shareLoad->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Share Load'), ['action' => 'delete', $shareLoad->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shareLoad->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Share Loads'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Share Load'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="shareLoads view large-9 medium-8 columns content">
    <h3><?= h($shareLoad->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $shareLoad->has('user') ? $this->Html->link($shareLoad->user->id, ['controller' => 'Users', 'action' => 'view', $shareLoad->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($shareLoad->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($shareLoad->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Load Quantity') ?></th>
            <td><?= $this->Number->format($shareLoad->load_quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($shareLoad->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($shareLoad->modified) ?></td>
        </tr>
    </table>
</div>
