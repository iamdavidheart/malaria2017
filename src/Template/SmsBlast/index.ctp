
<section class="main-container">

    <div class="container">
        <h3><?= __('SMS BLAST') ?></h3>
        <div class="row">



            <div class="col-md-8">
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'sms-blast', 'action' => 'list']) ?>">Blast List <i class="fa fa-list"></i></a>
            </div>



            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body well well-sm">
                        <label>Message</label>
                        <?= $this->Form->input('message', ['type' => 'textarea', 'id' => 'message', 'rows' => '5', 'class' => 'form-control', 'label' => false]) ?>

                        <div id="text_count" style="margin-top:0px;padding:5px;color:#000" class="alert alert-warning text-center"><b>Character Count : 0/0</b></div>


                         <a class="btn btn-default" id="send">SUBMIT <i class="fa fa-envelope"></i></a>
                        <div id="loading"></div>
                    </div>
                </div>
            </div>



            <div class="col-md-6 ">
            <fieldset>
            <legend><?= __('provinces') ?></legend>

            <select class="form-control " multiple name="province" id="province">
            <option value="1x">(Choose province)</option>
            <?php foreach ($provinces as $key => $prov) { ?>
            <option value="<?php echo $prov->id?>"  > 
            <?php echo $prov->description?>
            </option>

            <?php } ?>
            </select>
       
            </fieldset>
            </div>



            <div class="col-md-6 ">
            <fieldset>
            <legend><?= __('Setup') ?></legend>

            <select class="form-control " name="contact" id="contact">
                <option>(Setup Contacts)</option>
                <option value="1">Primary Numbers Only</option>
                <option value="2">All Contacts</option>
            </select>

            <select class="form-control " name="status" id="status">
                <option value="2">(Setup Status)</option>
                <option value="1">Active</option>
                <option value="0">Deactivated</option>
            </select>


       
            </fieldset>
            </div>



            <div class="users col-md-12">
                
                <div class="table-responsive" id="userContainer">

                </div>
            </div>
    

        </div>
    </div>
</div>

<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
   <!--  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css"/> -->
   <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script> -->
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var prov = [];
            $('#province').change(function(e) {
                var selected = $(e.target).val();
                prov = selected;
                load_users();
            }); 

            var status;
            $('#status').on('change', function() {
                status = this.value;
                 load_users();
            })

            function load_users() {
                $('#userContainer').load("<?= $this->Url->build(['controller' => 'sms-blast', 'action' => 'load-users']) ?>", {province:prov,status:status });
                // $('#group-id').change(function() {
                //     var groupId = $('#group-id').val();
                //     $('#customerContainer').load('/admin/customers/table', { group_id:groupId });
                // });
            }
            load_users();



            $("#message").keyup(function(){
                var content=$("#message").val();
                var length= content.length;
                var no_sms= parseInt(length)/160;
                no_sms=Math.ceil(no_sms); 
                $("#text_count").addClass("alert alert-warning text-center");
                $("#text_count").html("<b>Character Count : "+length+'/'+no_sms+"</b>");
            });




        });

    </script>
<?php $this->end() ?>
