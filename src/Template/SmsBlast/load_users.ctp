<table class="users table table-bordered " id="user-d" >
    <thead>
        <tr>
            <th scope="col" class="text-center"></th>
            <th scope="col" class="text-center"><?= $this->Paginator->sort('ACT') ?></th>
            <th scope="col" class="text-center"><?= $this->Paginator->sort('CODE') ?></th>
            <th scope="col" class="text-center"><?= $this->Paginator->sort('First Name') ?></th>
            <th scope="col" class="text-center"><?= $this->Paginator->sort('Last Name') ?></th>
            <th scope="col" class="text-center"><?= $this->Paginator->sort('Primary Number') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
        <tr>
        

            <td ><?=$user->id?> </td>
            <td >
            <?php 
            if($user->activated == 1){
                echo '<label class="label label-success">ACT</label>';
            }else{
                echo '<label class="label label-danger">DCT</label>';
            }
            ?>
            </td>
            <td><?= strtoupper($user->code) ?></td>
            <td><?= h($user->first_name) ?></td>
            <td><?= h($user->last_name) ?></td>
            <td class="actions text-center">
                <?php
                    if(!empty($user->user_contacts)){
                        echo $user->user_contacts[0]->value;
                    }
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>





<script type="text/javascript"> 
        $(document).ready(function () {

            var table = $('#user-d').DataTable({
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                            selectRow: true
                            }
                        }
                    ],
                        select: {
                        style: 'multi'
                    },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
            });

            //ACTIVATION
           $('#send').on('click', function(e){
                var userIds = [];


                var sms = $('#message').val();

                if(sms.length == 0) {
                   var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                        html += "Message is required.";
                        html += '</div>';
                          $('#loading').html(html);
                    return;
                }



                var rows_selected = table.column(0).checkboxes.selected();
                $.each(rows_selected, function(index, rowId){
                     userIds.push(rowId);
                });


                if(userIds.length == 0) {
                   var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                        html += 'You have no selected users.';
                        html += '</div>';
                          $('#loading').html(html);
                    return;
                }

                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'sms-blast', 'action' => 'send-blast']) ?>",
                    cache: false,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        user_id: userIds,sms:sms
                    },
                    beforeSend: function() {
                        $('#send i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>SMS NOW IS SENDING  Loading..');
                        $('#send').prop('disabled', true);
                    },
                    complete: function() {
                        $('#send').html('SUBMIT <i class="fa fa-envelope"></i>');
                        $('#send').prop('disabled', false);
                    },
                    success: function(response) {
                        var error = response.json.error,
                            message = response.json.message;
                        if(error){
                        }else{
                            var html = '';
                            html += '<div class="alert alert-success alert-dismissible" role="alert">';
                            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                            html += 'SMS blast has been saved.';
                            html += '</div>';

                            $('#loading').html(html);

                        }
                    }
                });






                return;
           });






        });
</script>


