



<section class="main-container">
    <div class="container">
        <div class="row">


     
            <!-- main start -->
            <!-- ================ -->
            <div class=" col-md-12" >
    
                        <table class="table table-bordered table-striped dataTable" id="list">
                            <thead>
                                <tr>
                                    <th class="text-center"></th>
                  
                                    <th class="text-center">RECIPIENT</th>
                                    <th class="text-center">MESSAGE</th>
                                    <th class="text-center">DATETIME</th>
                                    <th class="text-center actions"><?= __('ACTION') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  foreach ($smsOutgoings as $smsOutgoing){  ?>
                            <?php if($smsOutgoing->view == 0) {?>
                                 <tr style="background:#eaeaea">
                            <?php }else{ ?>
                                 <tr>
                            <?php } ?>
                                    <td><?=$smsOutgoing->id?></td>
                                    <td>
                                        <?php
                                            if(!empty($smsOutgoing->user)){
                                                echo $smsOutgoing->user->full_name;
                                            }else{
                                                echo $smsOutgoing->receiver_no;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                        echo $this->Text->truncate(
                                            $smsOutgoing->message,
                                            45,
                                            [
                                                'ellipsis' => '...',
                                                'exact' => false
                                            ]
                                        );
                                        ?>
                                    </td>
                                    <td><small><?php echo date('Y/m/d h:i A', strtotime($smsOutgoing->created)); ?></small></td>
                                    <td class="actions text-center">
                         
                                    <?php //$this->Form->postLink( __('<i class="fa fa-trash"></i>'), ['action' => 'delete-outgoing', $smsOutgoing->id], ['class' => 'btn btn-primary', 'escape' => false], ['confirm' => __('Are you sure you want to delete # {0}?', $smsOutgoing->id)]); ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>


        
            </div>
            <!-- main end -->


        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#list').DataTable({
                    columnDefs: [
                        {
                            targets: 0,
                            checkboxes: {
                            selectRow: true
                            }
                        }
                    ],
                        select: {
                        style: 'multi'
                    },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>