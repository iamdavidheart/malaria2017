


<section class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'designations', 'action' => 'add']) ?>">Add Designation <i class="fa fa-plus"></i></a>
            </div>


            <div class="col-md-12">
                <h3><?= __('DESIGNATION') ?></h3>

               <table class="table table-bordered table-striped " id="designation" cellpadding="0" cellspacing="0" >
                     <thead>
                        <tr>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('ID') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('MODIFIED') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('DESIGNATION') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('STATUS') ?></th>
                            <th scope="col" class="actions text-center" width="200px"><?= __('ACTIONS') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($designations as $designation): ?>
                        <tr>
                            <td><?= $this->Number->format($designation->id) ?></td>
                            <td><?= h($designation->modified) ?></td>
                            <td><?= h($designation->name) ?></td>
                            <td><?= $this->Number->format($designation->status) ?></td>
                            <td class="actions text-center">

                                        <?php 
                                        echo $this->Html->link(
                                            __('<i class="fa fa-edit"></i> Edit'),
                                            ['action' => 'edit', $designation->id],
                                            [
                                                'class' => 'btn btn-gray-transparent btn-sm ',
                                                'escape' => false
                                            ]);
                                        ?>

                                        <?= $this->Form->postLink( __('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $designation->id],
                                            ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $designation->id )]  ); ?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>



            </div>




        </div>
    </div>
</section>
<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#designation').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>







