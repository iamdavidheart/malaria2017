<section class="main-container">
    <div class="container">
        <div class="row">


            <div class="col-md-8">
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'designations']) ?>">Designation List<i class="fa fa-list"></i></a>
            </div>

            <div class="col-md-12">
                    <h3><?= __('ADD DESIGNATION') ?></h3>


                    <?php echo $this->Form->create($designation); ?>
                        <div class="row">
       
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >Name *</label>
                                    <?php  echo $this->Form->input('name', ['class' => 'form-control', 'label' => false]); ?>
               
                                </div>
                            </div>

                        </div>
                     
               
                         <button type="submit" class="btn btn-default">Submit</button>
                   <?= $this->Form->end() ?>

            </div>

  
        </div>
    </div>
</section>
