<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Template Notification'), ['action' => 'edit', $templateNotification->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Template Notification'), ['action' => 'delete', $templateNotification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $templateNotification->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Template Notifications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template Notification'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Templates'), ['controller' => 'Templates', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Template'), ['controller' => 'Templates', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="templateNotifications view large-9 medium-8 columns content">
    <h3><?= h($templateNotification->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $templateNotification->has('user') ? $this->Html->link($templateNotification->user->id, ['controller' => 'Users', 'action' => 'view', $templateNotification->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Template') ?></th>
            <td><?= $templateNotification->has('template') ? $this->Html->link($templateNotification->template->id, ['controller' => 'Templates', 'action' => 'view', $templateNotification->template->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($templateNotification->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($templateNotification->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Remarks') ?></h4>
        <?= $this->Text->autoParagraph(h($templateNotification->remarks)); ?>
    </div>
</div>
