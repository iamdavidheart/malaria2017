
<section class="main-container">
    <div class="container">
        <div class="row">

                <div class="col-md-8">
                    <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'items', 'action' => 'add']) ?>">Add Stock Level <i class="fa fa-plus"></i></a>
                </div>


            <div class="col-md-12">

                <h3><?= __('STOCK LEVEL LIST') ?></h3>
               <table class="table table-bordered table-striped " id="items" cellpadding="0" cellspacing="0" >
                <thead>
                    <tr>
                        <th scope="col" class="text-center">ID</th>
      
                        <th scope="col" class="text-center" width="20%"><?= $this->Paginator->sort('FACILITY') ?></th>
       
                        <th scope="col" class="text-center" width="35%"><?= $this->Paginator->sort('REPORT ITEM') ?></th>
                        <th scope="col" class="text-center"><?= $this->Paginator->sort('REPORTER') ?></th>
                        <th scope="col" class="text-center"><?= $this->Paginator->sort('REPORT DATE') ?></th>
                        <th scope="col" class="actions text-center" width="150px"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($items as $item): ?>
                    <tr>
                        <td><?= $this->Number->format($item->id) ?> <label class="label label-info"><?= h($item->source) ?></label></td>
          

                        <td>
                            <?php 
                            if(!empty($item->facility)){
                            echo '('.$item->facility->id.') - ' ;
                            echo $item->facility->province->description. ', '.$item->facility->municipality->description.',';
                            echo (!empty($item->facility->barangay))? $item->facility->barangay->description.', ' : 'No Barangay,' ;
                            echo (!empty($item->facility->sitio))? $item->facility->sitio->description.', ' : ' No Sitio, ' ;
                            }else{
                            echo 'No Facility';
                            }
                            ?>
                        </td>
                          <td class="text-center">
                            <?php 
                                echo (is_float($item->al_qty))?  '<b>AL: </b><label class="label label-success">'. $item->al_qty.'</label>' : '<label><b>AL:</b> <label class="label label-danger">NULL</label>' ;
                                echo (is_float($item->pq_qty))?  ', <b>PQ: </b><label class="label label-success">'. $item->pq_qty.'</label>' : ', <b>PQ:</b> <label class="label label-danger">NULL</label>' ;
                                echo (is_float($item->cq_qty))?  ', <b>CQ: </b><label class="label label-success">'. $item->cq_qty.'</label>' : ', <b>CQ:</b> <label class="label label-danger">NULL</label>' ;
                                echo (is_float($item->qt_qty))?  ', <b>QT: </b><label class="label label-success">'. $item->qt_qty.'</label>' : ', <b>QT:</b> <label class="label label-danger">NULL</label>' ;
                                echo (is_float($item->qa_qty))?  ', <b>QA: </b><label class="label label-success">'. $item->qa_qty.'</label>': ', <b>QA:</b> <label class="label label-danger">NULL</label>' ;

                            ?>
                        </td>
                        <td><?= $item->user->full_name ?></td>
                        <td><small><?php echo date('Y/m/d', strtotime($item->report_date)); ?></small></td>
                        <td class="actions text-center"">

                              <?php 
                                echo $this->Html->link(
                                    __('<i class="fa fa-edit"></i> Edit'),
                                    ['action' => 'edit', $item->id],
                                    [
                                        'class' => 'btn btn-gray-transparent btn-sm ',
                                        'escape' => false
                                    ]);
                                ?>

                                <?= $this->Form->postLink( __('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $item->id],
                                    ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $item->id )]  ); ?>

                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#items').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>
