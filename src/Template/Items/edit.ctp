<section class="main-container">
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="pull-right">
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'items']) ?>">Stock Level List <i class="fa fa-list"></i></a>
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'items', 'action' => 'add']) ?>">Add Stock Level <i class="fa fa-plus"></i></a>
                </div>
            </div>
            </div>

        <div class="col-md-12">
                <h3><?= __('EDIT STOCK LEVEL') ?></h3>

                            <?= $this->Form->create($item, ["class" => "form-horizontal", "role" => "form"]) ?>
                   
                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">AL</label>
                                    <div class="col-sm-4">
                                        <?= $this->Form->input('al_qty', ['class' => 'form-control', 'label' => false]) ?>
                                    </div>
                                </div>
                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">PQ</label>
                                    <div class="col-sm-4">
                                        <?= $this->Form->input('pq_qty', ['class' => 'form-control', 'label' => false]) ?>
                                    </div>
                                </div>
                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">CQ</label>
                                    <div class="col-sm-4">
                                        <?= $this->Form->input('cq_qty', ['class' => 'form-control', 'label' => false]) ?>
                                    </div>
                                </div>
                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">QT</label>
                                    <div class="col-sm-4">
                                        <?= $this->Form->input('qt_qty', ['class' => 'form-control', 'label' => false]) ?>
                                    </div>
                                </div>
                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">QA</label>
                                    <div class="col-sm-4">
                                        <?= $this->Form->input('qa_qty', ['class' => 'form-control', 'label' => false]) ?>
                                    </div>
                                </div>

                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">REPORT DATE</label>
                                    <div class="col-sm-8">
                                        <div class="input-group date" data-provide="datepicker">
                                             <?= $this->Form->input('report_date', ['type' => 'text','class' => 'form-control', 'label' => false]) ?>
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">FACILITY</label>
                                    <div class="col-sm-8">
                                        <select class="form-control chosen-select" name="facility_id" >
                                            <option>(Choose facility)</option>
                                            <?php foreach ($facilities as $key => $facility) { ?>
                                            <option value="<?php echo $facility->id?>" 
                                            <?php if($item->facility_id == $facility->id){ echo 'selected'; } ?> > 
                                            <?php echo $facility->province->description?>,
                                            <?php echo $facility->municipality->description?>,
                                            <?php echo (!empty($facility->barangay))? $facility->barangay->description.', ' : 'No Barangay,' ;?> 
                                            <?php echo (!empty($facility->sitio))? $facility->sitio->description.', ' : ' No Sitio, ' ;?> 
                                             <?php echo $facility->facility_category->name?> 
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group  has-success">
                                    <label for="inputEmail3" class="col-sm-2 control-label">REMARKS</label>
                                    <div class="col-sm-8">
                                        <?= $this->Form->input('remarks', ['type' => 'textarea', 'rows' => '3','class' => 'form-control', 'label' => false]) ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default">Update</button>
                                    </div>
                                </div>
                            </form>



        </div>

  
        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/bootstrap-datepicker/css/bootstrap-datepicker.css'); ?>
    <?= $this->Html->css('../js/plugin/choosen/css/chosenOrange.css'); ?>
<?php $this->end() ?>




<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('../js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
    <?=  $this->Html->script('../js/plugin/choosen/js/chosen.jquery.min.js'); ?>
    <script type="text/javascript">


        (function($){
            $(document).ready(function(){

                var config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : {allow_single_deselect:true},
                    '.chosen-select-no-single' : {disable_search_threshold:10},
                    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                    '.chosen-select-width'     : {width:"95%"}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }


                        //$('.datepicker').datepicker();
        }); // End document ready

        })(this.jQuery);




    </script>
<?php $this->end() ?>

