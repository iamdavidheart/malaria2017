<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Facilities'), ['controller' => 'Facilities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Facility'), ['controller' => 'Facilities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['controller' => 'SmsIncomings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['controller' => 'SmsIncomings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Item Details'), ['controller' => 'ItemDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item Detail'), ['controller' => 'ItemDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="items view large-9 medium-8 columns content">
    <h3><?= h($item->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Template Code') ?></th>
            <td><?= h($item->template_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Source') ?></th>
            <td><?= h($item->source) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Facility') ?></th>
            <td><?= $item->has('facility') ? $this->Html->link($item->facility->id, ['controller' => 'Facilities', 'action' => 'view', $item->facility->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sms Incoming') ?></th>
            <td><?= $item->has('sms_incoming') ? $this->Html->link($item->sms_incoming->id, ['controller' => 'SmsIncomings', 'action' => 'view', $item->sms_incoming->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $item->has('user') ? $this->Html->link($item->user->id, ['controller' => 'Users', 'action' => 'view', $item->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($item->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Al Qty') ?></th>
            <td><?= $this->Number->format($item->al_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pq Qty') ?></th>
            <td><?= $this->Number->format($item->pq_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cq Qty') ?></th>
            <td><?= $this->Number->format($item->cq_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Qt Qty') ?></th>
            <td><?= $this->Number->format($item->qt_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Qa Qty') ?></th>
            <td><?= $this->Number->format($item->qa_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($item->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Report Date') ?></th>
            <td><?= h($item->report_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($item->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($item->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Remarks') ?></h4>
        <?= $this->Text->autoParagraph(h($item->remarks)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Item Details') ?></h4>
        <?php if (!empty($item->item_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Item Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Item Unit Id') ?></th>
                <th scope="col"><?= __('Sku') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($item->item_details as $itemDetails): ?>
            <tr>
                <td><?= h($itemDetails->id) ?></td>
                <td><?= h($itemDetails->created) ?></td>
                <td><?= h($itemDetails->modified) ?></td>
                <td><?= h($itemDetails->item_id) ?></td>
                <td><?= h($itemDetails->user_id) ?></td>
                <td><?= h($itemDetails->code) ?></td>
                <td><?= h($itemDetails->quantity) ?></td>
                <td><?= h($itemDetails->item_unit_id) ?></td>
                <td><?= h($itemDetails->sku) ?></td>
                <td><?= h($itemDetails->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ItemDetails', 'action' => 'view', $itemDetails->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ItemDetails', 'action' => 'edit', $itemDetails->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ItemDetails', 'action' => 'delete', $itemDetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemDetails->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
