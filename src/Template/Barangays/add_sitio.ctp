          


<section class="main-container">
    <div class="container">
        <div class="row">
            </div>

                <div class="col-md-8">
                    <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'barangays']) ?>">Barangay <i class="fa fa-list"></i></a>
                    <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'sitios']) ?>">Sitio <i class="fa fa-list"></i></a>
                </div>

                <div class="col-md-12">
                    <h3><?= __('ADD SITIO') ?></h3>
                    <?= $this->Form->create($sitio) ?>
                        <div class="row">
                    
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >REGION</label>
                                     <input type="text" class="form-control" value="<?=$barangay->region->description?>" disabled>
                                      <input type="hidden" name="region_id" value="<?=$barangay->region_id?>">
                                      <input type="hidden" name="region_code" value="<?=$barangay->region_code?>">

                                </div>
                            </div>
          
                            <div class="col-md-4">
                                <div class="form-group has-success  has-feedback">
                                    <label class="control-label" >PROVINCE</label>
                                  <input type="text" class="form-control" disabled  value="<?=$barangay->province->description?>">
                                  <input type="hidden" name="province_id" value="<?=$barangay->province_id?>">
                                  <input type="hidden" name="province_code" value="<?=$barangay->province_code?>">

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >PROVINCE</label>
                                  <input type="text" class="form-control" disabled value="<?=$barangay->municipality->description?>">
                                  <input type="hidden" name="municipality_id" value="<?=$barangay->municipality_id?>">
                                  <input type="hidden" name="municipality_code" value="<?=$barangay->municipality_code?>">

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >BARANGAY</label>
                                  <input type="text" class="form-control" disabled  value="<?=$barangay->description?>">
                                  <input type="hidden" name="barangay_id" value="<?=$barangay->id?>">
                                  <input type="hidden" name="barangay_code" value="<?=$barangay->barangay_code?>">

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" >SITIO DESCRIPTION</label>
                                    <?= $this->Form->input('description', ['class' => 'form-control', 'label' => false]) ?>
                                </div>
                            </div>
                        </div>
    
                         <button type="submit" class="btn btn-default">ADD SITIO</button>
                   <?= $this->Form->end() ?>
                </div>
        </div>
    </div>
</section>




