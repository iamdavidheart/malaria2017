


<section class="main-container">
    <div class="container">
        <div class="row">
           


                <div class="col-md-12">


                    <h3><?= __('Barangay') ?></h3>
                        <table class="table table-bordered table-striped dataTable" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                <th class="text-center">PROVINCE</th>
                                <th class="text-center">MUNICIPALITY</th>
                                    <th class="text-center">BARANGAY</th>
                                    <th class="text-center">CODE</th>
            
                                    <th class="actions text-center" style="width:200px"><?= __('ACTION') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($barangays as $barangay): ?>
                                <tr>
                                    <td><?= $barangay->province->description ?></td>
                                    <td><?= $barangay->municipality->description ?></td>
                                    <td><?= $barangay->description ?></td>
                                    <td><?= $barangay->barangay_code ?></td>
                                    <td class="actions text-center">


                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-edit"></i> Edit'),
                                        ['action' => 'edit', $barangay->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm ',
                                            'escape' => false
                                        ]);
                                    ?>


                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-plus"></i> Add Sitio.'),
                                        ['action' => 'add-sitio', $barangay->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm ',
                                            'escape' => false
                                        ]);
                                    ?>

                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>



                </div>
        </div>
    </div>
</section>



