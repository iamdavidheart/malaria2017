


<section class="main-container">
    <div class="container">
        <div class="row">
            </div>

                <div class="col-md-8">
                    <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'barangays']) ?>">Barangay <i class="fa fa-list"></i></a>
                </div>

                <div class="col-md-12">
                    <h3><?= __('EDIT BARANGAY') ?></h3>
                    <?= $this->Form->create($barangay) ?>
                        <div class="row">
                    
                            <div class="col-md-6">
                                <div class="form-group  has-feedback">
                                    <label class="control-label" >CODE</label>
                                    <?= $this->Form->input('barangay_code', ['class' => 'form-control', 'label' => false, 'disabled' => true]) ?>
                                </div>
                            </div>
          
                            <div class="col-md-6">
                                <div class="form-group  has-feedback">
                                    <label class="control-label" >DESCRIPTION</label>
                                    <?= $this->Form->input('description', ['class' => 'form-control', 'label' => false]) ?>
                                </div>
                            </div>
                        </div>
    
                         <button type="submit" class="btn btn-default">Update</button>
                   <?= $this->Form->end() ?>
                </div>
        </div>
    </div>
</section>

