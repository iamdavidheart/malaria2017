<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Facility Category'), ['action' => 'edit', $facilityCategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Facility Category'), ['action' => 'delete', $facilityCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $facilityCategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Facility Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Facility Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Facilities'), ['controller' => 'Facilities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Facility'), ['controller' => 'Facilities', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="facilityCategories view large-9 medium-8 columns content">
    <h3><?= h($facilityCategory->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($facilityCategory->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($facilityCategory->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($facilityCategory->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($facilityCategory->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($facilityCategory->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($facilityCategory->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Facilities') ?></h4>
        <?php if (!empty($facilityCategory->facilities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Facility Category Id') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Municipality Id') ?></th>
                <th scope="col"><?= __('Barangay Id') ?></th>
                <th scope="col"><?= __('Sitio Id') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Remarks') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Cut Off Date') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($facilityCategory->facilities as $facilities): ?>
            <tr>
                <td><?= h($facilities->id) ?></td>
                <td><?= h($facilities->created) ?></td>
                <td><?= h($facilities->modified) ?></td>
                <td><?= h($facilities->facility_category_id) ?></td>
                <td><?= h($facilities->region_id) ?></td>
                <td><?= h($facilities->province_id) ?></td>
                <td><?= h($facilities->municipality_id) ?></td>
                <td><?= h($facilities->barangay_id) ?></td>
                <td><?= h($facilities->sitio_id) ?></td>
                <td><?= h($facilities->description) ?></td>
                <td><?= h($facilities->remarks) ?></td>
                <td><?= h($facilities->user_id) ?></td>
                <td><?= h($facilities->cut_off_date) ?></td>
                <td><?= h($facilities->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Facilities', 'action' => 'view', $facilities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Facilities', 'action' => 'edit', $facilities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Facilities', 'action' => 'delete', $facilities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $facilities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
