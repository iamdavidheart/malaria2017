<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $facilityCategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $facilityCategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Facility Categories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Facilities'), ['controller' => 'Facilities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Facility'), ['controller' => 'Facilities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="facilityCategories form large-9 medium-8 columns content">
    <?= $this->Form->create($facilityCategory) ?>
    <fieldset>
        <legend><?= __('Edit Facility Category') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('description');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
