
<section class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="pull-right">
                    <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'mprf-cases']) ?>">MPRF Cases  <i class="fa fa-list"></i></a>
                </div>
            </div>


            <div class="col-md-12">
                <h3><?= __('ADD MALARIA CASE') ?></h3>


                <div class="panel panel-default">
       
                    <?php echo $this->Form->create($mprfCase, ['id' => 'malariaForm' ]); ?>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-8 col-sm-12">
            
                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <select class="form-control chosen-select" name="facility_id" id="facility_id">
                                            <option>(Choose facility code)</option>
                                            <?php foreach ($facilities as $key => $facility) { ?>
                                            <option value="<?php echo $facility->id?>"><?php echo '#Code - '.$facility->id?></option>
                                            <?php } ?>
                                        </select>

                                        <label for="">NAME OF FACILITY</label>
                                    </div>
                                </div>

                                  <div class=" col-sm-4">
                                        <?= $this->Form->input('consult_date', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'consult_date', 'label' => false,'minYear' => date('Y') - 10, 'maxYear' => date('Y')]); ?>
                                      <i class="fa fa-calendar "></i>
                                      <label for="">DATE OF CONSULTATION</label>
                                  </div>
                    

                                  <div class=" col-sm-4">
                                        <?= $this->Form->input('death_date', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'death_date', 'label' => false,'minYear' => date('Y') - 10, 'maxYear' => date('Y') ]); ?>
                                      <i class="fa fa-calendar "></i>
                                      <label for="">DEATH DATE</label>
                                  </div>




                                 <div class="clearfix"></div>
      

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= $this->Form->input('last_name', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'last_name', 'label' => false]); ?>
                                        <label for="">LASTNAME</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <?= $this->Form->input('first_name', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'first_name', 'label' => false]); ?>
                                        <label for="">FIRST NAME</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <?= $this->Form->input('middle_name', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'middle_name', 'label' => false, 'required' => false]); ?>
                                        <label for="">M.I.</label>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <?= $this->Form->input('birthdate', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'birthdate', 'label' => false, 'required' => false,'minYear' => date('Y') - 70, 'maxYear' => date('Y')]); ?>
                                        <i class="fa fa-calendar"></i>

        
       

                                        <label>DATE OF BIRTH</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <?= $this->Form->input('age', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'age', 'label' => false, 'required' => false]); ?>
                                        <label for="">AGE</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <?= $this->Form->input('lessmonth', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'lessmonth', 'label' => false, 'required' => false]); ?>
                                        <label for="">MONTH (IF BELOW 1 YEAR OLD)</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">
                                        <?= $this->Form->input('weight', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'weight', 'label' => false, 'required' => false]); ?>
                                        <label for="">WEIGHT (KILO)</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">

                                       <?php
                                        echo $this->Form->radio(
                                            'gender',
                                            [
                                                ['value' => 'M', 'text' => 'MALE'],
                                                ['value' => 'F', 'text' => 'FEMALE']
                                            ]
                                        );
                                        ?><br>
                                        <label for="">GENDER</label>

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group ">

                                       <?php
                                        echo $this->Form->radio(
                                            'pregnant',
                                            [
                                                ['value' => 1, 'text' => 'YES'],
                                                ['value' => 0, 'text' => 'NO']
                                            ]
                                        );
                                        ?>
                                        <br>
                                        <label for="">PREGNANT</label>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <?= $this->Form->input('ip_group', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'ip_group', 'label' => false, 'required' => false]); ?>
                                        <label for="">IP GROUP</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <?= $this->Form->input('occupation', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'ip_group', 'label' => false, 'required' => false]); ?>
                                        <label for="">SOURCE OF INCOME/OCCUPATION</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <?php echo $this->Form->input('region_id', ['id' => 'region_id',  'class' => 'form-control', 
                                        'options' => $regions, 'label' => false, 'empty' => '(Choose Region)']); ?>
                                        <label for="">REGION</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <select  class="form-control" name="province_id" id="province_id">
                                        </select>
                                        <label for="">PROVINCE</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <select  class="form-control" name="municipality_id" id="municipality_id">
                                        </select>
                                        <label for="">MUNICIPALITY</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <select  class="form-control" name="barangay_id" id="barangay_id">
                                        </select>
                                        <label for="">BARANGAY</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group ">
                                        <select  class="form-control" name="sitio_id" id="sitio_id">
                                        </select>
                                        <label for="">SITIO</label>
                                    </div>
                                </div>
                           
                         
                                <div class="clearfix"></div>
                                <div class="well well-lg">

                                    <div class="row">

                                        <div class="col-sm-6">
                                            
                                            <div class="form-group ">
                                                <label for="">CHIEF COMPLAINT [ <?= $this->Form->checkbox('chief_complaint', ['required' => false , 'label' => false]); ?> ]  IF YES TEMP?</label>
                                                <?= $this->Form->input('temprature', ['autocomplete' => 'off', 'placeholder' => 'Temperature C', 'class' => 'form-control', 'id' => 'temprature', 'label' => false, 'required' => false]); ?>
                                            </div>

                                        </div> 

                                        <div class="col-sm-6">
                                      
                                            <div class="form-group ">
                                                <label for="">OTHER <?= $this->Form->checkbox('c_other_complaint', ['required' => false , 'label' => false]); ?></label>
                                                <?= $this->Form->input('other_complaint', ['autocomplete' => 'off', 'placeholder' => 'Other Complaint', 'class' => 'form-control', 'id' => 'other_complaint', 'label' => false, 'required' => false]); ?>
                                            </div>

                                        </div> 



                                        <div class="col-sm-10">
                                            <p>HISTORY OF TRAVEL FOR THE PAST TWO WEEKS?</p>
                                            <div class="form-group ">
                         
                                               <?php
                                                echo $this->Form->radio(
                                                    'travel_history',
                                                    [
                                                        ['value' => 1, 'text' => 'YES'],
                                                        ['value' => 0, 'text' => 'NO']
                                                    ]
                                                );
                                                ?>


                                            </div>



                                        </div> 
                                        <div class="col-sm-12 ">
                                            <div class="form-group ">
                                            <label for="">IF YES, WHERE?</label>
                                                <?= $this->Form->input('travel_history_place', ['autocomplete' => 'off', 'type' => 'textarea', 'rows' => 3, 'class' => 'form-control', 'id' => 'travel_history_place', 'label' => false, 'required' => false]); ?>
                                            </div>
                                        </div> 
                                        <div class="col-sm-10 ">
                                            <p>HISTORY OF BLOOD TRANSFUSION 2 WEEKS PRIOR TO ONSET OF ILLNESS?</p>
                                            <div class="form-group ">
                                               <?php
                                                echo $this->Form->radio(
                                                    'history_blood_trans',
                                                    [
                                                        ['value' => 1, 'text' => 'YES'],
                                                        ['value' => 0, 'text' => 'NO']
                                                    ]
                                                );
                                                ?>
                                                <br>
                                            </div>
                                        </div> 
                                        <div class="col-sm-10 ">
                                            <p>IF NO, 6 MONTHS PRIOR TO ILLNESS?</p>
                                            <div class="form-group ">
                                               <?php
                                                echo $this->Form->radio(
                                                    'history_illness',
                                                    [
                                                        ['value' => 1, 'text' => 'YES'],
                                                        ['value' => 0, 'text' => 'NO']
                                                    ]
                                                );
                                                ?>
                                                <br>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                            <div class="clearfix"></div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="panel panel-default">

                                    <div class="panel-body">
                                        <div class="form-group hide">
                                        <?= $this->Form->input('transaction', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'transaction', 'label' => false]); ?> 
                                            <label for="transaction_id">TRANSACTION ID</label>
                                        </div>

                                        <div class="form-group ">

                                        <?= $this->Form->input('blood_exam_date', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'blood_exam_date', 'label' => false, 'minYear' => date('Y') - 10, 'maxYear' => date('Y')]); ?>

                                             <i class="fa fa-calendar "></i>
                                            <label>DATE BLOOD EXAMINED</label>
                                        </div>

                                        <div class="form-group ">

                                        <?= $this->Form->input('blood_exam_result_date', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'blood_exam_result_date', 'label' => false ,'minYear' => date('Y') - 10, 'maxYear' => date('Y')]); ?>

                                             <i class="fa fa-calendar "></i>
                                            <label>DATE RESULT RELEASED</label>
                                        </div>

                                    </div>

                                    <div class="panel-heading">
                                        <div class="text-center">RESULT</div>
                                    </div>

                                    <div class="panel-body">
                                        <table class="table table-bordered">
                                            <thead>
                                                <th colspan="2"> <input type="checkbox" name="microscopy" value="1"> MICROSCOPY</th>
                                                <th > <input type="checkbox" name="rdt" value="1"> RDT</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="1">
                                                        <?= $this->Form->checkbox('m_pf', ['required' => false , 'label' => false]); ?>  PF 
                                                    </td>
                                                    <td colspan="1">
                                                        <?= $this->Form->checkbox('m_pv', ['required' => false , 'label' => false]); ?>  PV 
                                                    </td>
                                                    <td><?= $this->Form->checkbox('r_pf', ['required' => false , 'label' => false]); ?>  PF </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1">
                                                        <?= $this->Form->checkbox('pm', ['required' => false , 'label' => false]); ?> PM 
                                                    </td>
                                                    <td colspan="1">
                                                        <?= $this->Form->checkbox('po', ['required' => false , 'label' => false]); ?> PO 
                                                    </td>
                                                    <td><?= $this->Form->checkbox('r_pv', ['required' => false , 'label' => false]); ?> PV </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><?= $this->Form->checkbox('m_pfpv', ['required' => false , 'label' => false]); ?>  PF/PV </td>
                                                    <td><?= $this->Form->checkbox('r_pfpv', ['required' => false , 'label' => false]); ?> PF/PV </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><?= $this->Form->checkbox('pfpm', ['required' => false , 'label' => false]); ?> PF/PM </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><?= $this->Form->checkbox('pvpm', ['required' => false , 'label' => false]); ?>PV/PM </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">*****</td>
                                                    <td>*****</td>
                                                </tr>

                                            </tbody>
                                     
                                        </table>
                                        <div class="clearfix"></div>
                                            <div class="form-group floating-label">
                                                <?= $this->Form->input('parasite_blood', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'parasite_blood', 'label' => false, 'required' => false]); ?>
                                           
                                                <label for="parasite_blood">PARASITE PER UL BLOOD</label>
                                            </div>

                                            <div class="checkbox checkbox-styled">
                                                <label>
                                                    <?= $this->Form->checkbox('clinical_diagnosis', ['required' => false , 'label' => false]); ?>
                                                    <span>CLINICAL DIAGNOSIS</span>
                                                </label>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h4>ANTI-MALARIA DRUGS GIVEN</h4>
                        <table class="table table-striped">
                            <thead>
                                <th></th>
                                <th>TOTAL QTY</th>
                                <th>DATE STARTED</th>
                                <th>PREPARTION</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                    <?= $this->Form->checkbox('artemether_tab', ['required' => false , 'label' => false]); ?>
                                    ARTEMETHER-LUMEFANTRINE</td>
                                    <td >
                                        <?= $this->Form->input('artemether_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'artemether_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('artemether_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'artemether_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('artemether_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'artemether_prep', 'label' => false, 'required' => false]); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                   
                                    <?= $this->Form->checkbox('chloroquine_tab', ['required' => false , 'label' => false]); ?>
                                     (AL) CHLOROQUINE</td>
                                    <td >
                                        <?= $this->Form->input('chloroquine_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'chloroquine_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('chloroquine_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'chloroquine_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('chloroquine_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'chloroquine_prep', 'label' => false, 'required' => false]); ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                      
                                    <?= $this->Form->checkbox('primaquine_tab', ['required' => false , 'label' => false]); ?>
                                     PRIMAQUINE</td>
                                    <td >
                                        <?= $this->Form->input('primaquine_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'primaquine_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('primaquine_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'primaquine_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('primaquine_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'primaquine_prep', 'label' => false, 'required' => false]); ?>
                                    </td>
                                </tr>

                                <tr>

                                    <td>
                             
                                     <?= $this->Form->checkbox('quinine_tab', ['required' => false , 'label' => false]); ?>

                                    QUININE TAB</td>
                                    <td >
                                        <?= $this->Form->input('quinine_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'quinine_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('quinine_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'quinine_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('quinine_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'quinine_prep', 'label' => false, 'required' => false]); ?>
                                    </td>

                                </tr>


                                <tr>
                                    <td>
                                              <?= $this->Form->checkbox('quinine_ampules_tab', ['required' => false , 'label' => false]); ?>
                               
                                    QUININE AMPULES

                                    </td>
                                    <td >
                                        <?= $this->Form->input('quinine_ampules_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'quinine_ampules_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('quinine_ampules_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'quinine_ampules_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('quinine_ampules_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'quinine_ampules_prep', 'label' => false, 'required' => false]); ?>
                                    </td>

                                </tr>

                                <tr>
                                   <td>
                         
                                   <?= $this->Form->checkbox('tetracycline_tab', ['required' => false , 'label' => false]); ?>
                                   TETRACYCLINE

                                   </td>
                                    <td >
                                        <?= $this->Form->input('tetracycline_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'tetracycline_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('tetracycline_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'tetracycline_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('tetracycline_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'tetracycline_prep', 'label' => false, 'required' => false]); ?>
                                    </td>

                                </tr>

                                <tr>

                                   <td>
                                       <?= $this->Form->checkbox('doxycycline_tab', ['required' => false , 'label' => false]); ?>
                        
                                     DOXYCYCLINE</td>
                                    <td >
                                        <?= $this->Form->input('doxycycline_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'doxycycline_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('doxycycline_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'doxycycline_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('doxycycline_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'doxycycline_prep', 'label' => false, 'required' => false]); ?>
                                    </td>


                                </tr>
                                <tr>

                                   <td>
                                          <?= $this->Form->checkbox('clindamycin_tab', ['required' => false , 'label' => false]); ?>
                        
                                   CLINDAMYCIN</td>
                                    <td >
                                        <?= $this->Form->input('clindamycin_qty', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'clindamycin_qty', 'label' => false, 'required' => false]); ?>
                                    </td>
                                    <td >
                                        <?= $this->Form->input('clindamycin_started', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'clindamycin_started', 'label' => false, 'required' => false,'minYear' => date('Y') - 5, 'maxYear' => date('Y')]); ?>
                                    </td>
                                    <td >
                                      <?= $this->Form->input('clindamycin_prep', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'clindamycin_prep', 'label' => false, 'required' => false]); ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                     <?= $this->Form->checkbox('no_medecine', ['required' => false , 'label' => false]); ?>
                                    NO MEDECINE GIVEN</td>
                                    <td >
                                    </td>
                                    <td >
                         
                                    </td>
                                    <td >
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                        
                        <div class="well well-lg">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group ">

                                        <?= $this->Form->input('drug_intake_date', ['autocomplete' => 'off', 'class' => 'form-control', 'type' => 'date', 'default' => '', 'empty' => true, 'id' => 'drug_intake_date', 'label' => false,'minYear' => date('Y') - 10, 'maxYear' => date('Y')]); ?>
                                         <i class="fa fa-calendar "></i>
                                        <label>DATE MEDECINE GIVEN</label>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group ">
                    
                                        <?php
                                        echo $this->Form->radio(
                                            'supervised_initial_intake',
                                            [
                                                ['value' => 1, 'text' => 'YES'],
                                                ['value' => 0, 'text' => 'NO']
                                            ]
                                        );
                                        ?>
                                        <label for="">SUPERVISED INTAKE OF INITIAL DOSE?</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <p>HEALTH WORKER WHO ADMINISTER THE MEDECINE</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                            <?= $this->Form->input('hw_last_name', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'hw_last_name', 'label' => false, 'required' => false]); ?>
                                                <label for="worker_lastname">LAST NAME</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group ">

                                            <?= $this->Form->input('hw_first_name', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'hw_first_name', 'label' => false, 'required' => false]); ?>
                                                <label for="worker_firstname">FIRST NAME</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                            <?= $this->Form->input('hw_middle_name', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'hw_middle_name', 'label' => false, 'required' => false]); ?>
                                                <label for="worker_middlename">M.I.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group ">
                                    <?= $this->Form->input('hw_designation', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'hw_designation', 'label' => false, 'required' => false]); ?>
                                        <label for="designation">DESIGNATION</label>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group ">
                                       <?php
                                        echo $this->Form->radio(
                                            'disposition',
                                            [
                                                ['value' => 1, 'text' => 'REFERRED'],
                                                ['value' => 2, 'text' => 'DIED']
                                            ]
                                        );
                                        ?><br>
                                        <label for="">DISPOSITION OF PATIENT (TO BE FILLED UP WHEN CONSULTING OR PATIENT)</label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-sm-8">
                                    <div class="form-group ">
                                        <?= $this->Form->input('referred_to', ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'referred_to', 'label' => false, 'required' => false]); ?>

                                        <label for="referred_to">REFERRED TO</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-8">
                                    <div class="form-group ">
                                    <?= $this->Form->input('referral_reason', ['autocomplete' => 'off', 'type' => 'textarea', 'rows' => '3', 'id' => 'referral_reason',  'class' => 'form-control', 'label' => false, 'required' => false]); ?>
                                        <label for="reason_referral">REASON FOR REFERRAL</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-default btn-lg">Save</button>
                    </div>

                    <?php echo $this->Form->end(); ?>



                </div>




            </div>

        </div>
    </div>
</section>




<?php $this->start('scriptCss'); ?>

<?php $this->end() ?>

<?php $this->start('scriptBottom'); ?>



    <script type="text/javascript">
        $(document).ready(function(){
          
            // $('.datepicker').datepicker();

            // function facility(){
            //     $(".facility_id").load("/mprfCases/facilities", function(responseTxt, statusTxt, xhr){
            //         if(statusTxt == "success")
            //             console.log("facilities loaded successfully! test " + statusTxt);
            //         if(statusTxt == "error")
            //             console.log("Error: facilities  " + xhr.status + ": " + xhr.statusText);
            //     });
            // }
         

            $('#region_id').on('change',function(){
                var id    = $(this).val();
                // var code    = $(this).find('option:selected').data('id');
                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-province']) ?>", 
                    type: 'post',
                    dataType: 'json',
                    data: {
                        region_code : id
                    },
                    success: function(response){
                        if(response.json.error === false){
                            $('#province_id option').remove();
                            $('#municipality_id option').remove();
                            $('#barangay_id option').remove();
                            $('#sitio_id option').remove();

                            var html = '<option value="">(Choose Province)</option>';
                            $.map( response.json.provinces, function( value, index ) {
                                html += '<option value="'+ value.id +'" data-id="'+ value.province_code +'">' + value.description + '</option>';
                            });
                            $('#province_id').append(html);
                        }else{
                            // alert(response.json.message);
                        }
                    }
                });
            });


            $('#province_id').on('change',function(){
                var id    = $(this).val();
                //var code    = $(this).find('option:selected').data('id');
                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-municipality']) ?>",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        province_code : id
                    },
                    success: function(response){
                        if(response.json.error === false){
                            $('#municipality_id option').remove();
                            var html = '<option value="">(Choose Municipality/City)</option>';
                            $.map( response.json.municipalities, function( value, index ) {
                                html += '<option value="' + value.id + '" data-id="'+ value.municipality_code +'">' + value.description + '</option>';
                            });
                            $('#municipality_id').append(html);
                        }else{
                            // alert(response.json.message);
                        }
                    }
                });
            });

            $('#municipality_id').on('change',function(){
                var id    = $(this).val();
                //var code    = $(this).find('option:selected').data('id');
                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-barangay']) ?>",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        municipality_code : id
                    },
                    success: function(response){


                        if(response.json.error === false){

                            $('#barangay_id option').remove();
                            var html = '<option value="">(Choose Barangay)</option>';
                            $.map( response.json.barangays, function( value, index ) {
                                html += '<option value="'+ value.id +'" data-id="'+ value.barangay_code +'">' + value.description + '</option>';
                            });
                            $('#barangay_id').append(html);
                        }else{
                            // alert(response.json.message);
                        }
                    }
                });
            });

            $('#barangay_id').on('change',function(){
                var id    = $(this).val();
               // var code    = $(this).find('option:selected').data('id');
                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'regions', 'action' => 'ajax-sitio']) ?>",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        barangay_code : id
                    },
                    success: function(response){

                        if(response.json.error === false){

                            $('#sitio_id option').remove();
                            var html = '<option value="">(Choose Sitio)</option>';
                            $.map( response.json.sitios, function( value, index ) {
                                html += '<option value="'+ value.sitio_id +'" data-id="'+ value.sitio_id +'">' + value.description + '</option>';
                            });
                            $('#sitio_id').append(html);
                        }else{
                            // alert(response.json.message);
                        }
                    }
                });
            });



        });
    </script>


<?php $this->end() ?>


