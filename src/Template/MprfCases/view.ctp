<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mprf Case'), ['action' => 'edit', $mprfCase->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mprf Case'), ['action' => 'delete', $mprfCase->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mprfCase->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Mprf Cases'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mprf Case'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Incomings'), ['controller' => 'SmsIncomings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Incoming'), ['controller' => 'SmsIncomings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Facilities'), ['controller' => 'Facilities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Facility'), ['controller' => 'Facilities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regions'), ['controller' => 'Regions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Regions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Provinces'), ['controller' => 'Provinces', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Province'), ['controller' => 'Provinces', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipalities'), ['controller' => 'Municipalities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipality'), ['controller' => 'Municipalities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Barangays'), ['controller' => 'Barangays', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Barangay'), ['controller' => 'Barangays', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sitios'), ['controller' => 'Sitios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sitio'), ['controller' => 'Sitios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mprfCases view large-9 medium-8 columns content">
    <h3><?= h($mprfCase->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Sms Incoming') ?></th>
            <td><?= $mprfCase->has('sms_incoming') ? $this->Html->link($mprfCase->sms_incoming->id, ['controller' => 'SmsIncomings', 'action' => 'view', $mprfCase->sms_incoming->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $mprfCase->has('user') ? $this->Html->link($mprfCase->user->id, ['controller' => 'Users', 'action' => 'view', $mprfCase->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Source') ?></th>
            <td><?= h($mprfCase->source) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Facility') ?></th>
            <td><?= $mprfCase->has('facility') ? $this->Html->link($mprfCase->facility->id, ['controller' => 'Facilities', 'action' => 'view', $mprfCase->facility->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Template Code') ?></th>
            <td><?= h($mprfCase->template_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($mprfCase->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($mprfCase->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Middle Name') ?></th>
            <td><?= h($mprfCase->middle_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Weight') ?></th>
            <td><?= h($mprfCase->weight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gender') ?></th>
            <td><?= h($mprfCase->gender) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lessmonth') ?></th>
            <td><?= h($mprfCase->lessmonth) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ip Group') ?></th>
            <td><?= h($mprfCase->ip_group) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Occupation') ?></th>
            <td><?= h($mprfCase->occupation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Case Type') ?></th>
            <td><?= h($mprfCase->case_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Region') ?></th>
            <td><?= $mprfCase->has('region') ? $this->Html->link($mprfCase->region->id, ['controller' => 'Regions', 'action' => 'view', $mprfCase->region->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Province') ?></th>
            <td><?= $mprfCase->has('province') ? $this->Html->link($mprfCase->province->id, ['controller' => 'Provinces', 'action' => 'view', $mprfCase->province->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Municipality') ?></th>
            <td><?= $mprfCase->has('municipality') ? $this->Html->link($mprfCase->municipality->id, ['controller' => 'Municipalities', 'action' => 'view', $mprfCase->municipality->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Barangay') ?></th>
            <td><?= $mprfCase->has('barangay') ? $this->Html->link($mprfCase->barangay->id, ['controller' => 'Barangays', 'action' => 'view', $mprfCase->barangay->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sitio') ?></th>
            <td><?= $mprfCase->has('sitio') ? $this->Html->link($mprfCase->sitio->id, ['controller' => 'Sitios', 'action' => 'view', $mprfCase->sitio->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Temprature') ?></th>
            <td><?= h($mprfCase->temprature) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Other Complaint') ?></th>
            <td><?= h($mprfCase->other_complaint) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rdt No') ?></th>
            <td><?= h($mprfCase->rdt_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Parasaite Blood') ?></th>
            <td><?= h($mprfCase->parasaite_blood) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artemether Prep') ?></th>
            <td><?= h($mprfCase->artemether_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Chloroquine Prep') ?></th>
            <td><?= h($mprfCase->chloroquine_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primaquine Prep') ?></th>
            <td><?= h($mprfCase->primaquine_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Prep') ?></th>
            <td><?= h($mprfCase->quinine_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Ampules Prep') ?></th>
            <td><?= h($mprfCase->quinine_ampules_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tetracycline Prep') ?></th>
            <td><?= h($mprfCase->tetracycline_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Doxycycline Prep') ?></th>
            <td><?= h($mprfCase->doxycycline_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Clindamycin Prep') ?></th>
            <td><?= h($mprfCase->clindamycin_prep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Travel History Place') ?></th>
            <td><?= h($mprfCase->travel_history_place) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hw First Name') ?></th>
            <td><?= h($mprfCase->hw_first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hw Last Name') ?></th>
            <td><?= h($mprfCase->hw_last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hw Middle Name') ?></th>
            <td><?= h($mprfCase->hw_middle_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hw Designation') ?></th>
            <td><?= h($mprfCase->hw_designation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Referred To') ?></th>
            <td><?= h($mprfCase->referred_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Referral Reason') ?></th>
            <td><?= h($mprfCase->referral_reason) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Province Text') ?></th>
            <td><?= h($mprfCase->province_text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Municipality Text') ?></th>
            <td><?= h($mprfCase->municipality_text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Barangay Text') ?></th>
            <td><?= h($mprfCase->barangay_text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sitio Text') ?></th>
            <td><?= h($mprfCase->sitio_text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($mprfCase->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mprfCase->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Age') ?></th>
            <td><?= $this->Number->format($mprfCase->age) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pregnant') ?></th>
            <td><?= $this->Number->format($mprfCase->pregnant) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Chief Complaint') ?></th>
            <td><?= $this->Number->format($mprfCase->chief_complaint) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('C Other Complaint') ?></th>
            <td><?= $this->Number->format($mprfCase->c_other_complaint) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('M Pf') ?></th>
            <td><?= $this->Number->format($mprfCase->m_pf) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('R Pfpv') ?></th>
            <td><?= $this->Number->format($mprfCase->r_pfpv) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('R Pv') ?></th>
            <td><?= $this->Number->format($mprfCase->r_pv) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cdx') ?></th>
            <td><?= $this->Number->format($mprfCase->cdx) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Clinical Diagnosis') ?></th>
            <td><?= $this->Number->format($mprfCase->clinical_diagnosis) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artemether Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->artemether_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artemether Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->artemether_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Chloroquine Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->chloroquine_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Chloroquine Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->chloroquine_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primaquine Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->primaquine_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primaquine Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->primaquine_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->quinine_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->quinine_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Ampules Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->quinine_ampules_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Ampules Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->quinine_ampules_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tetracycline Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->tetracycline_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tetracycline Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->tetracycline_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Doxycycline Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->doxycycline_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Doxycycline Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->doxycycline_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Clindamycin Tab') ?></th>
            <td><?= $this->Number->format($mprfCase->clindamycin_tab) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Clindamycin Qty') ?></th>
            <td><?= $this->Number->format($mprfCase->clindamycin_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No Medecine') ?></th>
            <td><?= $this->Number->format($mprfCase->no_medecine) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('History Illness') ?></th>
            <td><?= $this->Number->format($mprfCase->history_illness) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Validated') ?></th>
            <td><?= $this->Number->format($mprfCase->validated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($mprfCase->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($mprfCase->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Consult Date') ?></th>
            <td><?= h($mprfCase->consult_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Death Date') ?></th>
            <td><?= h($mprfCase->death_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Birthdate') ?></th>
            <td><?= h($mprfCase->birthdate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Blood Exam Date') ?></th>
            <td><?= h($mprfCase->blood_exam_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Blood Exam Result Date') ?></th>
            <td><?= h($mprfCase->blood_exam_result_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Disease Start Date') ?></th>
            <td><?= h($mprfCase->disease_start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artemether Started') ?></th>
            <td><?= h($mprfCase->artemether_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Chloroquine Started') ?></th>
            <td><?= h($mprfCase->chloroquine_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primaquine Started') ?></th>
            <td><?= h($mprfCase->primaquine_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Started') ?></th>
            <td><?= h($mprfCase->quinine_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quinine Ampules Started') ?></th>
            <td><?= h($mprfCase->quinine_ampules_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tetracycline Started') ?></th>
            <td><?= h($mprfCase->tetracycline_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Doxycycline Started') ?></th>
            <td><?= h($mprfCase->doxycycline_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Clindamycin Started') ?></th>
            <td><?= h($mprfCase->clindamycin_started) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Drug Intake Date') ?></th>
            <td><?= h($mprfCase->drug_intake_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Microscopy') ?></th>
            <td><?= $mprfCase->microscopy ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rdt') ?></th>
            <td><?= $mprfCase->rdt ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('R Pf') ?></th>
            <td><?= $mprfCase->r_pf ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pm') ?></th>
            <td><?= $mprfCase->pm ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('M Pfpv') ?></th>
            <td><?= $mprfCase->m_pfpv ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pfpm') ?></th>
            <td><?= $mprfCase->pfpm ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pvpm') ?></th>
            <td><?= $mprfCase->pvpm ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('M Pv') ?></th>
            <td><?= $mprfCase->m_pv ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Po') ?></th>
            <td><?= $mprfCase->po ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nf') ?></th>
            <td><?= $mprfCase->nf ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pfnf') ?></th>
            <td><?= $mprfCase->pfnf ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Travel History') ?></th>
            <td><?= $mprfCase->travel_history ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('History Blood Trans') ?></th>
            <td><?= $mprfCase->history_blood_trans ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supervised Initial Intake') ?></th>
            <td><?= $mprfCase->supervised_initial_intake ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Disposition') ?></th>
            <td><?= $mprfCase->disposition ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Remarks') ?></h4>
        <?= $this->Text->autoParagraph(h($mprfCase->remarks)); ?>
    </div>
</div>
