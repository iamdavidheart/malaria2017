<section class="main-container">
    <div class="container">
        <div class="row">

        <div class="col-md-12">
            <div class="pull-right">
                <a class="btn btn-animated btn-gray-transparent btn-sm" href="<?= $this->Url->build(['controller' => 'mprf-cases', 'action' => 'add']) ?>">Add MPRF Case <i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div class="col-md-12">

                <h3><?= __('MPRF Cases') ?></h3>
                <table class="table table-bordered table-striped " id="mrfcases" >
                    <thead>
                        <tr>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('ID') ?></th>
                        
                            <th scope="col" class="text-center" width="20%"><?= $this->Paginator->sort('FACILITY') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('CONSULT DATE') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('DEATH DATE') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('NAME') ?></th>
                            <th scope="col" class="text-center"><?= $this->Paginator->sort('REPORTER') ?></th>
                            <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($mprfCases as $mprfCase): ?>
                        <tr>
                            <td><?= $this->Number->format($mprfCase->id) ?></td>
                            <td>
                            <?php
                                if(!empty($mprfCase->facility)){
                                    echo $mprfCase->facility->province->description. ', '.$mprfCase->facility->municipality->description.',';
                                    echo (!empty($mprfCase->facility->barangay))? $mprfCase->facility->barangay->description.', ' : 'No Barangay,' ;
                                    echo (!empty($mprfCase->facility->sitio))? $mprfCase->facility->sitio->description.', ' : ' No Sitio, ' ;
                                }else{
                                    echo 'No Facility';
                                }
                            ?>
                            </td>
                            <td><?= h($mprfCase->consult_date) ?></td>
                            <td><?= h($mprfCase->death_date) ?></td>
                            <td><?= h($mprfCase->first_name) ?> <?= h($mprfCase->last_name) ?></td>
                   
                              <td><?= $mprfCase->user->full_name ?></td>
                            <td class="actions text-center">


                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-edit"></i> Edit'),
                                        ['action' => 'edit', $mprfCase->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm ',
                                            'escape' => false
                                        ]);
                                    ?>

                      

                                    <?= $this->Form->postLink( __('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $mprfCase->id],
                                        ['class' => 'btn btn-gray-transparent btn-sm', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $mprfCase->id )]  ); ?>



                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

        </div>


        </div>
    </div>
</section>



<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#mrfcases').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>