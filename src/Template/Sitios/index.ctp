



<section class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <h3><?= __('SITIO') ?></h3>
                <table class="table table-bordered table-striped " id="sitio" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th class="text-center">PROVINCE</th>
                                    <th class="text-center">MUNICIPALITY</th>
                                    <th class="text-center">BARANGAY</th>
                                    <th class="text-center">SITIO</th>
                                    <th class="actions text-center" style="width:100px;"><?= __('ACTION') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sitios as $sitio): ?>
                                <tr>
                                    <td><?= $sitio->province->description ?></td>
                                    <td><?= $sitio->municipality->description ?></td>
                                    <td><?= $sitio->barangay->description ?></td>
                                    <td><?= $sitio->description ?></td>
                                    
                                    <td class="actions text-center">

              
                                  <?php 
                                    echo $this->Html->link(
                                        __('<i class="fa fa-edit"></i> Edit'),
                                        ['action' => 'edit', $sitio->id],
                                        [
                                            'class' => 'btn btn-gray-transparent btn-sm ',
                                            'escape' => false
                                        ]);
                                    ?>


                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                </table>




            </div>
        </div>
    </div>
</section>





<?php $this->start('scriptCss'); ?>
    <?= $this->Html->css('../js/plugin/datatable/css/jquery.dataTables.css'); ?>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/css/dataTables.checkboxes.css" rel="stylesheet" />
<?php $this->end() ?>


<?php $this->start('scriptFooter'); ?>
    <?=  $this->Html->script('plugin/datatable/js/jquery.dataTables.min.js'); ?>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script>



    <script type="text/javascript">

        $(document).ready(function () {

            var table = $('#sitio').DataTable({
                    // columnDefs: [
                    //     {
                    //         targets: 0,
                    //         checkboxes: {
                    //         selectRow: true
                    //         }
                    //     }
                    // ],
                    //     select: {
                    //     style: 'multi'
                    // },
                    "pageLength": 15,
                    "bAutoWidth": false,
                    "bDeferRender": true,
                    "fnDrawCallback": function() {
                        $('.editButton').on('click', function() {

                        });
                    }
                });


        });

    </script>
<?php $this->end() ?>




