<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userContact->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userContact->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Contacts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userContacts form large-9 medium-8 columns content">
    <?= $this->Form->create($userContact) ?>
    <fieldset>
        <legend><?= __('Edit User Contact') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('type');
            echo $this->Form->input('value');
            echo $this->Form->input('prime_setup');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
