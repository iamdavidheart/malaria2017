<?php
namespace App\Controller\Component;

use App\Model\Entity\User;
use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class SessionActivityComponent extends Component
{

    /**
     * Request object
     *
     * @var \Cake\Network\Request
     */
    protected $_request;

    /**
     * Instance of the Session object
     *
     * @return void
     */
    protected $_session;

    /**
     * Initialize properties.
     *
     * @param array $config The config data.
     * @return void
     */
    public function initialize(array $config)
    {

        $controller = $this->_registry->getController();
        $this->_request = $controller->request;
        $this->_session = $controller->request->session();


    }

    /**
     * Startup event to trace the user on the website.
     *
     * @param Event $event The event that was fired.
     *
     * @return void
     */
    public function startup(Event $event)
    {
  

 
        if (empty($this->_session->id())) {
            $this->_session->start();
           return;
        }


        $sessions = TableRegistry::get('Sessions');

        $prefix = isset($this->_request['prefix']) ? $this->_request['prefix'] . '/' : '';
        $controller = $prefix . $this->_request['controller'];
        $action = $this->_request['action'];
        $params = serialize($this->_request->pass);
        $expires = time() + ini_get('session.gc_maxlifetime');

        //@codingStandardsIgnoreStart
        $user_id = $this->_session->read('Auth.User.id');

        //@codingStandardIgnoreEnd
        $record = compact('controller', 'action', 'params', 'expires', 'user_id');

        $record[$sessions->primaryKey()] = $this->_session->id();
        $sessions->save(new Entity($record));


    }


}
