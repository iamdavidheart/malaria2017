<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class MalariaInitiativeComponent extends Component
{
    public function generate_code($s_length = null, $n_length = null) {

			$str='';
			$num='';
			$total_length = $s_length + $n_length ;
			$continue = true;
			do {

				if ($s_length) {
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
					$str = '';
					$size = strlen( $chars );
					for( $i = 0; $i < $s_length; $i++ ) {
						$str .= $chars[ rand( 0, $size - 1 ) ];
					}
					$str = strtoupper($str);
				} else {
					$str = '';
				}	
	
				if ($n_length) {
					$nums = "123456789";
					$num = '';
					$size = strlen($nums);
					for( $i = 0; $i < $n_length; $i++ ) {
						$num .= $nums[ rand( 0, $size - 1 ) ];
					}
					$num = strtoupper($num);
				} else {
					$num = '';
				}
		
					$gen_string = strlen($str) + strlen($num);
					
					if ($total_length == $gen_string) {
						$continue = false;
					}
			
			} while  ($continue);
			
			return $str.$num;

	}


	protected function word_break($string, $limit, $break=".", $pad="...") {
	    // return with no change if string is shorter than $limit    
	    if(strlen($string) <= $limit)   
	        return $string;   
	    // is $break present between $limit and the end of the string?    
	    if(false !== ($breakpoint = strpos($string, $break, $limit))) {  
	        if($breakpoint < strlen($string) - 1) {   
	            $string = substr($string, 0, $breakpoint) . $pad;   
	        }   
	    }  
	    return $string;   
	}  

	
}
