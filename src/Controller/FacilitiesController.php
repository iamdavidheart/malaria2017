<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Facilities Controller
 *
 * @property \App\Model\Table\FacilitiesTable $Facilities
 */
class FacilitiesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FacilityCategories', 'Regions', 'Provinces', 'Municipalities', 'Barangays', 'Sitios']
        ];
        $facilities = $this->paginate($this->Facilities);

        $this->set(compact('facilities'));
        $this->set('_serialize', ['facilities']);
    }

    /**
     * View method
     *
     * @param string|null $id Facility id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $facility = $this->Facilities->get($id, [
            'contain' => ['FacilityCategories', 'Regions', 'Provinces', 'Municipalities', 'Barangays', 'Sitios', 'Users', 'Items', 'MprfCases']
        ]);

        $this->set('facility', $facility);
        $this->set('_serialize', ['facility']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // $facility = $this->Facilities->newEntity();
        // if ($this->request->is('post')) {
        //     $facility = $this->Facilities->patchEntity($facility, $this->request->data);
        //     if ($this->Facilities->save($facility)) {
        //         $this->Flash->success(__('The facility has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The facility could not be saved. Please, try again.'));
        //     }
        // }
        // $facilityCategories = $this->Facilities->FacilityCategories->find('list', ['limit' => 200]);
        // $regions = $this->Facilities->Regions->find('list', ['limit' => 200]);
        // $provinces = $this->Facilities->Provinces->find('list', ['limit' => 200]);
        // $municipalities = $this->Facilities->Municipalities->find('list', ['limit' => 200]);
        // $barangays = $this->Facilities->Barangays->find('list', ['limit' => 200]);
        // $sitios = $this->Facilities->Sitios->find('list', ['limit' => 200]);
        // $this->set(compact('facility', 'facilityCategories', 'regions', 'provinces', 'municipalities', 'barangays', 'sitios'));
        // $this->set('_serialize', ['facility']);
        $facility = $this->Facilities->newEntity();
        if ($this->request->is('post')) {



            $this->request->data['created'] = date('Y-m-d h:i:s');
            $this->request->data['user_id'] = $this->Auth->user('id');


            $facilityD = $this->Facilities
            ->find()
            ->where(['Facilities.region_id' => $this->request->data['region_id'], 
                'Facilities.province_id' => $this->request->data['province_id'],
                'Facilities.municipality_id'  => $this->request->data['municipality_id'],
                'Facilities.barangay_id'  => $this->request->data['barangay_id'],
                'Facilities.facility_category_id'  => $this->request->data['facility_category_id'],
                'Facilities.sitio_id'  => $this->request->data['sitio_id'],
                ])->first();



            if(is_null($facilityD)){
                $facility = $this->Facilities->patchEntity($facility, $this->request->data);
                if ($this->Facilities->save($facility)) {
                    $this->Flash->success(__('The facility has been saved.'.$facility->id));
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('The facility could not be saved. Please, try again.'));
                }
            }else{
                     $this->Flash->error(__('Error Duplicate Data.'));
            }


        }

        $facilityCategories = $this->Facilities->FacilityCategories->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ]);


        $regions = $this->Facilities->Regions->find();

        $this->set(compact('facility', 'facilityCategories', 'regions'));
        $this->set('_serialize', ['facility']);

    }

    /**
     * Edit method
     *
     * @param string|null $id Facility id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // $facility = $this->Facilities->get($id, [
        //     'contain' => []
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     $facility = $this->Facilities->patchEntity($facility, $this->request->data);
        //     if ($this->Facilities->save($facility)) {
        //         $this->Flash->success(__('The facility has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The facility could not be saved. Please, try again.'));
        //     }
        // }
        // $facilityCategories = $this->Facilities->FacilityCategories->find('list', ['limit' => 200]);
        // $regions = $this->Facilities->Regions->find('list', ['limit' => 200]);
        // $provinces = $this->Facilities->Provinces->find('list', ['limit' => 200]);
        // $municipalities = $this->Facilities->Municipalities->find('list', ['limit' => 200]);
        // $barangays = $this->Facilities->Barangays->find('list', ['limit' => 200]);
        // $sitios = $this->Facilities->Sitios->find('list', ['limit' => 200]);
        // $this->set(compact('facility', 'facilityCategories', 'regions', 'provinces', 'municipalities', 'barangays', 'sitios'));
        // $this->set('_serialize', ['facility']);

        $facility = $this->Facilities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $facility = $this->Facilities->patchEntity($facility, $this->request->data);
            if ($this->Facilities->save($facility)) {
                $this->Flash->success(__('The facility has been saved.'));
                return $this->redirect(['action' => 'edit', $facility->id]);
            } else {
                $this->Flash->error(__('The facility could not be saved. Please, try again.'));
            }
        }




        $facilityCategories = $this->Facilities->FacilityCategories->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ]);

        $regions = $this->Facilities->Regions->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ]);

        $provinces = $this->Facilities->Regions->Provinces->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Provinces.region_id' => $facility->region_id]);

        $municipalities = $this->Facilities->Regions->Provinces->Municipalities->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Municipalities.province_id' => $facility->province_id]);

        $barangays = $this->Facilities->Regions->Provinces->Municipalities->Barangays->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Barangays.municipality_id' => $facility->municipality_id]);

        $sitios = $this->Facilities->Regions->Provinces->Municipalities->Barangays->Sitios->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Sitios.barangay_id' => $facility->barangay_id]);


        $this->set(compact('facility', 'facilityCategories', 'regions', 'provinces', 'municipalities', 'barangays', 'sitios'));
        $this->set('_serialize', ['facility']);
        
    }

    /**
     * Delete method
     *
     * @param string|null $id Facility id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $facility = $this->Facilities->get($id);
        if ($this->Facilities->delete($facility)) {
            $this->Flash->success(__('The facility has been deleted.'));
        } else {
            $this->Flash->error(__('The facility could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
