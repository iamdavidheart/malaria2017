<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{



    public function ajaxSetMobile()
    {

        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }

        $json = [
            'message' => '',
            'error'   => false
        ];

        $id = $this->request->data['id'];
        $user_id = $this->request->data['user_id'];

        $this->loadModel('UserContacts');

        $contacts = $this->UserContacts->find()->where(['user_id' => $user_id]);

        foreach ($contacts as $key => $value) {
            $value->prime_setup = 0;
            $this->UserContacts->save($value);
        }


        $setContact = $this->UserContacts->get($id);
        $setContact->prime_setup = 1;
        $this->UserContacts->save($setContact);


        $this->set(compact('json'));
        $this->set('_serialize', ['json']);


    }


    public function loadContacts()
    {
        $this->loadModel('UserContacts');
        $id = $this->request->data['id'];
        $btn = $this->request->data['btn'];
        $contacts = $this->UserContacts->find()->where(['user_id' => $id]);
        $this->set(compact('contacts', 'btn'));
        $this->set('_serialize', [ 'contacts', 'btn']);
    }



    public function activateUser()
    {

        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $json = [
            'error' => false,
            'message' => 'Success'
        ];

        $user_ids =  $this->request->data['user_id'];
        if(!empty($user_ids)){

            $this->loadModel('SmsOutgoings');
            foreach ($user_ids as $key => $id) {
         
                $user = $this->Users
                    ->find()
                    ->where(['Users.id' => $id])
                    ->first();
                $user->activated = 1;
                if ($this->Users->save($user)) {

                    $users = $this->Users
                                ->find()
                                ->where(['Users.id' => $id])
                                ->contain(['UserContacts'])
                                ->first();
                    if(!empty($users->user_contacts)){
                        foreach ($users->user_contacts as $key => $user_contact) {

                            if(preg_match("/^(09|\+639|639)\d{9}$/", $user_contact->value )){
                                $smsOutgoings = $this->SmsOutgoings->newEntity();
                                $smsOutgoings->message      = 'UserCode :'.$users->code.', account has been activated.';
                                $smsOutgoings->created      =  date('Y-m-d h:i:s');
                                $smsOutgoings->source       = 'WEB';
                                $smsOutgoings->receiver_no  = $user_contact->value;
                                $smsOutgoings->user_id      = $id;
                                $smsOutgoings->status       = 'pending';
                                $smsOutgoings->box          = 10;
                                $smsOutgoings->type         = 'sms reply';
                                if ($this->SmsOutgoings->save($smsOutgoings)) {
                                } else {
                                    $json['error'] = true;
                                    $json['message'] = __('The Error activation alert message, Please try again.');
                                }

                            }
                        }
                    }
                } 
            }
        }
        $this->set(compact('json'));
        $this->set('_serialize', ['json']);
    }

    public function deactivateUser()
    {

        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $json = [
            'error' => false,
            'message' => 'Success'
        ];

        $user_ids =  $this->request->data['user_id'];
        if(!empty($user_ids)){

            $this->loadModel('SmsOutgoings');
            foreach ($user_ids as $key => $id) {
         
                $user = $this->Users
                    ->find()
                    ->where(['Users.id' => $id])
                    ->first();

                $user->activated = 0;

                if ($this->Users->save($user)) {

                    $users = $this->Users
                                ->find()
                                ->where(['Users.id' => $id])
                                ->contain(['UserContacts'])
                                ->first();
                    if(!empty($users->user_contacts)){
                        foreach ($users->user_contacts as $key => $user_contact) {

                            if(preg_match("/^(09|\+639|639)\d{9}$/", $user_contact->value )){
                                $smsOutgoings = $this->SmsOutgoings->newEntity();
                                $smsOutgoings->message      = 'Usercode :'.$users->code. ', account has been deactivated.';
                                $smsOutgoings->created      =  date('Y-m-d h:i:s');
                                $smsOutgoings->source       = 'WEB';
                                $smsOutgoings->receiver_no  = $user_contact->value;
                                $smsOutgoings->user_id      = $id;
                                $smsOutgoings->status       = 'pending';
                                $smsOutgoings->box          = 10;
                                $smsOutgoings->type         = 'sms reply';
                                if ($this->SmsOutgoings->save($smsOutgoings)) {
                                } else {
                                    $json['error'] = true;
                                    $json['message'] = __('The Error activation alert message, Please try again.');
                                }

                            }
                        }
                    }
                } 
            }
        }
        $this->set(compact('json'));
        $this->set('_serialize', ['json']);
    }



    /**
     * Logout an user.
     *
     * @return \Cake\Network\Response
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    private function prefixNumber($number, $prefix){
        $nb = "";
        if(preg_match("/^(639)\d{9}$/", $number )){
            $nb = substr_replace($number,$prefix,0,2);
        }elseif(preg_match("/^(09)\d{9}$/", $number )){
            $nb = substr_replace($number,$prefix,0,1);
        }else{
            $nb = substr_replace($number,$prefix,0,3);
        }
        return $nb;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {



        // $this->loadModel('ShareLoads');
        // $this->loadModel('SmsOutgoings');

        // $shareloads = $this->ShareLoads->find()->where(['status' => 'PENDING']);
        // $status = 'PENDING';
        // foreach ($shareloads as $key => $shareload) {

        //     if($shareload->network == 'GLOBE'){

        //         if(preg_match("/^(09|\+639|639)\d{9}$/", $shareload->number )){

        //             $nb = $this->prefixNumber($shareload->number, 2);
                    
        //             $smsOutgoings = $this->SmsOutgoings->newEntity();
        //             $smsOutgoings->message      = 2;
        //             $smsOutgoings->created      =  date('Y-m-d h:i:s');
        //             $smsOutgoings->source       = 'G-REQUEST';
        //             $smsOutgoings->receiver_no  = $nb;
        //             $smsOutgoings->user_id      = $shareload->user_id;
        //             $smsOutgoings->status       = 'pending';
        //             $smsOutgoings->box          = 10;
        //             $smsOutgoings->type         = 'request';
        //             if ($this->SmsOutgoings->save($smsOutgoings)) {
        //                 $status = 'PROCESSING';
        //             }
        //         }
        //     }


        //     $share = $this->ShareLoads->get($shareload->id);
        //     $share->status = $status;
        //     $this->ShareLoads->save($share);
        // }



        // die;

        $users = $this->Users->find()
                ->contain([
                'Facilities.FacilityCategories', 
                'Facilities.Provinces',
                'Facilities.Municipalities',
                'Facilities.Barangays',
                'Facilities.Sitios',
                'Roles'

            ]);

        $users->order('Users.id DESC');

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Designations', 'Facilities', 'Barangays', 'CompanyTypes', 'Distributions', 'ItemDetails', 'Items', 'MprfCases', 'MprfExcels', 'Networks', 'Sitios', 'SmsIncomings', 'SmsOutgoings', 'TemplateNotifications', 'UserContacts']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // $user = $this->Users->newEntity();
        // if ($this->request->is('post')) {
        //     $user = $this->Users->patchEntity($user, $this->request->data);
        //     if ($this->Users->save($user)) {
        //         $this->Flash->success(__('The user has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The user could not be saved. Please, try again.'));
        //     }
        // }
        // $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        // $designations = $this->Users->Designations->find('list', ['limit' => 200]);
        // $this->set(compact('user', 'roles', 'designations'));
        // $this->set('_serialize', ['user']);

        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {

            $continue = true;
            do {
                $code = $this->MalariaInitiative->generate_code(3,2);
                if( !$this->Users->find()->where(['code' => $code])->count() ){
                    $continue = false;
                }
            } while ($continue);

            $this->request->data['code'] = $code;
            $this->request->data['created'] = date('Y-m-d h:i:s');
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['password'] = 'password';

            $user = $this->Users->patchEntity($user, $this->request->data, [
                'associated' => [
                    'UserContacts' => [
                        'accessibleFields' => ['id' => true]
                    ]  
                ]
            ]);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'edit', $user->id]);
            } else {

                $this->Flash->error(__('The user could not be saved. Please, try again.'));
                debug($user->errors());
                die;
            }
        }

        $designations = $this->Users->Designations->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);


        $this->loadModel('Facilities');
        $facilities = $this->Facilities->find()->contain(['FacilityCategories','Regions', 'Municipalities', 'Provinces', 'Barangays', 'Sitios']);


        $this->set(compact('user', 'designations', 'roles', 'facilities'));
        $this->set('_serialize', ['user']);


    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // $user = $this->Users->get($id, [
        //     'contain' => []
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     $user = $this->Users->patchEntity($user, $this->request->data);
        //     if ($this->Users->save($user)) {
        //         $this->Flash->success(__('The user has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The user could not be saved. Please, try again.'));
        //     }
        // }
        // $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        // $designations = $this->Users->Designations->find('list', ['limit' => 200]);
        // $this->set(compact('user', 'roles', 'designations'));
        // $this->set('_serialize', ['user']);


 $user = $this->Users->get($id, [
            'contain' => [ 'Designations', 'UserContacts']
        ]);


        $user_id = $this->Auth->user('id');

        $user = $this->Users->get($id, [
            'contain' => [ 'Designations', 'UserContacts']
        ]);

        if($user_id != $user->id){
            if($this->Auth->user('role_id') != 1){

                echo
                $this->Flash->error(__('Sorry account has no permission to access other users.'));
               return $this->redirect(array('controller' => 'pages'));
            }
        }




        if ($this->request->is(['patch', 'post', 'put'])) {


            $method = ($this->request->data['method']) ? $this->request->data['method'] : false;
            switch ($method) {

                case "m-users":

           // debug($this->request->data);
           //          die;
                
                    $user = $this->Users->patchEntity($user, $this->request->data, [
                        'associated' => [
                            'UserContacts' => [
                                'accessibleFields' => ['id' => true]
                            ]  
                        ]
                    ]);
          
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('The user has been saved.'));
                    } else {

                    debug($user->errors());
                    die;
                        $this->Flash->error(__('The user could not be saved. Please, try again.'));
                    }

                    break;

                case "m-facility":
                    $user = $this->Users->patchEntity($user, $this->request->data);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('The user has been saved.'));
                    } else {
                        $this->Flash->error(__('The user could not be saved. Please, try again.'));
                    }

                    break;

                case "m-changepass":

          
                    $data = $this->request->data;
                    if (!isset($data['current_password']) || !isset($data['password']) || !isset($data['confirm_password'])) {
                        $this->set(compact('user', 'oldEmail'));
                        return $this->Flash->error(__("Please, complete all fields !"));
                    }

                    if (!(new DefaultPasswordHasher)->check($data['current_password'], $user->password)) {
                        $this->set(compact('user', 'oldEmail'));
                        return $this->Flash->error(__("Your old password don't match !"));
                    }

                    $this->Users->patchEntity($user, $this->request->data(), ['validate' => 'password']);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__("Your password has been changed !"));
                    }


                    break;

            }
            return $this->redirect(['action' => 'edit', $user->id]);

        }


        $this->loadModel('Facilities');
        $facilities = $this->Facilities->find()->contain(['FacilityCategories','Regions', 'Municipalities', 'Provinces', 'Barangays', 'Sitios']);



        $designations = $this->Users->Designations->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);



        $this->set(compact('user', 'facilities', 'designations', 'roles', 'userContacts'));
        $this->set('_serialize', ['user']);

        
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
