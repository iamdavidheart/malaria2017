<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Regions Controller
 *
 * @property \App\Model\Table\RegionsTable $Regions
 */
class RegionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

            if($this->Auth->user('role_id') != 1){

                $this->Flash->error(__('Sorry account has no permission to access regions module.'));
               return $this->redirect(array('controller' => 'pages'));
            }


        $regions = $this->paginate($this->Regions);

        $this->set(compact('regions'));
        $this->set('_serialize', ['regions']);
    }

    /**
     * View method
     *
     * @param string|null $id Region id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $region = $this->Regions->get($id, [
            'contain' => ['Barangays', 'Facilities', 'MprfCases', 'Municipalities', 'Provinces', 'Sitios']
        ]);

        $this->set('region', $region);
        $this->set('_serialize', ['region']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $region = $this->Regions->newEntity();
        if ($this->request->is('post')) {
            $region = $this->Regions->patchEntity($region, $this->request->data);
            if ($this->Regions->save($region)) {
                $this->Flash->success(__('The region has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The region could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('region'));
        $this->set('_serialize', ['region']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Region id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $region = $this->Regions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $region = $this->Regions->patchEntity($region, $this->request->data);
            if ($this->Regions->save($region)) {
                $this->Flash->success(__('The region has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The region could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('region'));
        $this->set('_serialize', ['region']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Region id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $region = $this->Regions->get($id);
        if ($this->Regions->delete($region)) {
            $this->Flash->success(__('The region has been deleted.'));
        } else {
            $this->Flash->error(__('The region could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }




    public function ajaxProvince()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException("Ops! Something went wrong! ");
        }

        $json = [
            'error' => false,
            'message' => ''
        ];

        $this->loadModel('Provinces');

        $region_code = $this->request->data['region_code'];

        $provinces = $this->Provinces
                ->find()
                ->where([
                    'region_id' => $region_code 
                ]);

        if(empty($provinces->toArray())){

            $json['error'] = true;
            $json['message'] = 'Province are empty';
            $this->set(compact('json'));
            return;
        }


        $json['provinces'] = $provinces;

        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

    }


    public function ajaxMunicipality()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException("Ops! Something went wrong! ");
        }

        $json = [
            'error' => false,
            'message' => ''
        ];

        $this->loadModel('Municipalities');

        $province_code = $this->request->data['province_code'];

        $municipalities = $this->Municipalities
                ->find()
                ->where([
                    'Municipalities.province_id' => $province_code 
                ]);
        if(empty($municipalities->toArray())){

            $json['error'] = true;
            $json['message'] = 'Municipality are empty';
            $this->set(compact('json'));
            return;
        }


        $json['municipalities'] = $municipalities;

        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

    }


    public function ajaxBarangay()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException("Ops! Something went wrong! ");
        }

        $json = [
            'error' => false,
            'message' => ''
        ];

        $this->loadModel('Barangays');

        $municipality_code = $this->request->data['municipality_code'];

        $barangays = $this->Barangays
                ->find()
                ->where([
                    'Barangays.municipality_id' => $municipality_code 
                ]);
        if(empty($barangays->toArray())){

            $json['error'] = true;
            $json['message'] = 'Barangay are empty';
            $this->set(compact('json'));
            return;
        }

        $json['barangays'] = $barangays;

        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

    }

    public function ajaxSitio()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException("Ops! Something went wrong! ");
        }

        $json = [
            'error' => false,
            'message' => ''
        ];

        $this->loadModel('Sitios');

        $barangay_code = $this->request->data['barangay_code'];

        $sitios = $this->Sitios
                ->find()
                ->where([
                    'Sitios.barangay_id' => $barangay_code 
                ]);
        if(empty($sitios->toArray())){

            $json['error'] = true;
            $json['message'] = 'In this barangay sitio are empty';
            $this->set(compact('json'));
            return;
        }

        $json['sitios'] = $sitios;

        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

    }



    
}
