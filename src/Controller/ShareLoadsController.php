<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ShareLoads Controller
 *
 * @property \App\Model\Table\ShareLoadsTable $ShareLoads
 */
class ShareLoadsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $shareLoads = $this->paginate($this->ShareLoads);

        $this->set(compact('shareLoads'));
        $this->set('_serialize', ['shareLoads']);
    }

    /**
     * View method
     *
     * @param string|null $id Share Load id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $shareLoad = $this->ShareLoads->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('shareLoad', $shareLoad);
        $this->set('_serialize', ['shareLoad']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $shareLoad = $this->ShareLoads->newEntity();
        if ($this->request->is('post')) {
            $shareLoad = $this->ShareLoads->patchEntity($shareLoad, $this->request->data);
            if ($this->ShareLoads->save($shareLoad)) {
                $this->Flash->success(__('The share load has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The share load could not be saved. Please, try again.'));
            }
        }
        $users = $this->ShareLoads->Users->find('list', ['limit' => 200]);
        $this->set(compact('shareLoad', 'users'));
        $this->set('_serialize', ['shareLoad']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Share Load id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shareLoad = $this->ShareLoads->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $shareLoad = $this->ShareLoads->patchEntity($shareLoad, $this->request->data);
            if ($this->ShareLoads->save($shareLoad)) {
                $this->Flash->success(__('The share load has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The share load could not be saved. Please, try again.'));
            }
        }
        $users = $this->ShareLoads->Users->find('list', ['limit' => 200]);
        $this->set(compact('shareLoad', 'users'));
        $this->set('_serialize', ['shareLoad']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Share Load id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shareLoad = $this->ShareLoads->get($id);
        if ($this->ShareLoads->delete($shareLoad)) {
            $this->Flash->success(__('The share load has been deleted.'));
        } else {
            $this->Flash->error(__('The share load could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
