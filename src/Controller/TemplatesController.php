<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Templates Controller
 *
 * @property \App\Model\Table\TemplatesTable $Templates
 */
class TemplatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TemplateAlerts']
        ];
        $templates = $this->paginate($this->Templates);

        $this->set(compact('templates'));
        $this->set('_serialize', ['templates']);
    }

    /**
     * View method
     *
     * @param string|null $id Template id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $template = $this->Templates->get($id, [
            'contain' => ['TemplateAlerts', 'TemplateNotifications', 'TemplateReplies', 'TemplateSettings']
        ]);

        $this->set('template', $template);
        $this->set('_serialize', ['template']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $template = $this->Templates->newEntity();
        if ($this->request->is('post')) {
            $template = $this->Templates->patchEntity($template, $this->request->data);
            if ($this->Templates->save($template)) {
                $this->Flash->success(__('The template has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The template could not be saved. Please, try again.'));
            }
        }
        $templateAlerts = $this->Templates->TemplateAlerts->find('list', ['limit' => 200]);
        $this->set(compact('template', 'templateAlerts'));
        $this->set('_serialize', ['template']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Template id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // $template = $this->Templates->get($id, [
        //     'contain' => []
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     $template = $this->Templates->patchEntity($template, $this->request->data);
        //     if ($this->Templates->save($template)) {
        //         $this->Flash->success(__('The template has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The template could not be saved. Please, try again.'));
        //     }
        // }
        // $templateAlerts = $this->Templates->TemplateAlerts->find('list', ['limit' => 200]);
        // $this->set(compact('template', 'templateAlerts'));
        // $this->set('_serialize', ['template']);

   $template = $this->Templates->get($id, [
            'contain' => []
        ]);

        $this->loadModel('TemplateSettings');
        $templateSettings = $this->TemplateSettings->find()->where(['template_id' => $id])->first();
        // $temparray = array();
        // foreach ($templates as $value) {
        //     $temparray[$value->name] = $value->description;
        // }


        switch ($id) {
            case 6:
                $temp = 'reg';
                break;
            case 5:
                $temp = 'ost';
                break;
            case 4:
                $temp = 'dmw';
                break;
            case 3:
                $temp = 'dth';
                break;
            case 2:
                $temp = 'adg';
                break;

            default:
                $temp = 'nth';
                break;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {


            $method = ($this->request->data['method']) ? $this->request->data['method'] : false;
            switch ($method) {



                case "m-validation":

                    $templateSettings = $this->TemplateSettings->patchEntity($templateSettings, $this->request->data);
                    if ($this->TemplateSettings->save($templateSettings)) {
                        $this->Flash->success(__('The template settings has been saved.'));
                        return $this->redirect(['action' => 'edit', $template->id]);
                    } else {
                        $this->Flash->error(__('The template settings could not be saved. Please, try again.'));
                    }
                die;


                case "m-template":
                    $template = $this->Templates->patchEntity($template, $this->request->data);
                    if ($this->Templates->save($template)) {
                        $this->Flash->success(__('The template has been saved.'));
                        return $this->redirect(['action' => 'edit', $template->id]);
                    } else {
                        $this->Flash->error(__('The template could not be saved. Please, try again.'));
                    }
                    break;
                case "m-shareload":
                    $template = $this->Templates->patchEntity($template, $this->request->data);
                    if ($this->Templates->save($template)) {
                        $this->Flash->success(__('The template has been saved.'));
                        return $this->redirect(['action' => 'edit', $template->id]);
                    } else {
                        $this->Flash->error(__('The template could not be saved. Please, try again.'));
                    }
                case "m-notifcation":
                $this->loadModel('TemplateNotifications');
                 if(!empty($this->request->data['users'])){
                    $users = $this->request->data['users'];
                    foreach ($users as $key => $user) {
                        $tempNo = $this->TemplateNotifications->newEntity();
                        $tempNo->user_id = $user;
                        $tempNo->template_id = $template->id;
                        if ($this->TemplateNotifications->save($tempNo)) {}
                    }
                 }

                    break;

            }

        }




        $this->loadModel('Users');



        $users = $this->Users->find()->where(['Users.role_id IN' => [1] ])->contain(['Roles']);

        $this->loadModel('TemplateNotifications');
        $tempNotifcations = $this->TemplateNotifications
                    ->find()
                    ->contain(['Users.Roles'])
                    ->where(['TemplateNotifications.template_id' => $id]);


        $this->set(compact('template', 'users', 'tempNotifcations','templateSettings', 'temp'));
        $this->set('_serialize', ['template']);

        
    }

    /**
     * Delete method
     *
     * @param string|null $id Template id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $template = $this->Templates->get($id);
        if ($this->Templates->delete($template)) {
            $this->Flash->success(__('The template has been deleted.'));
        } else {
            $this->Flash->error(__('The template could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
