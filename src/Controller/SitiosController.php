<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Sitios Controller
 *
 * @property \App\Model\Table\SitiosTable $Sitios
 */
class SitiosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Barangays', 'Regions', 'Provinces', 'Municipalities']
        ];
        $sitios = $this->paginate($this->Sitios);

        $this->set(compact('sitios'));
        $this->set('_serialize', ['sitios']);
    }

    /**
     * View method
     *
     * @param string|null $id Sitio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sitio = $this->Sitios->get($id, [
            'contain' => ['Users', 'Barangays', 'Regions', 'Provinces', 'Municipalities', 'Facilities', 'MprfCases']
        ]);

        $this->set('sitio', $sitio);
        $this->set('_serialize', ['sitio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sitio = $this->Sitios->newEntity();
        if ($this->request->is('post')) {
            $sitio = $this->Sitios->patchEntity($sitio, $this->request->data);
            if ($this->Sitios->save($sitio)) {
                $this->Flash->success(__('The sitio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sitio could not be saved. Please, try again.'));
            }
        }
        $users = $this->Sitios->Users->find('list', ['limit' => 200]);
        $barangays = $this->Sitios->Barangays->find('list', ['limit' => 200]);
        $regions = $this->Sitios->Regions->find('list', ['limit' => 200]);
        $provinces = $this->Sitios->Provinces->find('list', ['limit' => 200]);
        $municipalities = $this->Sitios->Municipalities->find('list', ['limit' => 200]);
        $this->set(compact('sitio', 'users', 'barangays', 'regions', 'provinces', 'municipalities'));
        $this->set('_serialize', ['sitio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sitio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sitio = $this->Sitios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sitio = $this->Sitios->patchEntity($sitio, $this->request->data);
            if ($this->Sitios->save($sitio)) {
                $this->Flash->success(__('The sitio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sitio could not be saved. Please, try again.'));
            }
        }
        $users = $this->Sitios->Users->find('list', ['limit' => 200]);
        $barangays = $this->Sitios->Barangays->find('list', ['limit' => 200]);
        $regions = $this->Sitios->Regions->find('list', ['limit' => 200]);
        $provinces = $this->Sitios->Provinces->find('list', ['limit' => 200]);
        $municipalities = $this->Sitios->Municipalities->find('list', ['limit' => 200]);
        $this->set(compact('sitio', 'users', 'barangays', 'regions', 'provinces', 'municipalities'));
        $this->set('_serialize', ['sitio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sitio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sitio = $this->Sitios->get($id);
        if ($this->Sitios->delete($sitio)) {
            $this->Flash->success(__('The sitio has been deleted.'));
        } else {
            $this->Flash->error(__('The sitio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
