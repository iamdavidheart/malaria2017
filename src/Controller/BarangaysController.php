<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Barangays Controller
 *
 * @property \App\Model\Table\BarangaysTable $Barangays
 */
class BarangaysController extends AppController
{


    public function addSitio($id = null){

        $this->loadModel('Sitios');
        $this->loadModel('Barangays');


        $barangay = $this->Barangays->find()->where(['Barangays.id' => $id ])->contain(['Regions','Municipalities','Provinces'])->first();

        $sitio = $this->Sitios->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['created'] = date('Y-m-d h:i:s');
            $this->request->data['user_id'] = $this->Auth->user('id');

            $sitio = $this->Sitios->patchEntity($sitio, $this->request->data);
            if ($this->Sitios->save($sitio)) {
                $this->Flash->success(__('The Sitio has been saved.'));
                return $this->redirect(['controller' => 'sitios', 'action' => 'edit', $sitio->id ]);
            } else {
                $this->Flash->error(__('The Sitio could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('sitio', 'barangay'));
        $this->set('_serialize', ['sitios', 'barangay']);

    }




    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Regions', 'Provinces', 'Municipalities', 'Users']
        ];
        $barangays = $this->paginate($this->Barangays);

        $this->set(compact('barangays'));
        $this->set('_serialize', ['barangays']);
    }

    /**
     * View method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $barangay = $this->Barangays->get($id, [
            'contain' => ['Regions', 'Provinces', 'Municipalities', 'Users', 'ModifiedUsers', 'Facilities', 'MprfCases', 'Sitios']
        ]);

        $this->set('barangay', $barangay);
        $this->set('_serialize', ['barangay']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $barangay = $this->Barangays->newEntity();
        if ($this->request->is('post')) {
            $barangay = $this->Barangays->patchEntity($barangay, $this->request->data);
            if ($this->Barangays->save($barangay)) {
                $this->Flash->success(__('The barangay has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The barangay could not be saved. Please, try again.'));
            }
        }
        $regions = $this->Barangays->Regions->find('list', ['limit' => 200]);
        $provinces = $this->Barangays->Provinces->find('list', ['limit' => 200]);
        $municipalities = $this->Barangays->Municipalities->find('list', ['limit' => 200]);
        $users = $this->Barangays->Users->find('list', ['limit' => 200]);
        $modifiedUsers = $this->Barangays->ModifiedUsers->find('list', ['limit' => 200]);
        $this->set(compact('barangay', 'regions', 'provinces', 'municipalities', 'users', 'modifiedUsers'));
        $this->set('_serialize', ['barangay']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $barangay = $this->Barangays->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $barangay = $this->Barangays->patchEntity($barangay, $this->request->data);
            if ($this->Barangays->save($barangay)) {
                $this->Flash->success(__('The barangay has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The barangay could not be saved. Please, try again.'));
            }
        }
        $regions = $this->Barangays->Regions->find('list', ['limit' => 200]);
        $provinces = $this->Barangays->Provinces->find('list', ['limit' => 200]);
        $municipalities = $this->Barangays->Municipalities->find('list', ['limit' => 200]);
        $users = $this->Barangays->Users->find('list', ['limit' => 200]);

        $this->set(compact('barangay', 'regions', 'provinces', 'municipalities', 'users'));
        $this->set('_serialize', ['barangay']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Barangay id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $barangay = $this->Barangays->get($id);
        if ($this->Barangays->delete($barangay)) {
            $this->Flash->success(__('The barangay has been deleted.'));
        } else {
            $this->Flash->error(__('The barangay could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
