<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FacilityCategories Controller
 *
 * @property \App\Model\Table\FacilityCategoriesTable $FacilityCategories
 */
class FacilityCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $facilityCategories = $this->paginate($this->FacilityCategories);

        $this->set(compact('facilityCategories'));
        $this->set('_serialize', ['facilityCategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Facility Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $facilityCategory = $this->FacilityCategories->get($id, [
            'contain' => ['Facilities']
        ]);

        $this->set('facilityCategory', $facilityCategory);
        $this->set('_serialize', ['facilityCategory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $facilityCategory = $this->FacilityCategories->newEntity();
        if ($this->request->is('post')) {
            $facilityCategory = $this->FacilityCategories->patchEntity($facilityCategory, $this->request->data);
            if ($this->FacilityCategories->save($facilityCategory)) {
                $this->Flash->success(__('The facility category has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The facility category could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('facilityCategory'));
        $this->set('_serialize', ['facilityCategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Facility Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $facilityCategory = $this->FacilityCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $facilityCategory = $this->FacilityCategories->patchEntity($facilityCategory, $this->request->data);
            if ($this->FacilityCategories->save($facilityCategory)) {
                $this->Flash->success(__('The facility category has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The facility category could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('facilityCategory'));
        $this->set('_serialize', ['facilityCategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Facility Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $facilityCategory = $this->FacilityCategories->get($id);
        if ($this->FacilityCategories->delete($facilityCategory)) {
            $this->Flash->success(__('The facility category has been deleted.'));
        } else {
            $this->Flash->error(__('The facility category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
