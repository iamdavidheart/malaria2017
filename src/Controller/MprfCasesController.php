<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MprfCases Controller
 *
 * @property \App\Model\Table\MprfCasesTable $MprfCases
 */
class MprfCasesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $mprfCases = $this->MprfCases->find()->contain(['Users', 'Facilities.FacilityCategories',
         'Facilities.Barangays', 'Facilities.Regions', 'Facilities.Provinces', 'Facilities.Municipalities', 'Facilities.Sitios'])->where(['template_code' => 'ADG']);

        $user_id = $this->Auth->user('id');
        if($this->Auth->user('role_id') == 4){
            $mprfCases->where(['MprfCases.user_id' => $user_id]);
        }

        if($this->Auth->user('role_id') == 3){
            $this->loadModel('Users');
            $user = $this->Users->find()->where(['Users.id' => $user_id])->contain(['Facilities'])->first();
            if(!empty($user->facility)){
                $province_id = $user->facility->province_id;
                $mprfCases->where(['MprfCases.province_id' => $province_id])->orWhere(['MprfCases.user_id' => $user_id]);
            }else{
                $mprfCases = [];
            }
        }


        $this->set(compact('mprfCases'));
        $this->set('_serialize', ['mprfCases']);

    }

    /**
     * View method
     *
     * @param string|null $id Mprf Case id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mprfCase = $this->MprfCases->get($id, [
            'contain' => ['SmsIncomings', 'Users', 'Facilities', 'Regions', 'Provinces', 'Municipalities', 'Barangays', 'Sitios']
        ]);

        $this->set('mprfCase', $mprfCase);
        $this->set('_serialize', ['mprfCase']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // $mprfCase = $this->MprfCases->newEntity();
        // if ($this->request->is('post')) {
        //     $mprfCase = $this->MprfCases->patchEntity($mprfCase, $this->request->data);
        //     if ($this->MprfCases->save($mprfCase)) {
        //         $this->Flash->success(__('The mprf case has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The mprf case could not be saved. Please, try again.'));
        //     }
        // }
        // $smsIncomings = $this->MprfCases->SmsIncomings->find('list', ['limit' => 200]);
        // $users = $this->MprfCases->Users->find('list', ['limit' => 200]);
        // $facilities = $this->MprfCases->Facilities->find('list', ['limit' => 200]);
        // $regions = $this->MprfCases->Regions->find('list', ['limit' => 200]);
        // $provinces = $this->MprfCases->Provinces->find('list', ['limit' => 200]);
        // $municipalities = $this->MprfCases->Municipalities->find('list', ['limit' => 200]);
        // $barangays = $this->MprfCases->Barangays->find('list', ['limit' => 200]);
        // $sitios = $this->MprfCases->Sitios->find('list', ['limit' => 200]);
        // $this->set(compact('mprfCase', 'smsIncomings', 'users', 'facilities', 'regions', 'provinces', 'municipalities', 'barangays', 'sitios'));
        // $this->set('_serialize', ['mprfCase']);




        $mprfCase = $this->MprfCases->newEntity();
        if ($this->request->is('post')) {




            if(isset($this->request->data['province_id'])){  
                $province_id = $this->request->data['province_id'];
             }else{ 
                $province_id = null;
            }
            if(isset($this->request->data['municipality_id'])){  
                $municipality_id = $this->request->data['municipality_id'];
             }else{ 
                $municipality_id = null;
            }
            
            if(isset($this->request->data['barangay_id'])){  
                $barangay_id = $this->request->data['barangay_id'];
             }else{ 
                $barangay_id = null;
            }

            if(isset($this->request->data['sitio_id'])){  
                $sitio_id = $this->request->data['sitio_id'];
             }else{ 
                $sitio_id = null;
            }
            $consult_date = $this->request->data['consult_date'];

            $mprf = $this->MprfCases->find()
                    ->where([
                        'province_id' => $province_id, 
                        'municipality_id' => $municipality_id, 
                        'barangay_id' => $barangay_id
                    ])
                    ->order('MprfCases.id ASC')
                    ->first();

            if(!is_null($mprf)){



                    // $date1=date_create($mprf->consult_date);
                    // $date2=date_create($consult_date);
                    // $diff=date_diff($date1,$date2);
              
                    // if($diff->days > 28){
                    //      echo 'dave'.$this->request->data['status'] = 'new';
                    // }else{
                    //     echo 'ops'.$this->request->data['status'] = 'old';
                    // }

            }else{
                echo 'hec'. $this->request->data['status'] = 'new';
            }



            die;




            $this->request->data['created'] = date('Y-m-d h:i:s'); 
            $this->request->data['source'] = 'WEB'; 
            $this->request->data['user_id'] = $this->Auth->user('id'); 
            $this->request->data['template_code'] = 'ADG'; 


            $mprfCase = $this->MprfCases->patchEntity($mprfCase, $this->request->data);
            if ($this->MprfCases->save($mprfCase)) {
                $this->Flash->success(__('The mprf case has been saved.'));
                return $this->redirect(['action' => 'edit', $mprfCase->id]);
            } else {


                die($mprfCase->errors());
                $this->Flash->error(__('The mprf case could not be saved. Please, try again.'));
            }
        }


        $this->loadModel('Facilities');
        $facilities = $this->Facilities->find();

        $regions = $this->MprfCases->Regions->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ]);


        $this->set(compact('mprfCase', 'regions', 'facilities'));
        $this->set('_serialize', ['mprfCase', 'regions']);
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Mprf Case id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // $mprfCase = $this->MprfCases->get($id, [
        //     'contain' => []
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     $mprfCase = $this->MprfCases->patchEntity($mprfCase, $this->request->data);
        //     if ($this->MprfCases->save($mprfCase)) {
        //         $this->Flash->success(__('The mprf case has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The mprf case could not be saved. Please, try again.'));
        //     }
        // }
        // $smsIncomings = $this->MprfCases->SmsIncomings->find('list', ['limit' => 200]);
        // $users = $this->MprfCases->Users->find('list', ['limit' => 200]);
        // $facilities = $this->MprfCases->Facilities->find('list', ['limit' => 200]);
        // $regions = $this->MprfCases->Regions->find('list', ['limit' => 200]);
        // $provinces = $this->MprfCases->Provinces->find('list', ['limit' => 200]);
        // $municipalities = $this->MprfCases->Municipalities->find('list', ['limit' => 200]);
        // $barangays = $this->MprfCases->Barangays->find('list', ['limit' => 200]);
        // $sitios = $this->MprfCases->Sitios->find('list', ['limit' => 200]);
        // $this->set(compact('mprfCase', 'smsIncomings', 'users', 'facilities', 'regions', 'provinces', 'municipalities', 'barangays', 'sitios'));
        // $this->set('_serialize', ['mprfCase']);

        $mprfCase = $this->MprfCases->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mprfCase = $this->MprfCases->patchEntity($mprfCase, $this->request->data);
            if ($this->MprfCases->save($mprfCase)) {
                $this->Flash->success(__('The mprf case has been saved.'));
                return $this->redirect(['action' => 'edit',$mprfCase->id ]);
            } else {
                $this->Flash->error(__('The mprf case could not be saved. Please, try again.'));
            }
        }
 
        // $this->loadModel('Facilities');
        // $facilities = $this->Facilities->find();




        $facilities = $this->MprfCases->Facilities->find('list');


        $regions = $this->MprfCases->Regions->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ]);

        $provinces = $this->MprfCases->Regions->Provinces->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Provinces.region_id' => $mprfCase->region_id]);

        $municipalities = $this->MprfCases->Regions->Provinces->Municipalities->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Municipalities.province_id' => $mprfCase->province_id]);

        $barangays = $this->MprfCases->Regions->Provinces->Municipalities->Barangays->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Barangays.municipality_id' => $mprfCase->municipality_id]);

        $sitios = $this->MprfCases->Regions->Provinces->Municipalities->Barangays->Sitios->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_details'
        ])->where(['Sitios.barangay_id' => $mprfCase->barangay_id]);


        $this->set(compact('mprfCase', 'facilities', 'regions', 'provinces', 'municipalities', 'barangays', 'sitios'));
        $this->set('_serialize', ['mprfCase']);
        
    }

    /**
     * Delete method
     *
     * @param string|null $id Mprf Case id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mprfCase = $this->MprfCases->get($id);
        if ($this->MprfCases->delete($mprfCase)) {
            $this->Flash->success(__('The mprf case has been deleted.'));
        } else {
            $this->Flash->error(__('The mprf case could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
