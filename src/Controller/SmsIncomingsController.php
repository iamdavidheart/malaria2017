<?php
namespace App\Controller;


use Cake\Event\Event;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

/**
 * SmsIncomings Controller
 *
 * @property \App\Model\Table\SmsIncomingsTable $SmsIncomings
 */
class SmsIncomingsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MalariaInitiative');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['handle', 'process', 'test', 'test1', 'dmw', 'adg', 'dth', 'envaya', 'outgoing', 'samp', 'alerts', 'sampx', 'app', 'shareLoad', 'sampxx', 'ost', 'inner', 'update']);
    }


    public function inner(){
        // $this->loadModel('MprfCases');

        // $user = $this->MprfCases->find();
        // foreach ($user as $key => $value) {
        //    $value->first_name = ucfirst($value->first_name);
        //     $value->last_name = ucfirst($value->last_name);
        //      $this->MprfCases->save($value);
        // }
    }

    public function _insertLogs($logs = null, $phone = null)
    {
        $this->loadModel('Logs');
        $log = $this->Logs->newEntity();
        $log->logs          = $logs;
        $log->phone_number  = $phone;
        $this->Logs->save($log);

    }


    public function handle()
    {
        $this->autoRender = false;
        $this->response->type('application/json');
        $this->RequestHandler->renderAs($this, 'json');

        $result = 'FAILED';
        $response = [
            'event' => 'log',
            'message' => 'Malaria-Sms Status Connected...'
        ];

        if ($this->request->is('post')) {

            //*******************************************************************
            if($this->request->data['action'] == 'incoming') {


                $s_no     = $this->request->data['from'];

                    //LOAD
                    // if(substr($s_no, 0, 4) == '2652'){
                    //     $this->loadModel('SmsOutgoings');
                    //     $smsOutgoing = $this->SmsOutgoings->newEntity();
                    //     $smsOutgoing->created       = date('Y-m-d H:i:s');
                    //     $smsOutgoing->receiver_no   = $s_no;
                    //     $smsOutgoing->source        = 'SMS';
                    //     $smsOutgoing->box           = 10;
                    //     $smsOutgoing->status        = 'pending';
                    //     $smsOutgoing->message       = 'YES';
                    //     $smsOutgoing->network       = 'globe';
                    //     $smsOutgoing->type          = 'shareload';
                    //     $this->SmsOutgoings->save($smsOutgoing);
                    // }else{}

                    $p_no     = $this->request->data['phone_number'];
                    $m_type   = $this->request->data['message_type'];
                    $msg      = $this->request->data['message'];
                    if($m_type == 'sms'){
                        $smsI = $this->SmsIncomings->newEntity();
                        $smsI->created       = date('Y-m-d H:i:s');
                        $smsI->sender_no     = $s_no;
                        $smsI->phone_no      = $p_no;
                        $smsI->message       = $msg;
                        $smsI->message_type  = $m_type;
                        $smsI->status        = 'pending';
                        $smsI->box           = 10;
                        if ($this->SmsIncomings->save($smsI)) {
                            $result = 'OK';
                            $this->_processRequest();
                        }
                    }
                //$checkValidNo = $this->mobileValidation($_no);

                //Checking if number is valid for reply
               // if($checkValidNo == 'OK')
                //{
                //}

            }
            //*******************************************************************

            //*******************************************************************
            if($this->request->data['action'] == 'outgoing') {


                $this->loadModel('SmsOutgoings');
                $this->loadModel('Networks');
                $phone_number = str_replace('+639', '09', $this->request->data['phone_number']);
                $smsOutgoings = $this->SmsOutgoings->find();
                $result = 'OK';
                $outgoingLoad = 'emptyLoad';

                if ($phone_number == '09175743321' OR $phone_number == '09274631496') {

                    $response = [
                        'event' => 'log',
                        'message' => 'GLOBE'
                    ];

                    $networks = $this->Networks
                            ->find()
                            ->select(['network', 'prefix'])
                            ->where(['network' => 'Globe']);     

                    foreach ($networks as $key => $network) {
                       $prefix = str_replace('09', '+639', $network->prefix);
                       $smsOutgoings->orWhere(['SmsOutgoings.receiver_no LIKE' => $prefix.'%' ]);
                    }
                    
                    $smsOutgoings->where(['SmsOutgoings.status' => 'pending','SmsOutgoings.box' => 10]);
                    $outgoingLoad = $this->SmsOutgoings->find()
                        ->where([
                            'SmsOutgoings.status' => 'pending', 
                            'SmsOutgoings.network' => 'globe', 
                            'SmsOutgoings.type' => 'shareload',
                            'sender_no' => '+639175743321'
                        ]);

                } elseif ($phone_number == '09231951352') {
                    $response = [
                        'event' => 'log',
                        'message' => 'SUN'
                    ];

                    $networks = $this->Networks
                            ->find()
                            ->select(['network', 'prefix'])
                            ->where(['network' => 'Sun']);     

                    foreach ($networks as $key => $network) {
                       $prefix = str_replace('09', '+639', $network->prefix);
                       $smsOutgoings->orWhere(['SmsOutgoings.receiver_no LIKE' => $prefix.'%' ]);
                    }

                    $smsOutgoings->where(['SmsOutgoings.status' => 'pending','SmsOutgoings.box' => 10]);

                } elseif ($phone_number == '09192035657') {
                    $response = [
                        'event' => 'log',
                        'message' => 'SMART'
                    ];
                    $networks = $this->Networks
                            ->find()
                            ->select(['network', 'prefix'])
                            ->where(['network' => 'Smart']);     

                    foreach ($networks as $key => $network) {
                       $prefix = str_replace('09', '+639', $network->prefix);
                       $smsOutgoings->orWhere(['SmsOutgoings.receiver_no LIKE' => $prefix.'%' ]);
                    }
                    $smsOutgoings->where(['SmsOutgoings.status' => 'pending','SmsOutgoings.box' => 10]);
                }else{
                    $result = 'ERROR';
                    $response = [
                        'event' => 'log',
                        'message' => 'UNKNOWN NUMBER'
                    ];
                }
                if($result == 'OK'){

                    $messages = [];
                    // if($outgoingLoad != 'emptyLoad'){
                    //     if($outgoingLoad->count()) {
                    //         foreach ($outgoingLoad as $key => $load) {
                    //             $messages[] = [
                    //                 "id"        => $load->id,
                    //                 "to"        => $load->receiver_no,
                    //                 "message"   => $load->message
                    //             ];
                    //             $load->status = 'sending';
                    //             $load->sender_no = $this->request->data['phone_number'];

                    //             if($this->SmsOutgoings->save($load)){ }
                    //             if (10 == count($messages)) {
                    //                 break;
                    //             }
                    //         }
                    //        //response = ['events' => [['event' => 'send', 'messages' => $messages]]];
                    //     }
                    // }
                    if($smsOutgoings->count()) {
                        foreach ($smsOutgoings as $key => $smsOutgoing) {
                            $messages[] = [
                                "id"        => $smsOutgoing->id,
                                "to"        => $smsOutgoing->receiver_no,
                                "message"   => $smsOutgoing->message
                            ];
                            $smsOutgoing->status = 'sending';
                            $smsOutgoing->sender_no = $this->request->data['phone_number'];

                            if($this->SmsOutgoings->save($smsOutgoing)){ }
                            if (10 == count($messages)) {
                                break;
                            }
                        }
                    }
                    $response = ['event' => 'send', 'messages' => $messages];
                }
            }
            //*******************************************************************

            if($this->request->data['action'] == 'send_status') {
                $response = [
                    'event' => 'log',
                    'message' => 'SMS status: '. $this->request->data['status']
                ];
                $id         = $this->request->data['id'];
                $status     = $this->request->data['status'];
                $error      = $this->request->data['error'];
                $this->loadModel('SmsOutgoings');
                $smsOutgoings = $this->SmsOutgoings->find()->where(['SmsOutgoings.id' => $id])->first();

                if(!is_null($smsOutgoings)){
                    $smsOutgoings->status = $status;
                    $smsOutgoings->error  =  $error;
                    $this->SmsOutgoings->save($smsOutgoings);  
                }
            }
            //*******************************************************************
            if($this->request->data['action'] == 'device_status') {
                $response = [
                    'event' => 'log',
                    'message' => 'Status'
                ];
            }
            //*******************************************************************
        }

      echo json_encode(['result' => $result, 'response' => $response]);
    }

    public function update()
    {
        $this->autoRender = false;
        if ($this->request->is('post'))
        {
            $result = 'FAILED';
            $this->loadModel('SmsOutgoings');
            $id = $this->request->data['id'];
            $status = $this->request->data['status'];
            $smsOutgoing = $this->SmsOutgoings->find()->where(['SmsOutgoings.id' =>  $id])->first();
            $smsOutgoing->status = $status;
            if($this->SmsOutgoings->save($smsOutgoing))
            {
                $result = 'OK';
            }
            echo json_encode(['result' => $result]);
        }
    }

    public function _processRequest(){

        $this->autoRender = false;
        $smsIs = $this->SmsIncomings
                ->find()
                ->where(['SmsIncomings.status' =>  'pending']);

        $log    = '';
        $code   = ['REG', 'DTH', 'DMW', 'OST', 'ADG'];
        $status = 'spam';
        $temp_code = '';
        $error_log = '';
        $messages = [];
        $user_id = null;

        foreach ($smsIs as $key => $smsI) {

            $msg             = array_map('trim', explode(',', $smsI->message));
            $sms_code        = strtoupper($msg[0]);
            $smsI_id         = $smsI->id;
            $sender_no       = $smsI->sender_no;
            $phone_no        = $smsI->phone_no;

            if(in_array($sms_code, $code)){
                $temp_code = $sms_code;

                if($sms_code == 'REG') {

                    $rsp = $this->reg($smsI_id, $msg);
                    $user_id = $rsp['user_id'];
                    if($rsp['result'] == 'OK'){

                        $options = ['user_code' => $rsp['userCode'], 'full_name' => $rsp['full_name'] ];
                        
                        $this->_outgoing($smsI_id, $sms_code, $options, $user_id);
                        $status = 'processed';
                    }else{
                        $options = [
                            'sender_no'     => $sender_no, 
                            'error_message' => $rsp['message'],
                            'user_id'       => $user_id
                        ];
                        $error_log = $rsp['message'];
                        $this->_outgoingErrorMessage($smsI_id, $sms_code, $options);
                    }
                }

                //*******************************************************************
                if ($sms_code == 'ADG'){

                    $rsp = $this->adg($smsI_id, $msg);
                    $user_id = $rsp['user_id'];
                    if($rsp['result'] == 'OK'){
                        $options = ['full_name' => $rsp['full_name'] , 'receiver_no' => $sender_no, 'msg' =>  ''  ];
                        
                        $this->_outgoing($smsI_id, $sms_code, $options, $user_id);
                        $this->_alerts($sms_code, $options);
                        $status = 'processed';
                    }else{
                        $options = [
                            'sender_no'     => $sender_no, 
                            'error_message' => $rsp['message'],
                            'user_id'       => $user_id
                        ];
                        $error_log = $rsp['message'];
                        $this->_outgoingErrorMessage($smsI_id, $sms_code, $options);
                    }
                }

                //*******************************************************************
                if ($sms_code == 'DTH'){
                    $rsp = $this->dth($smsI_id, $msg);
                     $user_id = $rsp['user_id'];
                    if($rsp['result'] == 'OK'){
                        $options = ['full_name' => $rsp['full_name'] , 'receiver_no' => $sender_no, 'msg' => ''  ];
                        $this->_outgoing($smsI_id, $sms_code, $options, $user_id);
                        $this->_alerts($sms_code, $options);
                        $status = 'processed';
                    }else{
                        $options = [
                            'sender_no'     => $sender_no, 
                            'error_message' => $rsp['message'],
                            'user_id'       => $user_id
                        ];
                        $error_log = $rsp['message'];
                        $this->_outgoingErrorMessage($smsI_id, $sms_code, $options);
                    }
                }

                //*******************************************************************
                if ($sms_code == 'DMW') {
                   $rsp = $this->dmw($smsI_id, $msg);
                   $user_id = $rsp['user_id'];
                    if($rsp['result'] == 'OK'){
                        $options = ['full_name' => $rsp['full_name'] , 'receiver_no' => $sender_no, 'msg' => $rsp['itemCodes']  ];
                        
                        $this->_outgoing($smsI_id, $sms_code, $options, $user_id);
                        $this->_alerts($sms_code, $options);
                        $status = 'processed';
                    }else{
                        $options = [
                            'sender_no'     => $sender_no, 
                            'error_message' => $rsp['message'],
                            'user_id'       => $user_id
                        ];
                        $error_log = $rsp['message'];
                        $this->_outgoingErrorMessage($smsI_id, $sms_code, $options);
                    }
                }
                //*******************************************************************
                if ($sms_code == 'OST') {

                    $rsp = $this->ost($smsI_id, $msg);
                    $user_id = $rsp['user_id'];
                    if($rsp['result'] == 'OK'){
                        
                        $options = ['full_name' => $rsp['full_name'] , 'receiver_no' => $sender_no, 'msg' => $rsp['itemCodes'] ];
                        $this->_outgoing($smsI_id, $sms_code, $options, $user_id);
                        $this->_alerts($sms_code, $options);
                        $status = 'processed';

                    }else{
                        $options = [
                            'sender_no'     => $sender_no, 
                            'error_message' => $rsp['message'],
                            'user_id'       => $user_id
                        ];
                        $error_log = $rsp['message'];
                        $this->_outgoingErrorMessage($smsI_id, $sms_code, $options);
                    }

                }
            }else{

                $options = [
                    'sender_no'     => $sender_no, 
                    'error_message' => 'Invalid Template Code.'
                ];
                $error_log = 'Invalid Template Code.';
                $this->_outgoingSpamMessage($smsI_id, $options);
            }
            $sms = $this->SmsIncomings->get($smsI_id);
            $sms->status = $status;
            $sms->user_id = $user_id;
            $sms->error_message = $error_log;
            $sms->template_code = $temp_code;
            $this->SmsIncomings->save($sms);

            if (10 == count($messages)) {
                break;
            }
        }
        return array('log' =>  $log);
    }

    public function checkUserCode($userCode = null) {
        $this->autoRender = false;
        $this->loadModel('Users');
        return  $this->Users->find()->where(['code' => $userCode])->first();
    }

    public function _findTemplateCode($code) {
        $this->autoRender = false;
        $this->loadModel('Templates');
        return  $this->Templates
                ->find()
                ->contain(['TemplateAlerts'])
                ->where(['code' => strtoupper(trim($code))])->first();
    }

    public function checkValidMobile($mobile = null) {
        $result = 'OK';
        if(strlen($mobile) < 11 || strlen($mobile) > 12) {
            $result = 'ERROR';
        }
        if(strlen($mobile) == 11 && substr($mobile, 0, 2) != '09') {
            $result = 'ERROR';
        }

        if(strlen($mobile) == 12 && substr($mobile, 0, 3) != '639') {
            $result = 'ERROR';
        }
        return $result;
    }


    public function mobileValidation($mobile = null) {
        $result = 'OK';

        if(strlen($mobile) < 11 || strlen($mobile) > 13) {
            $result = 'ERROR';
        }

        if(strlen($mobile) == 13) {
            if(substr($mobile, 0, 4) != '+639'){
                $result = 'ERROR';
            }
        }
        if(strlen($mobile) == 12) {
            if(substr($mobile, 0, 3) != '639'){
                $result = 'ERROR';
            }

            if(substr($mobile, 0, 2) != '09'){
                $result = 'ERROR';
            }

        }
        return $result;
    }

    public function _shareload( $tempCode = null, $sender_no = null, $phone_no = null) {

        if(substr($sender_no, 0, 2) == '09'){
            $sender_no = substr_replace($sender_no, '+639', 0, 2);
        }elseif(substr($sender_no, 0, 3) == '639'){
            $sender_no = substr_replace($sender_no, '+639', 0, 3);
        }else{
            $sender_no = $sender_no;
        }

        if(substr($phone_no, 0, 2) == '09'){
            $phone_no = substr_replace($phone_no, '+639', 0, 2);
        }elseif(substr($phone_no, 0, 3) == '639'){
            $phone_no = substr_replace($phone_no, '+639', 0, 3);
        }else{
            $phone_no = $phone_no;
        }

        if($this->mobileValidation($sender_no) == 'OK' ) {
            $this->loadModel('Templates');
            $template = $this->Templates->find()->where(['Templates.code' => $tempCode])->first();

            if(!is_null($template)){
                $this->loadModel('SmsOutgoings');
                //if status is equal to 1 - share a load ONN else 0 - OFF
                switch ($phone_no) {
                    case '+639175743321':
                        if($template->globe_status == 1){
                 
                            if($template->globe_amount != 0 AND $template->globe_amount <= 5){
                                $smsOutgoing = $this->SmsOutgoings->newEntity();
        
                                $globeNumber  =  str_replace('+63', '2', $sender_no);
                                $smsOutgoing->created       = date('Y-m-d H:i:s');
                                $smsOutgoing->receiver_no   = $globeNumber;
                                $smsOutgoing->sender_no     = $phone_no;
                                $smsOutgoing->source        = 'SMS';
                                $smsOutgoing->box           = 10;
                                $smsOutgoing->status        = 'pending';
                                $smsOutgoing->network       = 'globe';
                                $smsOutgoing->type          = 'shareload';
                                $smsOutgoing->message       = $template->globe_amount;
                                if ($this->SmsOutgoings->save($smsOutgoing)) {
                                    echo 'ok';
                                }else{
                                    echo 'error';
                                }

                            }
                        }
                        break;
                    case 'smart':
                        if($template->smart_status == 1){
                            if($template->smart_amount != 0 AND $template->smart_amount <= 5){

                            }
                        }
                        break;
                    case 'sun':
                        if($template->sun_status == 1){
                            if($template->sun_amount != 0 AND $template->sun_amount <= 5){

                            }
                        }
                        break;
                    case 'tm':
                        if($template->tm_status == 1){
                            if($template->tm_amount != 0 AND $template->tm_amount <= 5){

                                $smsOutgoing = $this->SmsOutgoings->newEntity();
                                $sms_code = strtotime(date('Y-m-d h:i:s')).'-'.rand(0000, 9999);
                                $globeNumber  =  str_replace('+63', '2', $number);
                                $smsOutgoing->created       = date('Y-m-d H:i:s');
                                $smsOutgoing->receiver_no   = $globeNumber;
                                $smsOutgoing->sms_code      = $sms_code;
                                $smsOutgoing->source        = 'SMS';
                                $smsOutgoing->box           = 10;
                                $smsOutgoing->status        = 'pending';
                                $smsOutgoing->network       = 'globe';
                                $smsOutgoing->type          = 'shareload';
                                $smsOutgoing->message       = $template->globe_amount;
                                if ($this->SmsOutgoings->save($smsOutgoing)) {}

                            }
                        }
                        break;  
                    default:
                        break;
                }
            }
        }
    }

    public function shareLoadProcess($number) {
    }

    public function _outgoing($smsIncoming_id = null, $tempCode = null, $options = null, $user_id = null){

        $this->autoRender = false;
        $this->loadModel('Templates');
        $tmp = $this->Templates->find()
                ->where(['Templates.code' => $tempCode])
                ->first();
        if(!is_null($tmp)){

            $this->loadModel('SmsOutgoings');
            if($tmp->send_alert_msg == 1){

                if($tempCode == 'REG'){
                    $user_code = $options['user_code'];
                    $full_name = $options['full_name'];
                    $format  =  str_replace('[ConID]', $user_code, $tmp->format);
                    $this->loadModel('Users');
                    $userFindContacts = $this->Users->find()
                            ->where(['Users.code' => $user_code])
                            ->contain(['UserContacts'])
                            ->first();

                    if(!is_null($userFindContacts)){
                        if(!empty($userFindContacts->user_contacts)){
                            foreach ($userFindContacts->user_contacts as $key => $contacts) {
                                if(!empty($contacts->value) OR $contacts->value != ''){
                                    $receiver_no = $contacts->value;
                                    if(substr($receiver_no, 0, 2) == '09'){
                                        $number = substr_replace($receiver_no, '+639', 0, 2);
                                    }elseif(substr($receiver_no, 0, 3) == '639'){
                                        $number = substr_replace($receiver_no, '+639', 0, 3);
                                    }else{
                                        $number = $receiver_no;
                                    }
                                    if($this->mobileValidation($number) == 'OK' ) {
                                        $smsOutgoing = $this->SmsOutgoings->newEntity();
                                        $smsOutgoing->created       = date('Y-m-d H:i:s');
                                        $smsOutgoing->receiver_no   = $number;
                                        $smsOutgoing->sms_incoming_id  = $smsIncoming_id;
                                        $smsOutgoing->source    = 'SMS';
                                        $smsOutgoing->box       = 10;
                                        $smsOutgoing->user_id   = $user_id;
                                        // $smsOutgoing->user_id   = $userFindContacts->id;
                                        $smsOutgoing->status    = 'pending';
                                        $smsOutgoing->message   = $full_name .' ! '.$format;
                                        $smsOutgoing->type      = 'sms reply';
                                        $this->SmsOutgoings->save($smsOutgoing);
                                    }
                                }
                            }
                        }
                    }
                }

                if($tempCode != 'REG'){

                    $receiver_no = $options['receiver_no'];
                    $full_name   = $options['full_name'];
                    $smsOutgoing = $this->SmsOutgoings->newEntity();
                    $smsOutgoing->created       = date('Y-m-d H:i:s');
                    $smsOutgoing->receiver_no   = $receiver_no;
                    $smsOutgoing->sms_incoming_id  = $smsIncoming_id;
                    $smsOutgoing->source    = 'SMS';
                    $smsOutgoing->box       = 10;
                    $smsOutgoing->user_id   = $user_id;
                    $smsOutgoing->status    = 'pending';
                    $smsOutgoing->type      = 'sms reply';
                    $smsOutgoing->message   = $full_name.$tmp->format;
                    if ($this->SmsOutgoings->save($smsOutgoing)) {}
                }
            }
        }
    }

    public function _alerts( $tempCode = null, $options = null, $user_id = null){

        $this->autoRender = false;
        $this->loadModel('SmsOutgoings');
        $this->loadModel('UserContacts');
        $this->loadModel('TemplateNotifications');
        $this->loadModel('Templates');
        $template = $this->Templates
                ->find()
                ->where(['Templates.code' => $tempCode])->first();

        if(!is_null($template)){

            $notify = $this->TemplateNotifications->find()->where(['TemplateNotifications.template_id' => $template->template_alert_id]);


            foreach ($notify as $key => $noty) {

                $receiver = $this->UserContacts
                        ->find()
                        ->where(['UserContacts.user_id' => $noty->user_id])
                        ->contain(['Users']);

                foreach ($receiver as $key => $user_contact) {

                    if(!empty($user_contact->value) OR $user_contact->value != ''){

                        $receiver_no = $user_contact->value;
                        if(substr($receiver_no, 0, 2) == '09'){
                            $number = substr_replace($receiver_no, '+639', 0, 2);
                        }elseif(substr($receiver_no, 0, 3) == '639'){
                            $number = substr_replace($receiver_no, '+639', 0, 3);
                        }else{
                            $number = $receiver_no;
                        }

                        if($this->mobileValidation($number) == 'OK' ) {
                            $name       = $options['full_name'];
                            $messages   = $options['msg'];

                            $smsOutgoing = $this->SmsOutgoings->newEntity();
                            $smsOutgoing->created       = date('Y-m-d H:i:s');
                            $smsOutgoing->receiver_no   = $number;
                            $smsOutgoing->receiver_id   = $noty->user_id;
                            $smsOutgoing->box           = 10;
                            $smsOutgoing->user_id       = $noty->user_id;
                            $smsOutgoing->source        = 'SMS';
                            $smsOutgoing->status        = 'pending';
                            $smsOutgoing->type          = 'sms alerts';
                            $smsOutgoing->message       = 'Noty ['.$tempCode.'] - message: ('. $messages.') by '.$name;
                            if ($this->SmsOutgoings->save($smsOutgoing)) {

                            }
                        }
                    }
                }
            }
        }
    }


    public function _outgoingSpamMessage($smsIncoming_id = null,   $options = null){

        $this->autoRender = false;
        $sender_no = $options['sender_no'];
        $error_message = $options['error_message'];

        $this->loadModel('SmsOutgoings');
        $smsOutgoing = $this->SmsOutgoings->newEntity();
        $smsOutgoing->created       = date('Y-m-d H:i:s');
        $smsOutgoing->receiver_no   = $sender_no;
        $smsOutgoing->sms_incoming_id  = $smsIncoming_id;
        $smsOutgoing->box       = 10;
        $smsOutgoing->status    = 'pending';
        $smsOutgoing->message   = $error_message;
        $smsOutgoing->type      = 'sms reply';
        if ($this->SmsOutgoings->save($smsOutgoing)) {
        }
    
    }


    public function _outgoingErrorMessage($smsIncoming_id = null, $tempCode = null,  $options = null){

        $this->autoRender = false;
        $sender_no = $options['sender_no'];
        $user_id = $options['user_id'];
        $error_message = $options['error_message'];
        $this->loadModel('Templates');
        $template = $this->Templates->find()
                ->where(['Templates.code' => $tempCode])
                ->first();

        if(!is_null($template)){
            if($template->send_error_msg == 1){
                $this->loadModel('SmsOutgoings');
                $smsOutgoing = $this->SmsOutgoings->newEntity();
                $smsOutgoing->created       = date('Y-m-d H:i:s');
                $smsOutgoing->receiver_no   = $sender_no;
                $smsOutgoing->sms_incoming_id  = $smsIncoming_id;
                $smsOutgoing->user_id   = $user_id;
                $smsOutgoing->box       = 10;
                $smsOutgoing->status    = 'pending';
                $smsOutgoing->message   = $error_message;
                $smsOutgoing->type      = 'sms reply';
                if ($this->SmsOutgoings->save($smsOutgoing)) {

                }
            }
        }
    }
    public function reg($smsIncoming_id = null, $messages = null) {
        //$this->autoRender = false;
        // $text = 'reg,surname,given,222,4,10,34,09274631496';
        // $messages        = array_map('trim', explode(',', $text));
        $result = 'OK';
        $errorMessage = ''; 
        $code       = ''; 
        $full_name = '';
        $user_id = null;

        $this->loadModel('TemplateSettings');
        $templeMessage = $this->TemplateSettings->find()->where(['template_id' => 6])->first();

        if(!isset($messages[1]) && empty($messages[1]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->last_name;
        }
        if(!isset($messages[2]) && empty($messages[2]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->first_name;
        }

        if(!isset($messages[3]) && empty($messages[3]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->facilty_code;
        }

        if(!isset($messages[4]) && empty($messages[4]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->user_role;
        }

        if(!isset($messages[5]) && empty($messages[5]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->designation;
        }

        if(!isset($messages[6]) && empty($messages[6]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->mobile_one;
        }


        if($result == 'OK') {
            $this->loadModel('Users');

            $surname    = strtolower($messages[1]);
            $givenName  = strtolower($messages[2]);
            $faciltyCode    = $messages[3];
            $userRole       = $messages[4];
            $designation    = $messages[5];
            $mobileno1      = $messages[6];

            if($this->mobileValidation($mobileno1) == 'ERROR' ) {
                $result = 'ERROR';
                $errorMessage .=  $templeMessage->mobile_one_invalid;
            }

            $this->loadModel('Facilities');
            $facility = $this->Facilities->find()->where(['Facilities.id' => $faciltyCode])->first();
            if(is_null($facility)){
                $result = 'ERROR';
                $errorMessage .=  $templeMessage->facilty_code_invalid;
            }

            if(!in_array($userRole, [3,4])){
                $result = 'ERROR';
                $errorMessage .=  $templeMessage->user_role_invalid;
            }

            $this->loadModel('Designations');
            $designations = $this->Designations->find()->where(['Designations.id' => $designation])->first();
            if(is_null($designations)){
                $result = 'ERROR';
                $errorMessage .=  $templeMessage->designation_invalid;
            }

            $mobileno2 = '';
            if(isset($messages[7])) {
                $mobileno2  = $messages[7];
                if($this->mobileValidation($mobileno2) == 'ERROR' ) {
                    $result = 'ERROR';
                    $errorMessage .=  $templeMessage->mobile_two_invalid;
                }
            }


            $mobileno3 = '';
            if(isset($messages[8])) {
                $mobileno3  = $messages[8];
                if($this->mobileValidation($mobileno3) == 'ERROR' ) {
                    $result = 'ERROR';
                    $errorMessage .=  $templeMessage->mobile_three_invalid;
                }
            }

            $userDuplicate = $this->Users->find()->where(['first_name' => $givenName, 'last_name' => $surname])->first();
            if(!is_null($userDuplicate)){
                $result = 'ERROR';
                $errorMessage .=  $templeMessage->duplicate_name;
            }

            if($result == 'OK') {

                $continue = true;
                do {
                    $code = $this->MalariaInitiative->generate_code(3,2);
                    if( !$this->Users->find()->where(['code' => $code])->count() ){
                        $continue = false;
                    }
                } while ($continue);

                $user = $this->Users->newEntity();
         
                $user->code        = $code;
                $user->status      = 'off';
                $user->blocked     = 'off';
                $user->source      = 'SMS';

                $user->mobile_no_one     = $mobileno1;
                $user->mobile_no_two     = $mobileno2;
                $user->mobile_no_three   = $mobileno3;
                $user->password    = 'password';
                $user->created     = date('Y-m-d h:i:s');
                $user->role_id     = $userRole;
                $user->facility_id = $faciltyCode;
                $user->designation_id = $designation;
                $user->last_name   = ucfirst($surname);
                $user->first_name  = ucfirst($givenName);
                $user->province_id = $facility->province_id;

                if($this->Users->save($user)) {

                    $this->loadModel('UserContacts');
                     $userContacts = $this->UserContacts->newEntity();
                     $userContacts->user_id = $user->id;
                     $userContacts->type    = 'mobile';
                     $userContacts->created  = date('Y-m-d h:i:s');
                     $userContacts->value    = $mobileno1;
                     $userContacts->prime_setup    = 1;
                     $this->UserContacts->save($userContacts);

                     $userContacts = $this->UserContacts->newEntity();
                     $userContacts->user_id = $user->id;
                     $userContacts->type    = 'mobile';
                     $userContacts->created  = date('Y-m-d h:i:s');
                     $userContacts->value    = $mobileno2;
                     $this->UserContacts->save($userContacts);

                     $userContacts = $this->UserContacts->newEntity();
                     $userContacts->user_id = $user->id;
                     $userContacts->type    = 'mobile';
                     $userContacts->created  = date('Y-m-d h:i:s');
                     $userContacts->value    = $mobileno3;
                     $this->UserContacts->save($userContacts);

                     $user_id = $user->id;

                    $full_name = $user->first_name.' '.$user->last_name;
                    $errorMessage       = 'Valid Registration!';
                }
            }
        }
        return array('result' => $result, 'message' => $errorMessage, 'userCode' => $code, 'full_name' => $full_name, 'user_id' => $user_id);
    }
    public function adg($smsIncoming_id = null, $messages = null) {
        // $text = 'adg,npn54,01/12/16,saano,paano,34,m,u,pv, testing mode';
        // $messages        = array_map('trim', explode(',', $text));
        // $this->autoRender = false;
        $result = 'OK';
        $errorMessage = ''; 
        $full_name = '';
        $userCode = '';
        $user_id = null;


        $this->loadModel('TemplateSettings');
        $templeMessage = $this->TemplateSettings->find()->where(['template_id' => 2])->first();

        if(!isset($messages[1]) && empty($messages[1]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->user_code;
        }

        if(!isset($messages[2]) && empty($messages[2]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->report_date;
        }

        if(!isset($messages[3]) && empty($messages[3]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->last_name;
        }

        if(!isset($messages[4]) && empty($messages[4]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->first_name;
        }

        if(!isset($messages[5]) && empty($messages[5]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->age;
        }

        if(!isset($messages[6]) && empty($messages[6]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->gender;
        }

        if(!isset($messages[7]) && empty($messages[7]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->mprf_case;
        }

        if(!isset($messages[8]) && empty($messages[8]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->exam_result;
        }


        if($result == 'OK') {

            $consultDate    = $messages[2];
            $last_name      = strtolower($messages[3]);
            $first_name     = strtolower($messages[4]);
            $age            = $messages[5];
            $remarks        = (isset($messages[9]))? $messages[9] : '' ;
            $code           = strtoupper($messages[0]);
            $userCode       = strtoupper($messages[1]);



            $this->MprfCases = TableRegistry::get('MprfCases');
            $this->loadModel('Users');
            $user = $this->Users
                    ->find()
                    ->where(['Users.code' => $userCode])
                    ->contain([
                        'Facilities',
                        'Facilities.Provinces', 
                        'Facilities.Regions',
                        'Facilities.Municipalities',
                        'Facilities.Barangays',
                        'Facilities.Sitios'
                    ])->first();


            if(is_null($user)){
                $result = 'ERROR';
                $errorMessage .= $templeMessage->user_code_invalid;
            }else{
                $user_id = $user->id;

                if($user->activated != 1){
                    $result = 'ERROR';
                    $errorMessage .= $templeMessage->user_activation;
                }else{

                    $full_name = $user->first_name.' '.$user->last_name;
                    if(empty($user->facility)){
                        $result = 'ERROR';
                        $errorMessage .= $templeMessage->facilty_code;
                    }
                        
                    $reportDateArray = explode('/', $consultDate);
                    if(count($reportDateArray) == 3){
                        if(isset($reportDateArray[0]) && isset($reportDateArray[1]) && isset($reportDateArray[2]) ){
                            $date_err = 'ok';
                            if($reportDateArray[0] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[1] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[2] == ''){
                                $date_err = 'err';
                            }
                            if($date_err == 'ok'){
                                if (!checkdate($reportDateArray[0], $reportDateArray[1], $reportDateArray[2])) {
                                    $result = 'ERROR';
                                    $errorMessage .=  $templeMessage->error_report_date;
                                }

                                $cDate      = strtotime(date('Y-m-d', strtotime($consultDate)));
                                $date_today = strtotime(date('Y-m-d'));
                                if($cDate > $date_today){
                                    $result = 'ERROR';
                                    $errorMessage .= $templeMessage->invalid_report_date;
                                }
                            }else{
                                $result = 'ERROR';
                                $errorMessage = $templeMessage->error_report_date;
                            }
                        }else{
                            $result = 'ERROR';
                            $errorMessage = $templeMessage->error_report_date;
                        }
                    }else{
                        $result = 'ERROR';
                        $errorMessage = $templeMessage->error_report_date;
                    }


                }

            }

            $gender = trim(strtoupper($messages[6]));
            if(!in_array($gender, array('M', 'FN', 'FY'))) {
                $result = 'ERROR';
                $errorMessage .= $templeMessage->gender_invalid;
            }

            $caseType = strtoupper($messages[7]);
            if(!in_array($caseType, array('U', 'C', 'S', 'D'))) {
                $result = 'ERROR';
                $errorMessage .= $templeMessage->mprf_case_invalid;

                if($caseType == 'D'){
                    $deathValidation = $this->MprfCases
                            ->find()
                            ->where([
                                'first_name'    => ucfirst($first_name),
                                'last_name'     => ucfirst($last_name),
                                'template_code' => 'ADG'
                            ])->first();
                        if(!is_null($deathValidation)){
                            $deathValidation->death_date = date('Y-m-d h:i:s' , strtotime($consultDate));
                            $this->MprfCases->save($deathValidation);
                        }else{
                            $result = 'ERROR';
                            $errorMessage .= $templeMessage->corresponding_adg;
                        }
                }
            }
            
            $examRes = strtoupper($messages[8]);
            if(!in_array($examRes, array('PF', 'PV', 'PM', 'PO', 'PF/PV', 'PF/PM', 'PV/PM', 'NF', 'CDX'))) {
                $result = 'ERROR';
                $errorMessage .= $templeMessage->exam_result_invalid;
            }

            if($result == 'OK') {
                
                $caseDuplicate = $this->MprfCases
                            ->find()
                            ->where([
                                'template_code' => 'ADG',
                                'last_name'     => ucfirst($last_name),
                                'first_name'    => ucfirst($first_name),
                                'consult_date'  => date('Y-m-d' , strtotime($consultDate)),
                                'user_id'       => $user->id
                            ])
                            ->first();

                if(!is_null($caseDuplicate)){
                    $result = 'ERROR';
                    $errorMessage .= $templeMessage->duplicate_name;
                }else{

                    $case = $this->MprfCases->newEntity();
                    $case->sms_incoming_id  = $smsIncoming_id;
                    $case->user_id          = $user->id;
                    $case->source           = 'SMS';
                    $case->facility_id      = $user->facility_id;
                    $case->consult_date     = date('Y-m-d' , strtotime($consultDate));
                    $case->created          = date('Y-m-d h:i:s');
                    $case->last_name        = ucfirst($last_name);
                    $case->first_name       = ucfirst($first_name);
                    $case->age              = $age;
                    $case->template_code    = 'ADG';

                    if($gender == 'M'){
                        $gender = 'M';
                    }else{
                        $gender = 'F';
                    }

                    $case->gender           = $gender;
                    $case->case_type        = $caseType;

                    if($examRes == 'PF'){
                        $case->m_pf = 1;
                    }elseif($examRes == 'PV'){
                        $case->m_pv = 1;
                    }elseif($examRes == 'PM'){
                        $case->pm = 1;
                    }elseif($examRes == 'PO'){
                        $case->po = 1;
                    }elseif($examRes == 'PF/PV'){
                        $case->m_pfpv = 1;
                    }elseif($examRes == 'PF/PM'){
                        $case->pfpm = 1;
                    }elseif($examRes == 'PV/PM'){
                        $case->pvpm = 1;
                    }elseif($examRes == 'NF'){
                        $case->nf = 1;
                    }elseif($examRes == 'CDX'){
                        $case->cdx = 1;
                    }

                    $sitio_id               = (!empty($user->facility->sitio))? $user->facility->sitio->id : '' ;
                    $barangay_id            = (!empty($user->facility->barangay))? $user->facility->barangay->id : '' ;
                    $municipality_id        = (!empty($user->facility->municipality))? $user->facility->municipality->id : '' ;
                    $province_id            = (!empty($user->facility->province))? $user->facility->province->id : '' ;
                    $region_id              = (!empty($user->facility->region))? $user->facility->region->id : '' ;

                    $case->sitio_id        = $sitio_id;
                    $case->barangay_id     = $barangay_id;
                    $case->municipality_id = $municipality_id;
                    $case->province_id     = $province_id;
                    $case->region_id       = $region_id;
                    $case->remarks         = $remarks;

                    if($this->MprfCases->save($case)) {
                        $errorMessage       = 'Valid Malaria Case!';
                        
                    }else{
                        $result = 'ERROR';
                        foreach($case->errors() as $errors){
                            if(is_array($errors)){
                                foreach($errors as $error){
                                    $errorMessage    .=   $error;
                                }
                            }else{
                                $errorMessage    .=   $errors;
                            }
                        }
                    }
                }
            }
        }

        // echo json_encode(array('result' => $result, 'message' => $errorMessage));
        // die;

        return array('result' => $result, 'message' => $errorMessage, 'full_name' => $full_name, 'user_id' => $user_id);
    }
    /*
     * DTH - Malaria Death Case
     */
    public function dth($smsIncoming_id = null, $messages = null) {

        // $text = 'dth,aton123,04/05/2016,hans,samuel,sito,bargy,mun,prov,remarks';
        // $messages   = array_map('trim', explode(',', $text));

        $this->autoRender = false;
        $result = 'OK';
        $errorMessage = ''; 
        $full_name = '';
        $user_id = null;

        $this->loadModel('TemplateSettings');
        $templeMessage = $this->TemplateSettings->find()->where(['template_id' => 3])->first();



        if(!isset($messages[1]) && empty($messages[1]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->user_code;
        }

        if(!isset($messages[2]) && empty($messages[2]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->report_date;
        }

        if(!isset($messages[3]) && empty($messages[3]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->last_name;
        }

        if(!isset($messages[4]) && empty($messages[4]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->first_name;
        }

        // if(!isset($messages[5]) && empty($messages[5]) ) {
        //     $result = 'ERROR';
        //     $errorMessage .= 'Please provide Sitio. ';
        // }
        // if(!isset($messages[6]) && empty($messages[6]) ) {
        //     $result = 'ERROR';
        //     $errorMessage .= 'Please provide Barangay. ';
        // }
        // if(!isset($messages[7]) && empty($messages[7]) ) {
        //     $result = 'ERROR';
        //     $errorMessage .= 'Please provide Municipality. ';
        // }
        // if(!isset($messages[8]) && empty($messages[8]) ) {
        //     $result = 'ERROR';
        //     $errorMessage .= 'Please provide Province. ';
        // }

        if($result == 'OK') {

            $death_date = $messages[2];
            $last_name  = $messages[3];
            $first_name = $messages[4];
            $sitio          = isset($messages[5])? $messages[5] : '' ;
            $barangay       = isset($messages[6])? $messages[6] : '' ;
            $municipality   = isset($messages[7])? $messages[7] : '' ;
            $province       = isset($messages[8])? $messages[8] : '' ;
            $remarks        = isset($messages[9])? $messages[9] : '' ;


            $this->MprfCases = TableRegistry::get('MprfCases');
            $userCode    = strtoupper($messages[1]);
            $this->loadModel('Users');
            $user = $this->Users
                    ->find()
                    ->where(['Users.code' => $userCode])
                    ->contain([
                        'Facilities',
                        'Facilities.Provinces', 
                        'Facilities.Regions',
                        'Facilities.Municipalities',
                        'Facilities.Barangays',
                        'Facilities.Sitios'
                    ])->first();

            if(is_null($user)){
                $result = 'ERROR';
                $errorMessage .= $templeMessage->user_code_invalid;
            }else{
                $user_id = $user->id;
                if($user->activated != 1){
                    $result = 'ERROR';
                    $errorMessage .= $templeMessage->user_activation;
                }else{

                    $full_name = $user->first_name.' '.$user->last_name;
                    if(empty($user->facility)){
                        $result = 'ERROR';
                        $errorMessage .= $templeMessage->facility_code;
                    }

                    $reportDateArray = explode('/', $death_date);
                    if(count($reportDateArray) == 3){
                        if(isset($reportDateArray[0]) && isset($reportDateArray[1]) && isset($reportDateArray[2]) ){
                            $date_err = 'ok';
                            if($reportDateArray[0] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[1] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[2] == ''){
                                $date_err = 'err';
                            }
                            if($date_err == 'ok'){
                                if (!checkdate($reportDateArray[0], $reportDateArray[1], $reportDateArray[2])) {
                                    $result = 'ERROR';
                                    $errorMessage .= $templeMessage->error_report_date;
                                }

                                $dDate      = strtotime(date('Y-m-d', strtotime($death_date)));
                                $date_today = strtotime(date('Y-m-d'));
                                if($dDate > $date_today){
                                    $result = 'ERROR';
                                    $errorMessage .= $templeMessage->invalid_report_date;
                                }
                            }else{
                                $result = 'ERROR';
                                $errorMessage = $templeMessage->error_report_date;
                            }
                        }else{
                            $result = 'ERROR';
                            $errorMessage = $templeMessage->error_report_date;
                        }
                    }else{
                        $result = 'ERROR';
                        $errorMessage = $templeMessage->error_report_date;
                    }

                }


            }

            $deathValidation = $this->MprfCases
                ->find()
                ->where([
                    'first_name'    => $first_name,
                    'last_name'     => $last_name,
                    'template_code' => 'ADG'
                ])->first();

            if(!is_null($deathValidation)){
                $deathValidation->death_date = date('Y-m-d h:i:s' , strtotime($death_date));
                $this->MprfCases->save($deathValidation);

            }else{
                $result = 'ERROR';
                $errorMessage .= $templeMessage->corresponding_adg;
            }

            $caseDuplicate = $this->MprfCases
                    ->find()
                    ->where([
                        'template_code' => 'DTH',
                        'last_name'     => $last_name,
                        'first_name'    => $first_name,
                        'death_date'    => date('Y-m-d' , strtotime($death_date)),
                        'user_id'       => $user->id
                    ])
                    ->first();

            if(!is_null($caseDuplicate)){
                $result = 'ERROR';
                $errorMessage .= $templeMessage->duplicate_name;
            }

    

            if($result == 'OK') {

                $sitio_id = (!empty($user->facility->sitio))? $user->facility->sitio->id : '' ;
                $barangay_id = (!empty($user->facility->barangay))? $user->facility->barangay->id : '' ;
                $municipality_id = (!empty($user->facility->municipality))? $user->facility->municipality->id : '' ;
                $province_id = (!empty($user->facility->province))? $user->facility->province->id : '' ;
                $region_id              = (!empty($user->facility->region))? $user->facility->region->id : '' ;

                $case = $this->MprfCases->newEntity();
                $case->sms_incoming_id  = $smsIncoming_id;
                $case->user_id          = $user->id;
                $case->source           = 'SMS';
                $case->facility_id      = $user->facility_id;
                $case->death_date       = date('Y-m-d h:i:s' , strtotime($death_date));
                $case->created          = date('Y-m-d h:i:s');
                $case->last_name        = $last_name;
                $case->first_name       = $first_name;

                $case->sitio_text        = $sitio;
                $case->barangay_text     = $barangay;
                $case->municipality_text = $municipality;
                $case->province_text     = $province;
                $case->template_code     = 'DTH';
                $case->case_type         = 'D';
                $case->sitio_id        = $sitio_id;
                $case->barangay_id     = $barangay_id;
                $case->municipality_id = $municipality_id;
                $case->province_id     = $province_id;
                $case->region_id       = $region_id;
                $case->remarks         = $remarks;

                if($this->MprfCases->save($case)) {
                    $errorMessage       = 'Valid Malaria DTH Case!';

                }else{
                    $result = 'ERROR';
                    foreach($case->errors() as $errors){
                        if(is_array($errors)){
                            foreach($errors as $error){
                            $errorMessage    .=   $error;
                            }
                        }else{
                            $errorMessage    .=   $errors;
                        }
                    }
                }
            }
        }
        return array('result' => $result, 'message' => $errorMessage, 'full_name' => $full_name, 'user_id' => $user_id);
    }

    public function _codeValidation($codeValue = null){

        // $test = 'PQ    1000';
        // $test = trim(str_replace(' ', '',$test));

        $mainCode = 'errno';
        $value = 0;

        if(preg_match("/^AL([0-9]{0,6})$|^PQ([0-9]{0,6})$|^CQ([0-9]{0,6})$|^QT([0-9]{0,6})$|^QA([0-9]{0,6})$/", $codeValue, $matches)){
           $code = substr($codeValue, 0, 2);
            if($code == 'AL'){
                $mainCode = 'AL';
                $al     = str_replace($code, '', $codeValue);
                $value  = ($al != '')? $al : 0 ;

            }elseif($code == 'PQ'){

                $mainCode = 'PQ';
                $pq = str_replace($code, '', $codeValue);
                $value = ($pq != '')? $pq : 0 ;

            }elseif($code == 'CQ'){

                $mainCode = 'CQ';
                $cq = str_replace($code, '', $codeValue);
                $value = ($cq != '')? $cq : 0 ;

            }elseif($code == 'QT'){

                $mainCode = 'QT';
                $qt = str_replace($code, '', $codeValue);
                $value = ($qt != '')? $qt : 0 ;

            }elseif($code == 'QA'){

                $mainCode = 'QA';
                $qa = str_replace($code, '', $codeValue);
                $value = ($qa != '')? $qa : 0 ;

            }
        }
        return array('code' => $mainCode, 'value' => $value);


    }




    public function dmw($smsIncoming_id = null, $messages = null){
        // $text = 'dmw,ZBL95,04/05/16,al100,pq122,cq100,qt200,qa1000,hehe';
        //  $messages   = array_map('trim', explode(',', $text));
        $this->autoRender = false;
        $result = 'OK';
        $errorMessage = ''; 
        $array = [ 
            'AL' => 'err', 
            'PQ' => 'err', 
            'CQ' => 'err', 
            'QT' => 'err', 
            'QA' => 'err'
        ];
        $full_name = '';
        $remarks = '';
        $itemCodes = '';
        $user_id = null;

        $this->loadModel('TemplateSettings');
        $templeMessage = $this->TemplateSettings->find()->where(['template_id' => 4])->first();


        if(!isset($messages[1]) && empty($messages[1]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->user_code;
        }
        if(!isset($messages[2]) && empty($messages[2]) ) {
            $result = 'ERROR';
            $errorMessage .=  $templeMessage->report_date;
        }
        if(isset($messages[3]) && !empty($messages[3]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[3])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }
        if(isset($messages[4]) && !empty($messages[4]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[4])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }
        if(isset($messages[5]) && !empty($messages[5]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[5])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }

        }
        if(isset($messages[6]) && !empty($messages[6]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[6])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }
        if(isset($messages[7]) && !empty($messages[7]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[7])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }

        foreach ($array as $key => $value) {
            if($value === 'err'){
                $result = 'ERROR';
                $errorMessage .= $templeMessage->stock_code.' '.$key.'.';
            }
        }

        if($result == 'OK') {
            $report_date  = $messages[2];
            if(isset($messages[8])){
                $remarks = $messages[8];
            }
            $userCode    = strtoupper($messages[1]);
            $this->loadModel('Users');
            $user = $this->Users
                    ->find()
                    ->where(['Users.code' => $userCode])
                    ->contain([
                        'Facilities',
                        'Facilities.Provinces', 
                        'Facilities.Regions',
                        'Facilities.Municipalities',
                        'Facilities.Barangays',
                        'Facilities.Sitios'
                    ])->first();

            if(is_null($user)){
                $result = 'ERROR';
                $errorMessage .= $templeMessage->user_code_invalid;
            }else{
                $user_id = $user->id;

                if($user->activated != 1){
                    $result = 'ERROR';
                    $errorMessage .= $templeMessage->user_activation;
                }else{

                    $full_name = $user->first_name.' '.$user->last_name;

                    $reportDateArray = explode('/', $report_date);
                    if(count($reportDateArray) == 3){
                        if(isset($reportDateArray[0]) && isset($reportDateArray[1]) && isset($reportDateArray[2]) ){
                            $date_err = 'ok';
                            if($reportDateArray[0] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[1] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[2] == ''){
                                $date_err = 'err';
                            }
                            if($date_err == 'ok'){
                                if (!checkdate($reportDateArray[0], $reportDateArray[1], $reportDateArray[2])) {
                                    $result = 'ERROR';
                                    $errorMessage .= $templeMessage->error_report_date;
                                }
                                $rDate      = strtotime(date('Y-m-d', strtotime($report_date)));

                                $yearToday = date('Y');
                                $yeatNext = date('Y', strtotime('+1 month'));


                                $start_ts = strtotime($yearToday.'-12-26');
                                $end_date = strtotime($yeatNext.'-01-05');

                                if(($rDate >= $start_ts) && ($rDate <= $end_ts)){

                                }else{
                                    $result = 'ERROR';
                                    $errorMessage .= $templeMessage->error_report_date;
                                }

                                $date_today = strtotime(date('Y-m-d'));
                                if($rDate > $date_today){
                                    $result = 'ERROR';
                                    $errorMessage =  $templeMessage->invalid_report_date;
                                }
                            }else{
                                $result = 'ERROR';
                                $errorMessage = $templeMessage->error_report_date;
                            }
                        }else{
                            $result = 'ERROR';
                            $errorMessage = $templeMessage->error_report_date;
                        }
                    }else{
                        $result = 'ERROR';
                        $errorMessage = $templeMessage->error_report_date;
                    }
                }
            }

            if($result == 'OK') {
                $itemCodes = 'AL :'.$array['AL'].', PQ :'.$array['PQ'].', CQ :'.$array['CQ'].', QT :'.$array['QT'].', QA :'.$array['QA'];
                $this->loadModel('Items');
                $item = $this->Items->newEntity();
                $item->created           = date('Y-m-d h:i:s');
                $item->sms_incoming_id   = $smsIncoming_id;
                $item->report_date       = date('Y-m-d h:i:s' , strtotime($report_date));
                $item->user_id           = $user->id;
                $item->source            = 'SMS';
                $item->facility_id       = $user->facility_id;
                $item->remarks           = $remarks;

                $sitio_id               = (!empty($user->facility->sitio))? $user->facility->sitio->id : '' ;
                $barangay_id            = (!empty($user->facility->barangay))? $user->facility->barangay->id : '' ;
                $municipality_id        = (!empty($user->facility->municipality))? $user->facility->municipality->id : '' ;
                $province_id            = (!empty($user->facility->province))? $user->facility->province->id : '' ;
                $region_id              = (!empty($user->facility->region))? $user->facility->region->id : '' ;

                $item->template_code     = 'DMW';

                $item->sitio_id        = $sitio_id;
                $item->barangay_id     = $barangay_id;
                $item->municipality_id = $municipality_id;
                $item->province_id     = $province_id;
                $item->region_id       = $region_id;

                $item->al_qty       = $array['AL'];
                $item->pq_qty       = $array['PQ'];
                $item->cq_qty       = $array['CQ'];
                $item->qt_qty       = $array['QT'];
                $item->qa_qty       = $array['QA'];

                if($this->Items->save($item)) {

                }
            }
        }
        // debug(array('result' => $result, 'message' => $errorMessage));
        // die;
        return array('result' => $result, 'message' => $errorMessage, 'full_name' => $full_name, 'itemCodes' => $itemCodes, 'user_id' => $user_id);
    }

   public function ost($smsIncoming_id = null, $messages = null){

        // $text = 'ost,ohh65,04/05/2016,al10';
        // $messages   = array_map('trim', explode(',', $text));
        $this->autoRender = false;
        $full_name = '';
        $result = 'OK';
        $errorMessage = ''; 
        $remarks = '';
        $itemCodes = '';
        $user_id = null;

        $array = [ 
            'AL' => null, 
            'PQ' => null, 
            'CQ' => null, 
            'QT' => null, 
            'QA' => null
        ];


        $this->loadModel('TemplateSettings');
        $templeMessage = $this->TemplateSettings->find()->where(['template_id' => 5])->first();


        if(!isset($messages[1]) && empty($messages[1]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->user_code;
        }
        if(!isset($messages[2]) && empty($messages[2]) ) {
            $result = 'ERROR';
            $errorMessage .= $templeMessage->report_date;
        }

        if(isset($messages[3]) && !empty($messages[3]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[3])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }

        if(isset($messages[4]) && !empty($messages[4]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[4])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }
        if(isset($messages[5]) && !empty($messages[5]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[5])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }

        }
        if(isset($messages[6]) && !empty($messages[6]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[6])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }
        if(isset($messages[7]) && !empty($messages[7]) ) {
            $code = strtoupper(trim(str_replace(' ', '',$messages[7])));
            $valid = $this->_codeValidation($code);
            if($valid['code'] != 'errno'){
                $valid['code'];
                foreach ($array as $value => $key) {
                    if($value == $valid['code']){
                        $array[$value] = $valid['value'];
                    }
                }
            }
        }


        if($array['AL'] === null && $array['PQ'] === null && $array['CQ'] === null && $array['QT'] === null && $array['QA'] === null){
            $result = 'ERROR';
            $errorMessage .= $templeMessage->stock_code;
        }


        if($result == 'OK') {

            $report_date  = $messages[2];
            if(isset($messages[8])){
                $remarks = $messages[8];
            }

            $userCode    = strtoupper($messages[1]);
            $this->loadModel('Users');
            $user = $this->Users
                    ->find()
                    ->where(['Users.code' => $userCode])
                    ->contain([
                        'Facilities',
                        'Facilities.Provinces', 
                        'Facilities.Regions',
                        'Facilities.Municipalities',
                        'Facilities.Barangays',
                        'Facilities.Sitios'
                    ])->first();

            if(is_null($user)){
                $result = 'ERROR';
                $errorMessage .=  $templeMessage->user_code_invalid;

            }else{
                $user_id = $user->id;
                if($user->activated != 1){
                    $result = 'ERROR';
                    $errorMessage .=  $templeMessage->user_activation;
                }else{

                    $full_name = $user->first_name.' '.$user->last_name;

                    $reportDateArray = explode('/', $report_date);
                    if(count($reportDateArray) == 3){
                        if(isset($reportDateArray[0]) && isset($reportDateArray[1]) && isset($reportDateArray[2]) ){
                            $date_err = 'ok';
                            if($reportDateArray[0] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[1] == ''){
                                $date_err = 'err';
                            }

                            if($reportDateArray[2] == ''){
                                $date_err = 'err';
                            }
                            if($date_err == 'ok'){
                                if (!checkdate($reportDateArray[0], $reportDateArray[1], $reportDateArray[2])) {
                                    $result = 'ERROR';
                                    $errorMessage .= $templeMessage->error_report_date;
                                }
                                $rDate      = strtotime(date('Y-m-d', strtotime($report_date)));
                                $date_today = strtotime(date('Y-m-d'));
                                if($rDate > $date_today){
                                    $result = 'ERROR';
                                    $errorMessage = $templeMessage->invalid_report_date;
                                }
                            }else{
                                $result = 'ERROR';
                                $errorMessage = $templeMessage->error_report_date;
                            }
                        }else{
                            $result = 'ERROR';
                            $errorMessage = $templeMessage->error_report_date;
                        }
                    }else{
                        $result = 'ERROR';
                        $errorMessage = $templeMessage->error_report_date;
                    }
                }
            }



            if($result == 'OK') {
                
               $itemCodes = 'AL :'.$array['AL'].', PQ :'.$array['PQ'].', CQ :'.$array['CQ'].', QT :'.$array['QT'].', QA :'.$array['QA'];

                $this->loadModel('Items');
                $item = $this->Items->newEntity();
                $item->created           = date('Y-m-d h:i:s');
                $item->sms_incoming_id   = $smsIncoming_id;
                $item->report_date       = date('Y-m-d h:i:s' , strtotime($report_date));
                $item->user_id           = $user->id;
                $item->source            = 'SMS';
                $item->facility_id       = $user->facility_id;
                $item->remarks           = $remarks;
                $item->template_code     = 'OST';

                $sitio_id               = (!empty($user->facility->sitio))? $user->facility->sitio->id : '' ;
                $barangay_id            = (!empty($user->facility->barangay))? $user->facility->barangay->id : '' ;
                $municipality_id        = (!empty($user->facility->municipality))? $user->facility->municipality->id : '' ;
                $province_id            = (!empty($user->facility->province))? $user->facility->province->id : '' ;
                $region_id              = (!empty($user->facility->region))? $user->facility->region->id : '' ;

                $item->sitio_id        = $sitio_id;
                $item->barangay_id     = $barangay_id;
                $item->municipality_id = $municipality_id;
                $item->province_id     = $province_id;
                $item->region_id       = $region_id;

                $item->al_qty       = $array['AL'];
                $item->pq_qty       = $array['PQ'];
                $item->cq_qty       = $array['CQ'];
                $item->qt_qty       = $array['QT'];
                $item->qa_qty       = $array['QA'];

                if($this->Items->save($item)) {
                }
            }
        }

       return array('result' => $result, 'message' => $errorMessage, 'full_name' => $full_name, 'itemCodes' => $itemCodes, 'user_id' => $user_id);

   }




}