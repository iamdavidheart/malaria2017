<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TemplateNotifications Controller
 *
 * @property \App\Model\Table\TemplateNotificationsTable $TemplateNotifications
 */
class TemplateNotificationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Templates']
        ];
        $templateNotifications = $this->paginate($this->TemplateNotifications);

        $this->set(compact('templateNotifications'));
        $this->set('_serialize', ['templateNotifications']);
    }

    /**
     * View method
     *
     * @param string|null $id Template Notification id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $templateNotification = $this->TemplateNotifications->get($id, [
            'contain' => ['Users', 'Templates']
        ]);

        $this->set('templateNotification', $templateNotification);
        $this->set('_serialize', ['templateNotification']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $templateNotification = $this->TemplateNotifications->newEntity();
        if ($this->request->is('post')) {
            $templateNotification = $this->TemplateNotifications->patchEntity($templateNotification, $this->request->data);
            if ($this->TemplateNotifications->save($templateNotification)) {
                $this->Flash->success(__('The template notification has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The template notification could not be saved. Please, try again.'));
            }
        }
        $users = $this->TemplateNotifications->Users->find('list', ['limit' => 200]);
        $templates = $this->TemplateNotifications->Templates->find('list', ['limit' => 200]);
        $this->set(compact('templateNotification', 'users', 'templates'));
        $this->set('_serialize', ['templateNotification']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Template Notification id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $templateNotification = $this->TemplateNotifications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $templateNotification = $this->TemplateNotifications->patchEntity($templateNotification, $this->request->data);
            if ($this->TemplateNotifications->save($templateNotification)) {
                $this->Flash->success(__('The template notification has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The template notification could not be saved. Please, try again.'));
            }
        }
        $users = $this->TemplateNotifications->Users->find('list', ['limit' => 200]);
        $templates = $this->TemplateNotifications->Templates->find('list', ['limit' => 200]);
        $this->set(compact('templateNotification', 'users', 'templates'));
        $this->set('_serialize', ['templateNotification']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Template Notification id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $temp_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $templateNotification = $this->TemplateNotifications->get($id);
        if ($this->TemplateNotifications->delete($templateNotification)) {
            $this->Flash->success(__('The template notification has been deleted.'));

        } else {
            $this->Flash->error(__('The template notification could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'templates', 'action' => 'edit', $temp_id]);
    }
}
