<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SmsOutgoings Controller
 *
 * @property \App\Model\Table\SmsOutgoingsTable $SmsOutgoings
 */
class SmsOutgoingsController extends AppController
{


    public function inbox()
    {

        $this->loadModel('SmsIncomings');
        $pageno = '';
        $smsIncomings = $this->SmsIncomings
                    ->find()
                    ->where([
                        'SmsIncomings.status' => 'processed'
                    ])
                    ->order('SmsIncomings.id DESC' )
                    ->contain(['Users']);


        $this->set(compact('smsIncomings', 'pageno'));
        $this->set('_serialize', ['smsIncomings']);

    }
    public function loadInbox(){

        $this->loadModel('SmsIncomings');
        $pageno = '';
        $smsIncomings = $this->SmsIncomings
                    ->find()
                    ->where([
                        'SmsIncomings.status' => 'processed'
                    ])
                    ->order('SmsIncomings.id DESC' )
                    ->contain(['Users']);


        $this->set(compact('smsIncomings', 'pageno'));
        $this->set('_serialize', ['smsIncomings']);    }

    public function sent()
    {

    }
    public function loadSent()
    {
        $pageno = '';
        $smsOutgoings = $this->SmsOutgoings
                    ->find()
                    ->where([
                        'SmsOutgoings.status' => 'sent', 'box' => 10
                    ])
                    ->contain(['Users','ReceiverUsers'])
                    ->order(['SmsOutgoings.id DESC']);

        $this->set(compact('smsOutgoings', 'pageno'));
        $this->set('_serialize', ['smsOutgoings']);  
    }



    public function outbox()
    {
    }

    public function loadOutbox()
    {
        $pageno = '';
        $smsOutgoings = $this->SmsOutgoings
                        ->find()
                        ->orWhere(['SmsOutgoings.status' => 'pending', 'box' => 10])
                        ->contain(['Users','ReceiverUsers'])
                        ->order(['SmsOutgoings.id DESC']);

        $this->set(compact('smsOutgoings', 'pageno'));
        $this->set('_serialize', ['smsOutgoings']);
    }

    
    public function spam()
    {

        $this->loadModel('SmsIncomings');
        $pageno = '';
        $smsIncomings = $this->SmsIncomings
                    ->find()
                    ->where(['SmsIncomings.status' => 'spam'])
                    ->contain(['Users'])
                    ->order('SmsIncomings.id DESC' );

        $this->set(compact('smsIncomings', 'pageno'));
        $this->set('_serialize', ['smsIncomings']);
    }

    public function loadSpam()
    {

        $this->loadModel('SmsIncomings');
        $pageno = '';
        $smsIncomings = $this->SmsIncomings
                    ->find()
                    ->where(['SmsIncomings.status' => 'spam'])
                    ->contain(['Users'])
                    ->order('SmsIncomings.id DESC' );

        $this->set(compact('smsIncomings', 'pageno'));
        $this->set('_serialize', ['smsIncomings']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Groups', 'SmsIncomings', 'Receivers', 'Users']
        ];
        $smsOutgoings = $this->paginate($this->SmsOutgoings);

        $this->set(compact('smsOutgoings'));
        $this->set('_serialize', ['smsOutgoings']);
    }

    /**
     * View method
     *
     * @param string|null $id Sms Outgoing id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsOutgoing = $this->SmsOutgoings->get($id, [
            'contain' => ['Groups', 'SmsIncomings', 'Receivers', 'Users']
        ]);

        $this->set('smsOutgoing', $smsOutgoing);
        $this->set('_serialize', ['smsOutgoing']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smsOutgoing = $this->SmsOutgoings->newEntity();
        if ($this->request->is('post')) {
            $smsOutgoing = $this->SmsOutgoings->patchEntity($smsOutgoing, $this->request->data);
            if ($this->SmsOutgoings->save($smsOutgoing)) {
                $this->Flash->success(__('The sms outgoing has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms outgoing could not be saved. Please, try again.'));
            }
        }
        $groups = $this->SmsOutgoings->Groups->find('list', ['limit' => 200]);
        $smsIncomings = $this->SmsOutgoings->SmsIncomings->find('list', ['limit' => 200]);
        $receivers = $this->SmsOutgoings->Receivers->find('list', ['limit' => 200]);
        $users = $this->SmsOutgoings->Users->find('list', ['limit' => 200]);
        $this->set(compact('smsOutgoing', 'groups', 'smsIncomings', 'receivers', 'users'));
        $this->set('_serialize', ['smsOutgoing']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sms Outgoing id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsOutgoing = $this->SmsOutgoings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsOutgoing = $this->SmsOutgoings->patchEntity($smsOutgoing, $this->request->data);
            if ($this->SmsOutgoings->save($smsOutgoing)) {
                $this->Flash->success(__('The sms outgoing has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms outgoing could not be saved. Please, try again.'));
            }
        }
        $groups = $this->SmsOutgoings->Groups->find('list', ['limit' => 200]);
        $smsIncomings = $this->SmsOutgoings->SmsIncomings->find('list', ['limit' => 200]);
        $receivers = $this->SmsOutgoings->Receivers->find('list', ['limit' => 200]);
        $users = $this->SmsOutgoings->Users->find('list', ['limit' => 200]);
        $this->set(compact('smsOutgoing', 'groups', 'smsIncomings', 'receivers', 'users'));
        $this->set('_serialize', ['smsOutgoing']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sms Outgoing id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsOutgoing = $this->SmsOutgoings->get($id);
        if ($this->SmsOutgoings->delete($smsOutgoing)) {
            $this->Flash->success(__('The sms outgoing has been deleted.'));
        } else {
            $this->Flash->error(__('The sms outgoing could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSms()
    {


        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $json = [
            'error' => false,
            'message' => 'Success'
        ];

        $ids =  $this->request->data['id'];
        if(!empty($ids)){

            foreach ($ids as $key => $id) {
     
                $smsOutgoing = $this->SmsOutgoings->get($id);
                if ($this->SmsOutgoings->delete($smsOutgoing)) {
                   // $this->Flash->success(__('The sms outgoing has been deleted.'));
                } else {
                   // $this->Flash->error(__('The sms outgoing could not be deleted. Please, try again.'));
                }

            }
        }
        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

    }
    public function deleteInc()
    {

        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $json = [
            'error' => false,
            'message' => 'Success'
        ];
        $this->loadModel('SmsIncomings');
        $ids =  $this->request->data['id'];
        if(!empty($ids)){

            foreach ($ids as $key => $id) {
     
                $smsIncoming = $this->SmsIncomings->get($id);
                if ($this->SmsIncomings->delete($smsIncoming)) {
                   // $this->Flash->success(__('The sms outgoing has been deleted.'));
                } else {
                   // $this->Flash->error(__('The sms outgoing could not be deleted. Please, try again.'));
                }

            }
        }
        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

    }



}
