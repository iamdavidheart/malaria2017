<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

class SmsBlastController extends AppController
{

	 /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function list()
    {

    	$this->loadModel('SmsOutgoings');
        $pageno = '';
        $smsOutgoings = $this->SmsOutgoings
                        ->find()
                        ->orWhere(['SmsOutgoings.status' => 'pending', 'SmsOutgoings.box' => 20])
                        ->contain(['Users','ReceiverUsers'])
                        ->order(['SmsOutgoings.id DESC']);


        $this->set(compact('smsOutgoings', 'pageno'));
        $this->set('_serialize', ['smsOutgoings']);



	}

	 /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

    	$this->loadModel('Users');
    	$this->loadModel('Provinces');
        $users = $this->Users->find()
        			->contain(['UserContacts' => function($q){
        				return $q->where(['UserContacts.prime_setup' => 1]);
        			}])
        			->where(['Users.role_id IN' => [2,3,4]]);
       	$provinces = $this->Provinces->find();
        $this->set(compact('users', 'provinces'));
        $this->set('_serialize', ['users']);



    }

    public function loadUsers()
    {

    	$this->loadModel('Users');
        $users = $this->Users->find()
        			->contain(['UserContacts' => function($q){
        				return $q->where(['UserContacts.prime_setup' => 1]);
        			}])
        			->where(['Users.role_id IN' => [2,3,4]]);
        if(isset($this->request->data['province'])){
	    	$arrayProvince = $this->request->data['province'];
	    	if(count($arrayProvince)==  1){
	    		if($arrayProvince[0] != "1x"){
	    			$users->andWhere(['Users.province_id IN' => $arrayProvince ]); 
	    			if(!empty($arrayProvince)){} 
	    		}
	    	}
	      
        }
        if(isset($this->request->data['status'])){
	    	$arrayStatus = $this->request->data['status'];
	    	if($arrayStatus != 2){
	        	$arrayStatus = ($arrayStatus != "1")? 0 : 1 ;
	        	$users->andWhere(['Users.activated IN' => $arrayStatus ]); 
	    	}

        }



        $this->set(compact('users'));
        $this->set('_serialize', ['users']);


    }


    public function sendBlast()
    {


        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $json = [
            'error' => false,
            'message' => 'Success'
        ];
        $this->loadModel('Users');

        $user_ids =  $this->request->data['user_id'];
        $sms =  $this->request->data['sms'];
        if(!empty($user_ids)){

            $this->loadModel('SmsOutgoings');

            $code = $this->generate_code(3,3).'-'.date('Y-m-d h:i');
            foreach ($user_ids as $key => $id) {
         
                $user = $this->Users
                    ->find()
                    ->where(['Users.id' => $id])
                    ->first();
                $user->activated = 1;
                if ($this->Users->save($user)) {

                    $users = $this->Users
                                ->find()
                                ->where(['Users.id' => $id])
                                ->contain(['UserContacts'])
                                ->first();
                    if(!empty($users->user_contacts)){
                        foreach ($users->user_contacts as $key => $user_contact) {

                            if(preg_match("/^(09|\+639|639)\d{9}$/", $user_contact->value )){
                                $smsOutgoings = $this->SmsOutgoings->newEntity();
                                $smsOutgoings->message      = $sms;
                                $smsOutgoings->group_id     = $code;
                                $smsOutgoings->created      =  date('Y-m-d h:i:s');
                                $smsOutgoings->source       = 'WEB';
                                $smsOutgoings->receiver_no  = $user_contact->value;
                                $smsOutgoings->user_id      = $id;
                                $smsOutgoings->status       = 'pending';
                                $smsOutgoings->box          = 20;
                                $smsOutgoings->type         = 'sms blast';
                                if ($this->SmsOutgoings->save($smsOutgoings)) {
                                } else {
                                    $json['error'] = true;
                                    $json['message'] = __('The Error activation alert message, Please try again.');
                                }

                            }
                        }
                    }
                } 
            }
        }
        $this->set(compact('json'));
        $this->set('_serialize', ['json']);

	}


    public function generate_code($s_length = null, $n_length = null) {

			$str='';
			$num='';
			$total_length = $s_length + $n_length ;
			$continue = true;
			do {

				if ($s_length) {
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
					$str = '';
					$size = strlen( $chars );
					for( $i = 0; $i < $s_length; $i++ ) {
						$str .= $chars[ rand( 0, $size - 1 ) ];
					}
					$str = strtoupper($str);
				} else {
					$str = '';
				}	
	
				if ($n_length) {
					$nums = "123456789";
					$num = '';
					$size = strlen($nums);
					for( $i = 0; $i < $n_length; $i++ ) {
						$num .= $nums[ rand( 0, $size - 1 ) ];
					}
					$num = strtoupper($num);
				} else {
					$num = '';
				}
		
					$gen_string = strlen($str) + strlen($num);
					
					if ($total_length == $gen_string) {
						$continue = false;
					}
			
			} while  ($continue);
			
			return $str.$num;

	}


}