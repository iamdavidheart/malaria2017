<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 */
class ItemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $items = $this->Items
                ->find()
                ->contain([
                'Users',
                'Facilities.FacilityCategories', 
                'Facilities.Provinces',
                'Facilities.Municipalities',
                'Facilities.Barangays',
                'Facilities.Sitios'
            ]);
        $user_id = $this->Auth->user('id');
        if($this->Auth->user('role_id') == 4 OR $this->Auth->user('role_id') == 3){
            $items->where(['Items.user_id' => $user_id]);
        }


        $items->where(['Items.template_code' => 'DMW'])->order('Items.id DESC');

        // if($this->Auth->user('role_id') == 3){
        //     $this->loadModel('Users');
        //     $user = $this->Users->find()->where(['Users.id' => $user_id])->contain(['Facilities'])->first();
        //     if(!empty($user->facility)){
        //         $province_id = $user->facility->province_id;
        //         $mprfCases->where(['MprfCases.province_id' => $province_id]);
        //     }else{
        //         $mprfCases = [];
        //     }
        // }
        $this->set(compact('items'));
        $this->set('_serialize', ['items']);
    }

    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => ['Facilities', 'SmsIncomings', 'Users', 'ItemDetails']
        ]);

        $this->set('item', $item);
        $this->set('_serialize', ['item']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $item = $this->Items->newEntity();
        if ($this->request->is('post')) {
        	$this->request->data['template_code'] = 'DMW';
        	$this->request->data['source'] = 'WEB';
        	$this->request->data['sms_incoming_id'] =  0;
            $this->request->data['created'] = date('Y-m-d h:i:s');
            $this->request->data['user_id'] =  $this->Auth->user('id');

        	$this->request->data['report_date'] =  date('Y-m-d h:i:s' , strtotime($this->request->data['report_date']));


            $item = $this->Items->patchEntity($item, $this->request->data);

            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));
                return $this->redirect(['action' => 'edit', $item->id]);
            } else {
            	debug($item->errors());
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Facilities');
        $facilities = $this->Facilities->find()->contain(['FacilityCategories','Regions', 'Municipalities', 'Provinces', 'Barangays', 'Sitios']);



        $this->set(compact('item', 'facilities'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Facilities');
        $facilities = $this->Facilities->find()->contain(['FacilityCategories','Regions', 'Municipalities', 'Provinces', 'Barangays', 'Sitios']);

        $this->set(compact('item', 'facilities'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $item = $this->Items->get($id);
        if ($this->Items->delete($item)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
