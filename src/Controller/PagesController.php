<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function login()
    {

        //$this->viewBuilder()->layout('LuvidaTemp.login');

        // if($this->Auth->user()){
        //     return $this->redirect(['controller' => 'pages', 'action' => 'index']);
        // }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);

                // $this->Flash->success(__('Welcome User'));
              

                return $this->redirect(['controller' => 'pages', 'action' => 'index']);

            }else{
                echo 'heheh';
            }

            die;
            $this->Flash->error(__("Username or password is incorrect."));
        }

        
    }

    /**
     * Logout an user.
     *
     * @return \Cake\Network\Response
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }


    public function index()
    {

        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
    
             $this->Flash->success(__('Welcome User'));
                    return $this->redirect(['controller' => 'pages', 'action' => 'index']);


            }else{
               $this->Flash->error(__("Username or password is incorrect."));
                return $this->redirect(['controller' => 'pages', 'action' => 'index']);
               
            }


            die;
        }





    $year = date("Y");
    if($this->request->query('year')){
        $year = $this->request->query('year');
    }


    // $start_date = date('Y-m-d',strtotime(date('Y-01-01')));
    // $end_date   = date('Y-m-d');

    $ds     = date('Y-m-d', strtotime(date('Y-01-01')));
    $today  = date('Y-m-d');
    $de     = date('Y-m-d');

    if($this->request->query('ds')){
        $ds = $this->request->query('ds');
    }

    if($this->request->query('de')){
        $de = $this->request->query('de');
    }



    $this->loadModel("MprfCases");
    $result = $this->MprfCases
            ->find()
            ->where(function($exp) use ($ds, $de){
                return $exp->between('MprfCases.consult_date', $ds, $de);
            });

        $sms = $result->newExpr()->addCase($result->newExpr()->add(['MprfCases.source' => 'SMS']));
        $web = $result->newExpr()->addCase($result->newExpr()->add(['MprfCases.source' => 'WEB']));

        $result
        ->select([
            'sms' => $result->func()->sum($sms),
            'web' => $result->func()->sum($web)
        ]);



    $this->loadModel("Provinces");
    $query = $this->Provinces->find()
    ->innerJoinWith('MprfCases', function ($q) use ($year) {
        return $q
            ->where(['YEAR(MprfCases.consult_date)' => $year]);
    });

   $province = $query->func()->concat(['Provinces.description' => 'literal']);

    $_month = [
    1 => 'jan', 2 => 'feb', 3 => 'mar', 4 => 'apr', 
    5 => 'may', 6 => 'jun', 7 => 'jul', 8 => 'aug', 
    9 => 'sep', 10 => 'oct', 11 => 'nov', 12 => 'dec']; 

    foreach ($_month as $key => $value) {
        $$value = $query->newExpr()->addCase
        (
            $query->newExpr()->add([
                'month(MprfCases.consult_date)' => $key
            ])
        );
    }
    $query
        ->select([
         'province' => $province,
            // 'year' => $malariaYear,
            'jan' => $query->func()->sum($jan),
            'feb' => $query->func()->sum($feb),
            'mar' => $query->func()->sum($mar),
            'apr' => $query->func()->sum($apr),
            'may' => $query->func()->sum($may),
            'jun' => $query->func()->sum($jun),
            'jul' => $query->func()->sum($jul),
            'aug' => $query->func()->sum($aug),
            'sep' => $query->func()->sum($sep),
            'oct' => $query->func()->sum($oct),
            'nov' => $query->func()->sum($nov),
            'dec' => $query->func()->sum($dec)
        ])
        ->group(['Provinces.description']);

        //*******************************************************************
        $uQuery = $this->Provinces->find()
        ->innerJoinWith('MprfCases', function ($q) use ($year) {
            return $q
                ->where(['YEAR(MprfCases.created)' => $year]);
        });

        foreach ($_month as $key => $value) {
            $$value = $uQuery->newExpr()->addCase
            (
                $uQuery->newExpr()->add([
                    'month(MprfCases.consult_date)' => $key, 'MprfCases.case_type' => 'u'
                ])
            );
        }

        $_monthC = [
        1 => 'janc', 2 => 'febc', 3 => 'marc', 4 => 'aprc', 
        5 => 'mayc', 6 => 'junc', 7 => 'julc', 8 => 'augc', 
        9 => 'sepc', 10 => 'octc', 11 => 'novc', 12 => 'decc']; 

        foreach ($_monthC as $key => $value) {
            $$value = $uQuery->newExpr()->addCase
            (
                $uQuery->newExpr()->add([
                    'month(MprfCases.consult_date)' => $key, 'MprfCases.case_type' => 'C'
                ])
            );
        }


        $_monthS = [
        1 => 'jans', 2 => 'febs', 3 => 'mars', 4 => 'aprs', 
        5 => 'mays', 6 => 'juns', 7 => 'juls', 8 => 'augs', 
        9 => 'seps', 10 => 'octs', 11 => 'novs', 12 => 'decs']; 

        foreach ($_monthS as $key => $value) {
            $$value = $uQuery->newExpr()->addCase
            (
                $uQuery->newExpr()->add([
                    'month(MprfCases.consult_date)' => $key, 'MprfCases.case_type' => 'S'
                ])
            );
        }

        $_monthD = [
        1 => 'jand', 2 => 'febd', 3 => 'mard', 4 => 'aprd', 
        5 => 'mayd', 6 => 'jund', 7 => 'juld', 8 => 'augd', 
        9 => 'sepd', 10 => 'octd', 11 => 'novd', 12 => 'decd']; 

        foreach ($_monthD as $key => $value) {
            $$value = $uQuery->newExpr()->addCase
            (
                $uQuery->newExpr()->add([
                    'month(MprfCases.death_date)' => $key, 'MprfCases.case_type' => 'D'
                ])
            );
        }


        $uQuery
            ->select([
                'jan' => $uQuery->func()->sum($jan),
                'feb' => $uQuery->func()->sum($feb),
                'mar' => $uQuery->func()->sum($mar),
                'apr' => $uQuery->func()->sum($apr),
                'may' => $uQuery->func()->sum($may),
                'jun' => $uQuery->func()->sum($jun),
                'jul' => $uQuery->func()->sum($jul),
                'aug' => $uQuery->func()->sum($aug),
                'sep' => $uQuery->func()->sum($sep),
                'oct' => $uQuery->func()->sum($oct),
                'nov' => $uQuery->func()->sum($nov),
                'dec' => $uQuery->func()->sum($dec),

                'janc' => $uQuery->func()->sum($janc),
                'febc' => $uQuery->func()->sum($febc),
                'marc' => $uQuery->func()->sum($marc),
                'aprc' => $uQuery->func()->sum($aprc),
                'mayc' => $uQuery->func()->sum($mayc),
                'junc' => $uQuery->func()->sum($junc),
                'julc' => $uQuery->func()->sum($julc),
                'augc' => $uQuery->func()->sum($augc),
                'sepc' => $uQuery->func()->sum($sepc),
                'octc' => $uQuery->func()->sum($octc),
                'novc' => $uQuery->func()->sum($novc),
                'decc' => $uQuery->func()->sum($decc),

                'jans' => $uQuery->func()->sum($jans),
                'febs' => $uQuery->func()->sum($febs),
                'mars' => $uQuery->func()->sum($mars),
                'aprs' => $uQuery->func()->sum($aprs),
                'mays' => $uQuery->func()->sum($mays),
                'juns' => $uQuery->func()->sum($juns),
                'juls' => $uQuery->func()->sum($juls),
                'augs' => $uQuery->func()->sum($augs),
                'seps' => $uQuery->func()->sum($seps),
                'octs' => $uQuery->func()->sum($octs),
                'novs' => $uQuery->func()->sum($novs),
                'decs' => $uQuery->func()->sum($decs),

                'jand' => $uQuery->func()->sum($jand),
                'febd' => $uQuery->func()->sum($febd),
                'mard' => $uQuery->func()->sum($mard),
                'aprd' => $uQuery->func()->sum($aprd),
                'mayd' => $uQuery->func()->sum($mayd),
                'jund' => $uQuery->func()->sum($jund),
                'juld' => $uQuery->func()->sum($juld),
                'augd' => $uQuery->func()->sum($augd),
                'sepd' => $uQuery->func()->sum($sepd),
                'octd' => $uQuery->func()->sum($octd),
                'novd' => $uQuery->func()->sum($novd),
                'decd' => $uQuery->func()->sum($decd)

            ]);

            // debug($uQuery->toArray());
            // die;
   
        
        $start_date = date('m/d/Y', strtotime($ds));
        $end_date   = date('m/d/Y', strtotime($de));

        $this->set(compact( 'provinces', 'query', 'uQuery', 'start_date', 'end_date', 'result'));
        $this->set('_serialize', [ 'provinces', 'query', 'uQuery', 'result']);





    }
    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
