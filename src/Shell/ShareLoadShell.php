<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ShareLoadShell extends Shell
{

    public function request()
    {


        $this->loadModel('ShareLoads');
        $this->loadModel('SmsOutgoings');

        $shareloads = $this->ShareLoads->find()->where(['status' => 'PENDING']);
        $status = 'PENDING';
        foreach ($shareloads as $key => $shareload) {

            if($shareload->network == 'GLOBE'){

                if(preg_match("/^(09|\+639|639)\d{9}$/", $shareload->number )){

                    $nb = $this->prefixNumber($shareload->number, 2);
                    
                    $smsOutgoings = $this->SmsOutgoings->newEntity();
                    $smsOutgoings->message      = 2;
                    $smsOutgoings->created      =  date('Y-m-d h:i:s');
                    $smsOutgoings->source       = 'G-REQUEST';
                    $smsOutgoings->receiver_no  = $nb;
                    $smsOutgoings->user_id      = $shareload->user_id;
                    $smsOutgoings->status       = 'pending';
                    $smsOutgoings->box          = 10;
                    $smsOutgoings->type         = 'request';
                    if ($this->SmsOutgoings->save($smsOutgoings)) {
                        $status = 'PROCESSING';
                    }
                }
            }


            $share = $this->ShareLoads->get($shareload->id);
            $share->status = $status;
            $this->ShareLoads->save($share);
        }
    }
    private function prefixNumber($number, $prefix){
        $nb = "";
        if(preg_match("/^(639)\d{9}$/", $number )){
            $nb = substr_replace($number,$prefix,0,2);
        }elseif(preg_match("/^(09)\d{9}$/", $number )){
            $nb = substr_replace($number,$prefix,0,1);
        }else{
            $nb = substr_replace($number,$prefix,0,3);
        }
        return $nb;
    }


    public function checker()
    {


        $this->loadModel('SmsIncomings');

        $smsIncoings = $this->SmsIncomings->find()->where(['status' => 'LOAD']);
        $status = 'LOAD';
        foreach ($smsIncoings as $key => $smsIncoming) {

            if($smsIncoming->message_type == 'sms-globe'){

                    $smsOutgoings = $this->SmsOutgoings->newEntity();
                    $smsOutgoings->message      = "YES";
                    $smsOutgoings->created      =  date('Y-m-d h:i:s');
                    $smsOutgoings->source       = 'load';
                    $smsOutgoings->receiver_no  = $smsIncoming->sender_no;
                    $smsOutgoings->status       = 'pending';
                    $smsOutgoings->box          = 10;
                    $smsOutgoings->type         = 'reply';
                    if ($this->SmsOutgoings->save($smsOutgoings)) {
                        $status = 'LOADED';
                    }
                }
            }


            $share = $this->SmsIncomings->get($smsIncoming->id);
            $share->status = $status;
            $this->SmsIncomings->save($share);
        }
    }


    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main()
    {
        if (!class_exists('Psy\Shell')) {
            $this->err('<error>Unable to load Psy\Shell.</error>');
            $this->err('');
            $this->err('Make sure you have installed psysh as a dependency,');
            $this->err('and that Psy\Shell is registered in your autoloader.');
            $this->err('');
            $this->err('If you are using composer run');
            $this->err('');
            $this->err('<info>$ php composer.phar require --dev psy/psysh</info>');
            $this->err('');
            return self::CODE_ERROR;
        }

        $this->out("You can exit with <info>`CTRL-C`</info> or <info>`exit`</info>");
        $this->out('');

        Log::drop('debug');
        Log::drop('error');
        $this->_io->setLoggers(false);
        restore_error_handler();
        restore_exception_handler();

        $psy = new PsyShell();
        $psy->run();
    }

    /**
     * Display help for this console.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = new ConsoleOptionParser('console');
        $parser->description(
            'This shell provides a REPL that you can use to interact ' .
            'with your application in an interactive fashion. You can use ' .
            'it to run adhoc queries with your models, or experiment ' .
            'and explore the features of CakePHP and your application.' .
            "\n\n" .
            'You will need to have psysh installed for this Shell to work.'
        );
        return $parser;
    }
}
