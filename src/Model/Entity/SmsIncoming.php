<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SmsIncoming Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $user_id
 * @property string $template_code
 * @property string $sms_code
 * @property string $sender_no
 * @property string $phone_no
 * @property string $message
 * @property string $status
 * @property string $message_type
 * @property string $error_message
 * @property int $validate
 * @property int $box
 * @property int $view
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\MprfCase[] $mprf_cases
 * @property \App\Model\Entity\SmsOutgoing[] $sms_outgoings
 */
class SmsIncoming extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
