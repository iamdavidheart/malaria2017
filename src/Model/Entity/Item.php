<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $template_code
 * @property string $source
 * @property \Cake\I18n\Time $report_date
 * @property int $facility_id
 * @property int $sms_incoming_id
 * @property int $user_id
 * @property string $remarks
 * @property float $al_qty
 * @property float $pq_qty
 * @property float $cq_qty
 * @property float $qt_qty
 * @property float $qa_qty
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $status
 *
 * @property \App\Model\Entity\Facility $facility
 * @property \App\Model\Entity\SmsIncoming $sms_incoming
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ItemDetail[] $item_details
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
