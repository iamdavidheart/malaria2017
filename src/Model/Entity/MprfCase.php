<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MprfCase Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $sms_incoming_id
 * @property int $user_id
 * @property string $source
 * @property int $facility_id
 * @property string $template_code
 * @property \Cake\I18n\Time $consult_date
 * @property \Cake\I18n\Time $death_date
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property int $age
 * @property string $weight
 * @property \Cake\I18n\Time $birthdate
 * @property string $gender
 * @property string $lessmonth
 * @property int $pregnant
 * @property string $ip_group
 * @property string $occupation
 * @property string $case_type
 * @property string $region_id
 * @property string $province_id
 * @property string $municipality_id
 * @property string $barangay_id
 * @property string $sitio_id
 * @property \Cake\I18n\Time $blood_exam_date
 * @property \Cake\I18n\Time $blood_exam_result_date
 * @property int $chief_complaint
 * @property string $temprature
 * @property int $c_other_complaint
 * @property string $other_complaint
 * @property bool $microscopy
 * @property bool $rdt
 * @property bool $r_pf
 * @property int $m_pf
 * @property bool $pm
 * @property bool $m_pfpv
 * @property int $r_pfpv
 * @property bool $pfpm
 * @property bool $pvpm
 * @property bool $m_pv
 * @property int $r_pv
 * @property bool $po
 * @property bool $nf
 * @property int $cdx
 * @property bool $pfnf
 * @property string $rdt_no
 * @property string $parasaite_blood
 * @property int $clinical_diagnosis
 * @property \Cake\I18n\Time $disease_start_date
 * @property int $artemether_tab
 * @property int $artemether_qty
 * @property \Cake\I18n\Time $artemether_started
 * @property string $artemether_prep
 * @property int $chloroquine_tab
 * @property int $chloroquine_qty
 * @property \Cake\I18n\Time $chloroquine_started
 * @property string $chloroquine_prep
 * @property int $primaquine_tab
 * @property int $primaquine_qty
 * @property \Cake\I18n\Time $primaquine_started
 * @property string $primaquine_prep
 * @property int $quinine_tab
 * @property int $quinine_qty
 * @property \Cake\I18n\Time $quinine_started
 * @property string $quinine_prep
 * @property int $quinine_ampules_tab
 * @property int $quinine_ampules_qty
 * @property \Cake\I18n\Time $quinine_ampules_started
 * @property string $quinine_ampules_prep
 * @property int $tetracycline_tab
 * @property int $tetracycline_qty
 * @property \Cake\I18n\Time $tetracycline_started
 * @property string $tetracycline_prep
 * @property int $doxycycline_tab
 * @property int $doxycycline_qty
 * @property \Cake\I18n\Time $doxycycline_started
 * @property string $doxycycline_prep
 * @property int $clindamycin_tab
 * @property int $clindamycin_qty
 * @property \Cake\I18n\Time $clindamycin_started
 * @property string $clindamycin_prep
 * @property int $no_medecine
 * @property bool $travel_history
 * @property string $travel_history_place
 * @property bool $history_blood_trans
 * @property int $history_illness
 * @property \Cake\I18n\Time $drug_intake_date
 * @property bool $supervised_initial_intake
 * @property string $hw_first_name
 * @property string $hw_last_name
 * @property string $hw_middle_name
 * @property string $hw_designation
 * @property bool $disposition
 * @property string $referred_to
 * @property string $referral_reason
 * @property string $remarks
 * @property string $province_text
 * @property string $municipality_text
 * @property string $barangay_text
 * @property string $sitio_text
 * @property int $validated
 * @property string $status
 *
 * @property \App\Model\Entity\SmsIncoming $sms_incoming
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Facility $facility
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Province $province
 * @property \App\Model\Entity\Municipality $municipality
 * @property \App\Model\Entity\Barangay $barangay
 * @property \App\Model\Entity\Sitio $sitio
 */
class MprfCase extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
