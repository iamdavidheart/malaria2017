<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $role_id
 * @property string $code
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $mobile_no_one
 * @property string $mobile_no_two
 * @property string $mobile_no_three
 * @property string $email
 * @property int $designation_id
 * @property int $facility_id
 * @property string $blocked
 * @property string $status
 * @property string $source
 * @property int $activated
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Designation $designation
 * @property \App\Model\Entity\Facility[] $facilities
 * @property \App\Model\Entity\Barangay[] $barangays
 * @property \App\Model\Entity\CompanyType[] $company_types
 * @property \App\Model\Entity\Distribution[] $distributions
 * @property \App\Model\Entity\ItemDetail[] $item_details
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\MprfCase[] $mprf_cases
 * @property \App\Model\Entity\MprfExcel[] $mprf_excels
 * @property \App\Model\Entity\Network[] $networks
 * @property \App\Model\Entity\Sitio[] $sitios
 * @property \App\Model\Entity\SmsIncoming[] $sms_incomings
 * @property \App\Model\Entity\SmsOutgoing[] $sms_outgoings
 * @property \App\Model\Entity\TemplateNotification[] $template_notifications
 * @property \App\Model\Entity\UserContact[] $user_contacts
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */




    protected function _setPassword($value) {
        return (new DefaultPasswordHasher)->hash($value);
    }


    /**
     * Get the full name of the user. If empty, return the username.
     *
     * @return string
     */
    protected function _getFullName()
    {
        $fullName = trim($this->first_name . ' ' . $this->last_name);
        return (!empty($fullName)) ? $fullName : $this->username;
    }

}
