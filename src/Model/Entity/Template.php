<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Template Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $code
 * @property string $description
 * @property string $sms_format
 * @property int $send_error_msg
 * @property int $send_alert_msg
 * @property int $template_alert_id
 * @property string $format
 * @property string $remarks
 * @property int $status
 * @property int $globe_amount
 * @property string $globe_syntax
 * @property string $globe_send_to
 * @property string $globe_status
 * @property int $smart_amount
 * @property string $smart_syntax
 * @property string $smart_send_to
 * @property string $smart_status
 * @property int $sun_amount
 * @property string $sun_syntax
 * @property string $sun_send_to
 * @property string $sun_status
 * @property int $tm_amount
 * @property string $tm_syntax
 * @property string $tm_send_to
 * @property string $tm_status
 *
 * @property \App\Model\Entity\TemplateAlert $template_alert
 * @property \App\Model\Entity\TemplateNotification[] $template_notifications
 * @property \App\Model\Entity\TemplateReply[] $template_replies
 * @property \App\Model\Entity\TemplateSetting[] $template_settings
 */
class Template extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
