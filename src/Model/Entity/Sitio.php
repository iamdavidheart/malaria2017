<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sitio Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property string $description
 * @property string $barangay_code
 * @property string $municipality_code
 * @property string $province_code
 * @property string $region_code
 * @property int $barangay_id
 * @property int $region_id
 * @property int $province_id
 * @property int $municipality_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Barangay $barangay
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Province $province
 * @property \App\Model\Entity\Municipality $municipality
 * @property \App\Model\Entity\Facility[] $facilities
 * @property \App\Model\Entity\MprfCase[] $mprf_cases
 */
class Sitio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected function _getFullDetails()
    {
        $details = trim($this->description);
        return (!empty($details)) ? $details : 'Empty';
            
    }
    
}
