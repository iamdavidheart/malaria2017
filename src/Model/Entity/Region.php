<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Region Entity
 *
 * @property int $id
 * @property string $psgc_code
 * @property string $description
 * @property string $code
 *
 * @property \App\Model\Entity\Barangay[] $barangays
 * @property \App\Model\Entity\Facility[] $facilities
 * @property \App\Model\Entity\MprfCase[] $mprf_cases
 * @property \App\Model\Entity\Municipality[] $municipalities
 * @property \App\Model\Entity\Province[] $provinces
 * @property \App\Model\Entity\Sitio[] $sitios
 */
class Region extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _getFullDetails()
    {
        $details = trim($this->description);
        return (!empty($details)) ? $details : 'Empty';
            
    }

    
}
