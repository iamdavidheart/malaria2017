<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SmsOutgoing Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $sms_code
 * @property string $group_id
 * @property string $source
 * @property int $sms_incoming_id
 * @property string $receiver_no
 * @property int $receiver_id
 * @property string $sender_no
 * @property int $user_id
 * @property string $message
 * @property string $error
 * @property string $status
 * @property int $box
 * @property string $network
 * @property string $type
 * @property int $view
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\SmsIncoming $sms_incoming
 * @property \App\Model\Entity\Receiver $receiver
 * @property \App\Model\Entity\User $user
 */
class SmsOutgoing extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
