<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Facility Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $facility_category_id
 * @property int $region_id
 * @property int $province_id
 * @property int $municipality_id
 * @property int $barangay_id
 * @property int $sitio_id
 * @property string $description
 * @property string $remarks
 * @property int $user_id
 * @property \Cake\I18n\Time $cut_off_date
 * @property int $status
 *
 * @property \App\Model\Entity\FacilityCategory $facility_category
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Province $province
 * @property \App\Model\Entity\Municipality $municipality
 * @property \App\Model\Entity\Barangay $barangay
 * @property \App\Model\Entity\Sitio $sitio
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\MprfCase[] $mprf_cases
 */
class Facility extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
