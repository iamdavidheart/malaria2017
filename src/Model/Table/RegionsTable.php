<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Regions Model
 *
 * @property \Cake\ORM\Association\HasMany $Barangays
 * @property \Cake\ORM\Association\HasMany $Facilities
 * @property \Cake\ORM\Association\HasMany $MprfCases
 * @property \Cake\ORM\Association\HasMany $Municipalities
 * @property \Cake\ORM\Association\HasMany $Provinces
 * @property \Cake\ORM\Association\HasMany $Sitios
 *
 * @method \App\Model\Entity\Region get($primaryKey, $options = [])
 * @method \App\Model\Entity\Region newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Region[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Region|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Region patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Region[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Region findOrCreate($search, callable $callback = null, $options = [])
 */
class RegionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('regions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Barangays', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Facilities', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('MprfCases', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Municipalities', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Provinces', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Sitios', [
            'foreignKey' => 'region_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('psgc_code');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('code');

        return $validator;
    }
}
