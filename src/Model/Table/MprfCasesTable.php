<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MprfCases Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SmsIncomings
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Facilities
 * @property \Cake\ORM\Association\BelongsTo $Regions
 * @property \Cake\ORM\Association\BelongsTo $Provinces
 * @property \Cake\ORM\Association\BelongsTo $Municipalities
 * @property \Cake\ORM\Association\BelongsTo $Barangays
 * @property \Cake\ORM\Association\BelongsTo $Sitios
 *
 * @method \App\Model\Entity\MprfCase get($primaryKey, $options = [])
 * @method \App\Model\Entity\MprfCase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MprfCase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MprfCase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MprfCase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MprfCase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MprfCase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MprfCasesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('mprf_cases');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SmsIncomings', [
            'foreignKey' => 'sms_incoming_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Facilities', [
            'foreignKey' => 'facility_id'
        ]);
        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
        ]);
        $this->belongsTo('Provinces', [
            'foreignKey' => 'province_id'
        ]);
        $this->belongsTo('Municipalities', [
            'foreignKey' => 'municipality_id'
        ]);
        $this->belongsTo('Barangays', [
            'foreignKey' => 'barangay_id'
        ]);
        $this->belongsTo('Sitios', [
            'foreignKey' => 'sitio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('source', 'create')
            ->notEmpty('source');

        $validator
            ->allowEmpty('template_code');

        $validator
            ->date('consult_date')
            ->allowEmpty('consult_date');

        $validator
            ->date('death_date')
            ->allowEmpty('death_date');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->allowEmpty('middle_name');

        $validator
            ->integer('age')
            ->allowEmpty('age');

        $validator
            ->allowEmpty('weight');

        $validator
            ->dateTime('birthdate')
            ->allowEmpty('birthdate');

        $validator
            ->allowEmpty('gender');

        $validator
            ->allowEmpty('lessmonth');

        $validator
            ->integer('pregnant')
            ->allowEmpty('pregnant');

        $validator
            ->allowEmpty('ip_group');

        $validator
            ->allowEmpty('occupation');

        $validator
            ->allowEmpty('case_type');

        $validator
            ->dateTime('blood_exam_date')
            ->allowEmpty('blood_exam_date');

        $validator
            ->dateTime('blood_exam_result_date')
            ->allowEmpty('blood_exam_result_date');

        $validator
            ->integer('chief_complaint')
            ->allowEmpty('chief_complaint');

        $validator
            ->allowEmpty('temprature');

        $validator
            ->integer('c_other_complaint')
            ->allowEmpty('c_other_complaint');

        $validator
            ->allowEmpty('other_complaint');

        $validator
            ->boolean('microscopy')
            ->allowEmpty('microscopy');

        $validator
            ->boolean('rdt')
            ->allowEmpty('rdt');

        $validator
            ->boolean('r_pf')
            ->allowEmpty('r_pf');

        $validator
            ->integer('m_pf')
            ->allowEmpty('m_pf');

        $validator
            ->boolean('pm')
            ->allowEmpty('pm');

        $validator
            ->boolean('m_pfpv')
            ->allowEmpty('m_pfpv');

        $validator
            ->integer('r_pfpv')
            ->allowEmpty('r_pfpv');

        $validator
            ->boolean('pfpm')
            ->allowEmpty('pfpm');

        $validator
            ->boolean('pvpm')
            ->allowEmpty('pvpm');

        $validator
            ->boolean('m_pv')
            ->allowEmpty('m_pv');

        $validator
            ->integer('r_pv')
            ->allowEmpty('r_pv');

        $validator
            ->boolean('po')
            ->allowEmpty('po');

        $validator
            ->boolean('nf')
            ->allowEmpty('nf');

        $validator
            ->integer('cdx')
            ->allowEmpty('cdx');

        $validator
            ->boolean('pfnf')
            ->allowEmpty('pfnf');

        $validator
            ->allowEmpty('rdt_no');

        $validator
            ->allowEmpty('parasaite_blood');

        $validator
            ->integer('clinical_diagnosis')
            ->allowEmpty('clinical_diagnosis');

        $validator
            ->dateTime('disease_start_date')
            ->allowEmpty('disease_start_date');

        $validator
            ->integer('artemether_tab')
            ->allowEmpty('artemether_tab');

        $validator
            ->integer('artemether_qty')
            ->allowEmpty('artemether_qty');

        $validator
            ->dateTime('artemether_started')
            ->allowEmpty('artemether_started');

        $validator
            ->allowEmpty('artemether_prep');

        $validator
            ->integer('chloroquine_tab')
            ->allowEmpty('chloroquine_tab');

        $validator
            ->integer('chloroquine_qty')
            ->allowEmpty('chloroquine_qty');

        $validator
            ->dateTime('chloroquine_started')
            ->allowEmpty('chloroquine_started');

        $validator
            ->allowEmpty('chloroquine_prep');

        $validator
            ->integer('primaquine_tab')
            ->allowEmpty('primaquine_tab');

        $validator
            ->integer('primaquine_qty')
            ->allowEmpty('primaquine_qty');

        $validator
            ->dateTime('primaquine_started')
            ->allowEmpty('primaquine_started');

        $validator
            ->allowEmpty('primaquine_prep');

        $validator
            ->integer('quinine_tab')
            ->allowEmpty('quinine_tab');

        $validator
            ->integer('quinine_qty')
            ->allowEmpty('quinine_qty');

        $validator
            ->dateTime('quinine_started')
            ->allowEmpty('quinine_started');

        $validator
            ->allowEmpty('quinine_prep');

        $validator
            ->integer('quinine_ampules_tab')
            ->allowEmpty('quinine_ampules_tab');

        $validator
            ->integer('quinine_ampules_qty')
            ->allowEmpty('quinine_ampules_qty');

        $validator
            ->dateTime('quinine_ampules_started')
            ->allowEmpty('quinine_ampules_started');

        $validator
            ->allowEmpty('quinine_ampules_prep');

        $validator
            ->integer('tetracycline_tab')
            ->allowEmpty('tetracycline_tab');

        $validator
            ->integer('tetracycline_qty')
            ->allowEmpty('tetracycline_qty');

        $validator
            ->dateTime('tetracycline_started')
            ->allowEmpty('tetracycline_started');

        $validator
            ->allowEmpty('tetracycline_prep');

        $validator
            ->integer('doxycycline_tab')
            ->allowEmpty('doxycycline_tab');

        $validator
            ->integer('doxycycline_qty')
            ->allowEmpty('doxycycline_qty');

        $validator
            ->dateTime('doxycycline_started')
            ->allowEmpty('doxycycline_started');

        $validator
            ->allowEmpty('doxycycline_prep');

        $validator
            ->integer('clindamycin_tab')
            ->allowEmpty('clindamycin_tab');

        $validator
            ->integer('clindamycin_qty')
            ->allowEmpty('clindamycin_qty');

        $validator
            ->dateTime('clindamycin_started')
            ->allowEmpty('clindamycin_started');

        $validator
            ->allowEmpty('clindamycin_prep');

        $validator
            ->integer('no_medecine')
            ->allowEmpty('no_medecine');

        $validator
            ->boolean('travel_history')
            ->allowEmpty('travel_history');

        $validator
            ->allowEmpty('travel_history_place');

        $validator
            ->boolean('history_blood_trans')
            ->allowEmpty('history_blood_trans');

        $validator
            ->integer('history_illness')
            ->allowEmpty('history_illness');

        $validator
            ->dateTime('drug_intake_date')
            ->allowEmpty('drug_intake_date');

        $validator
            ->boolean('supervised_initial_intake')
            ->allowEmpty('supervised_initial_intake');

        $validator
            ->allowEmpty('hw_first_name');

        $validator
            ->allowEmpty('hw_last_name');

        $validator
            ->allowEmpty('hw_middle_name');

        $validator
            ->allowEmpty('hw_designation');

        $validator
            ->boolean('disposition')
            ->allowEmpty('disposition');

        $validator
            ->allowEmpty('referred_to');

        $validator
            ->allowEmpty('referral_reason');

        $validator
            ->allowEmpty('remarks');

        $validator
            ->allowEmpty('province_text');

        $validator
            ->allowEmpty('municipality_text');

        $validator
            ->allowEmpty('barangay_text');

        $validator
            ->allowEmpty('sitio_text');

        $validator
            ->integer('validated')
            ->allowEmpty('validated');

        $validator
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sms_incoming_id'], 'SmsIncomings'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['facility_id'], 'Facilities'));
        $rules->add($rules->existsIn(['region_id'], 'Regions'));
        $rules->add($rules->existsIn(['province_id'], 'Provinces'));
        $rules->add($rules->existsIn(['municipality_id'], 'Municipalities'));
        $rules->add($rules->existsIn(['barangay_id'], 'Barangays'));
        $rules->add($rules->existsIn(['sitio_id'], 'Sitios'));

        return $rules;
    }
}
