<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Barangays Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Regions
 * @property \Cake\ORM\Association\BelongsTo $Provinces
 * @property \Cake\ORM\Association\BelongsTo $Municipalities
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $ModifiedUsers
 * @property \Cake\ORM\Association\HasMany $Facilities
 * @property \Cake\ORM\Association\HasMany $MprfCases
 * @property \Cake\ORM\Association\HasMany $Sitios
 *
 * @method \App\Model\Entity\Barangay get($primaryKey, $options = [])
 * @method \App\Model\Entity\Barangay newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Barangay[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Barangay|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Barangay patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Barangay[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Barangay findOrCreate($search, callable $callback = null, $options = [])
 */
class BarangaysTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('barangays');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
        ]);
        $this->belongsTo('Provinces', [
            'foreignKey' => 'province_id'
        ]);
        $this->belongsTo('Municipalities', [
            'foreignKey' => 'municipality_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_user_id'
        ]);
        $this->hasMany('Facilities', [
            'foreignKey' => 'barangay_id'
        ]);
        $this->hasMany('MprfCases', [
            'foreignKey' => 'barangay_id'
        ]);
        $this->hasMany('Sitios', [
            'foreignKey' => 'barangay_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('barangay_code');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('region_code');

        $validator
            ->allowEmpty('province_code');

        $validator
            ->allowEmpty('municipality_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['region_id'], 'Regions'));
        $rules->add($rules->existsIn(['province_id'], 'Provinces'));
        $rules->add($rules->existsIn(['municipality_id'], 'Municipalities'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['modified_user_id'], 'ModifiedUsers'));

        return $rules;
    }
}
