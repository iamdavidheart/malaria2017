<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsOutgoings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $SmsIncomings
 * @property \Cake\ORM\Association\BelongsTo $Receivers
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\SmsOutgoing get($primaryKey, $options = [])
 * @method \App\Model\Entity\SmsOutgoing newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SmsOutgoing[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SmsOutgoing|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsOutgoing patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SmsOutgoing[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SmsOutgoing findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SmsOutgoingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sms_outgoings');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->belongsTo('Groups', [
        //     'foreignKey' => 'group_id'
        // ]);
        $this->belongsTo('SmsIncomings', [
            'foreignKey' => 'sms_incoming_id'
        ]);
        // $this->belongsTo('Receivers', [
        //     'foreignKey' => 'receiver_id'
        // ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('ReceiverUsers', [
            'className' => 'Users',
            'foreignKey' => 'receiver_id',
            'propertyName' => 'receiver_user'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('sms_code');

        $validator
            ->allowEmpty('source');

        $validator
            ->requirePresence('receiver_no', 'create')
            ->notEmpty('receiver_no');

        $validator
            ->allowEmpty('sender_no');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->allowEmpty('error');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('box')
            ->requirePresence('box', 'create')
            ->notEmpty('box');

        $validator
            ->allowEmpty('network');

        $validator
            ->allowEmpty('type');

        $validator
            ->integer('view')
            ->requirePresence('view', 'create')
            ->notEmpty('view');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['group_id'], 'Groups'));
        $rules->add($rules->existsIn(['sms_incoming_id'], 'SmsIncomings'));
        // $rules->add($rules->existsIn(['receiver_id'], 'Receivers'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
