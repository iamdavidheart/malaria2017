<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Templates Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TemplateAlerts
 * @property \Cake\ORM\Association\HasMany $TemplateNotifications
 * @property \Cake\ORM\Association\HasMany $TemplateReplies
 * @property \Cake\ORM\Association\HasMany $TemplateSettings
 *
 * @method \App\Model\Entity\Template get($primaryKey, $options = [])
 * @method \App\Model\Entity\Template newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Template[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Template|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Template patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Template[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Template findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TemplatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('templates');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TemplateAlerts', [
            'foreignKey' => 'template_alert_id'
        ]);
        $this->hasMany('TemplateNotifications', [
            'foreignKey' => 'template_id'
        ]);
        $this->hasMany('TemplateReplies', [
            'foreignKey' => 'template_id'
        ]);
        $this->hasMany('TemplateSettings', [
            'foreignKey' => 'template_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->requirePresence('sms_format', 'create')
            ->notEmpty('sms_format');

        $validator
            ->integer('send_error_msg')
            ->allowEmpty('send_error_msg');

        $validator
            ->integer('send_alert_msg')
            ->requirePresence('send_alert_msg', 'create')
            ->notEmpty('send_alert_msg');

        $validator
            ->allowEmpty('format');

        $validator
            ->allowEmpty('remarks');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->integer('globe_amount')
            ->allowEmpty('globe_amount');

        $validator
            ->allowEmpty('globe_syntax');

        $validator
            ->allowEmpty('globe_send_to');

        $validator
            ->allowEmpty('globe_status');

        $validator
            ->integer('smart_amount')
            ->allowEmpty('smart_amount');

        $validator
            ->allowEmpty('smart_syntax');

        $validator
            ->allowEmpty('smart_send_to');

        $validator
            ->allowEmpty('smart_status');

        $validator
            ->integer('sun_amount')
            ->allowEmpty('sun_amount');

        $validator
            ->allowEmpty('sun_syntax');

        $validator
            ->allowEmpty('sun_send_to');

        $validator
            ->allowEmpty('sun_status');

        $validator
            ->integer('tm_amount')
            ->allowEmpty('tm_amount');

        $validator
            ->allowEmpty('tm_syntax');

        $validator
            ->allowEmpty('tm_send_to');

        $validator
            ->allowEmpty('tm_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['template_alert_id'], 'TemplateAlerts'));

        return $rules;
    }
}
