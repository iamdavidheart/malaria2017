<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 * @property \Cake\ORM\Association\BelongsTo $Designations
 * @property \Cake\ORM\Association\BelongsTo $Facilities
 * @property \Cake\ORM\Association\HasMany $Barangays
 * @property \Cake\ORM\Association\HasMany $CompanyTypes
 * @property \Cake\ORM\Association\HasMany $Distributions
 * @property \Cake\ORM\Association\HasMany $Facilities
 * @property \Cake\ORM\Association\HasMany $ItemDetails
 * @property \Cake\ORM\Association\HasMany $Items
 * @property \Cake\ORM\Association\HasMany $MprfCases
 * @property \Cake\ORM\Association\HasMany $MprfExcels
 * @property \Cake\ORM\Association\HasMany $Networks
 * @property \Cake\ORM\Association\HasMany $Sitios
 * @property \Cake\ORM\Association\HasMany $SmsIncomings
 * @property \Cake\ORM\Association\HasMany $SmsOutgoings
 * @property \Cake\ORM\Association\HasMany $TemplateNotifications
 * @property \Cake\ORM\Association\HasMany $UserContacts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Designations', [
            'foreignKey' => 'designation_id'
        ]);
        $this->belongsTo('Facilities', [
            'foreignKey' => 'facility_id'
        ]);
        $this->hasMany('Barangays', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CompanyTypes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Distributions', [
            'foreignKey' => 'user_id'
        ]);

        
        // $this->hasMany('Facilities', [
        //     'foreignKey' => 'user_id'
        // ]);
        $this->hasMany('ItemDetails', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('MprfCases', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('MprfExcels', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Networks', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Sitios', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('SmsIncomings', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('SmsOutgoings', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TemplateNotifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserContacts', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
       $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->allowEmpty('last_name');

        $validator
            ->allowEmpty('middle_name');

        $validator
            ->allowEmpty('status');

        $validator
            ->notEmpty('code')
            ->add('code', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => __("This code is already used.")
                ]
            ]);


        return $validator;
    }

    public function validationPassword(Validator $validator)
    {
        return $validator
            ->notEmpty('password', __("You must specify your new password."))
            ->add('password', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 6, 20],
                    'message' => __("Your password (confirmation) must be between {0} and {1} characters.", 6, 20)
                ]
            ])
            ->notEmpty('confirm_password', __("You must specify your password (confirmation)."))
            ->add('confirm_password', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 6, 20],
                    'message' => __("Your password (confirmation) must be between {0} and {1} characters.", 6, 20)
                ],
                'equalToPassword' => [
                    'rule' => function ($value, $context) {
                            return $value === $context['data']['password'];
                    },
                    'message' => __("Your password confirm must match with your new password")
                ]
            ]);
    }


    

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        // $rules->add($rules->existsIn(['designation_id'], 'Designations'));
        // $rules->add($rules->existsIn(['facility_id'], 'Facilities'));

        return $rules;
    }
}
