<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsIncomings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Items
 * @property \Cake\ORM\Association\HasMany $MprfCases
 * @property \Cake\ORM\Association\HasMany $SmsOutgoings
 *
 * @method \App\Model\Entity\SmsIncoming get($primaryKey, $options = [])
 * @method \App\Model\Entity\SmsIncoming newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SmsIncoming[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SmsIncoming|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsIncoming patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SmsIncoming[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SmsIncoming findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SmsIncomingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sms_incomings');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'sms_incoming_id'
        ]);
        $this->hasMany('MprfCases', [
            'foreignKey' => 'sms_incoming_id'
        ]);
        $this->hasMany('SmsOutgoings', [
            'foreignKey' => 'sms_incoming_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('template_code');

        $validator
            ->allowEmpty('sms_code');

        $validator
            ->requirePresence('sender_no', 'create')
            ->notEmpty('sender_no');

        $validator
            ->allowEmpty('phone_no');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->allowEmpty('message_type');

        $validator
            ->allowEmpty('error_message');

        $validator
            ->integer('validate')
            ->allowEmpty('validate');

        $validator
            ->integer('box')
            ->requirePresence('box', 'create')
            ->notEmpty('box');

        $validator
            ->integer('view')
            ->requirePresence('view', 'create')
            ->notEmpty('view');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
