<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Provinces Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Regions
 * @property \Cake\ORM\Association\HasMany $Barangays
 * @property \Cake\ORM\Association\HasMany $Facilities
 * @property \Cake\ORM\Association\HasMany $MprfCases
 * @property \Cake\ORM\Association\HasMany $Municipalities
 * @property \Cake\ORM\Association\HasMany $Sitios
 *
 * @method \App\Model\Entity\Province get($primaryKey, $options = [])
 * @method \App\Model\Entity\Province newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Province[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Province|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Province patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Province[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Province findOrCreate($search, callable $callback = null, $options = [])
 */
class ProvincesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('provinces');
        $this->displayField('id');
        $this->primaryKey('id');

        // $this->belongsTo('Regions', [
        //     'foreignKey' => 'region_id'
        // ]);
        
        $this->belongsTo('Regions', [
            'foreignKey' => 'region_code',
            'propertyName' => 'region'
        ]);



        $this->hasMany('Barangays', [
            'foreignKey' => 'province_id'
        ]);
        $this->hasMany('Facilities', [
            'foreignKey' => 'province_id'
        ]);
        $this->hasMany('MprfCases', [
            'foreignKey' => 'province_id'
        ]);
        $this->hasMany('Municipalities', [
            'foreignKey' => 'province_id'
        ]);
        $this->hasMany('Sitios', [
            'foreignKey' => 'province_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('psgc_code');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('region_code');

        $validator
            ->allowEmpty('province_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['region_id'], 'Regions'));

        return $rules;
    }
}
