<?php
namespace App\View\Helper;

use Cake\Network\Request;
use Cake\Routing\Router;
use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class CountHelper extends Helper
{


    /**
     * Helpers used.
     *
     * @var array
     */
    public $helpers = ['Html'];

    /**
     * Construct method.
     *
     * @param \Cake\View\View $view The view that was fired.
     * @param array $config The config passed to the class.
     */
    public function __construct(View $view, $config = [])
    {
        parent::__construct($view, $config);

    }


    public function inbox()
    {
        $this->SmsIncomings = TableRegistry::get('SmsIncomings');
        return  $this->SmsIncomings
                ->find()
                ->where([
                    'SmsIncomings.status' => 'processed'
                ])->count();

    }


    public function unreadinbox()
    {
        $this->SmsIncomings = TableRegistry::get('SmsIncomings');
        return  $this->SmsIncomings
                ->find()
                ->where([
                    'SmsIncomings.status' => 'processed',
                    'SmsIncomings.view' => 0
                ])->count();

    }

    public function spam()
    {
        $this->SmsIncomings = TableRegistry::get('SmsIncomings');
        return  $this->SmsIncomings
                ->find()
                ->where([
                    'SmsIncomings.status' => 'spam'
                ])->count();

    }

    public function unreadspam()
    {
        $this->SmsIncomings = TableRegistry::get('SmsIncomings');
        return  $this->SmsIncomings
                ->find()
                ->where([
                    'SmsIncomings.status' => 'spam',
                    'SmsIncomings.view' => 0
                ])->count();

    }


    public function outbox()
    {
        $this->SmsOutgoings = TableRegistry::get('SmsOutgoings');
        return  $this->SmsOutgoings
                ->find()
                ->where([
                    'SmsOutgoings.status' => 'pending','SmsOutgoings.box' => 10
                ])
                ->orWhere(['SmsOutgoings.status' => 'sending'])
                ->count();
    }

    public function unreadoutbox()
    {
        $this->SmsOutgoings = TableRegistry::get('SmsOutgoings');
        return  $this->SmsOutgoings
                ->find()
                ->where([
                    'SmsOutgoings.status' => 'pending','SmsOutgoings.box' => 10,'SmsOutgoings.view' => 0
                ])
                ->orWhere(['SmsOutgoings.status' => 'sending'])
                ->count();
    }


    public function sent()
    {
        $this->SmsOutgoings = TableRegistry::get('SmsOutgoings');
        return  $this->SmsOutgoings
                ->find()
                ->where([
                    'SmsOutgoings.status' => 'sent','SmsOutgoings.box' => 10
                ])->count();

    }

    public function unreadsent()
    {
        $this->SmsOutgoings = TableRegistry::get('SmsOutgoings');
        return  $this->SmsOutgoings
                ->find()
                ->where([
                    'SmsOutgoings.status' => 'sent',
                    'SmsOutgoings.view' => 0,
                    'SmsOutgoings.box' => 10
                ])->count();

    }


}