<?php
$cakeDescription = 'Cakeswatch Theme';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?= $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cakeswatch is adaptation of Bootswatch for CakePHP themes">
    <title>
        <?= $cakeDescription; ?>:
        <?= $this->fetch('title'); ?>
    </title>

        <!-- Mobile Meta -->
       
        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>


    <?php


        echo $this->Html->css('../html/bootstrap/css/bootstrap.css'); 
        echo $this->Html->css('../html/fonts/font-awesome/css/font-awesome.css'); 
        echo $this->Html->css('../html/fonts/fontello/css/fontello.css'); 

        //
        echo $this->Html->css('../html/plugins/magnific-popup/magnific-popup.css'); 
        echo $this->Html->css('../html/plugins/rs-plugin/css/settings.css'); 
        echo $this->Html->css('../html/plugins/hover/hover-min.css'); 

        //
        echo $this->Html->css('../html/css/style.css'); 
        echo $this->Html->css('../html/css/typography-default.css'); 

        echo $this->Html->css('../html/style-switcher/style-switcher.css'); 
        echo $this->Html->css('../html/css/custom.css'); 

        echo $this->Html->css('../html/css/skins/bluex.css'); 

        echo $this->fetch('meta');
        echo $this->fetch('scriptCss');

    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="no-trans front-page transparent-header  ">

         <?= $this->element('top'); ?>


            <section class="container">
            <?php echo $this->Flash->render(); ?>
            </section>
            <?= $this->fetch('content'); ?>

        


<footer id="footer" class="clearfix">


                <!-- .subfooter start -->
                <!-- ================ -->
                <div class="subfooter">
                    <div class="container">
                        <div class="subfooter-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center">Copyrignt Movements Againts Malaria</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .subfooter end -->

            </footer>





        <?php 

        echo $this->Html->script('../html/plugins/jquery.min.js'); 
        echo $this->Html->script('../html/bootstrap/js/bootstrap.min.js'); 
        echo $this->Html->script('../html/plugins/modernizr.js'); 
        echo $this->Html->script('../html/plugins/rs-plugin/js/jquery.themepunch.tools.min.js'); 
        echo $this->Html->script('../html/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js'); 
        echo $this->Html->script('../html/plugins/isotope/isotope.pkgd.min.js'); 
        echo $this->Html->script('../html/plugins/magnific-popup/jquery.magnific-popup.min.js'); 
        echo $this->Html->script('../html/plugins/waypoints/jquery.waypoints.min.js'); 
        echo $this->Html->script('../html/plugins/jquery.countTo.js'); 


        echo $this->Html->script('../html/plugins/jquery.parallax-1.1.3.js');
        echo $this->Html->script('../html/plugins/vide/jquery.vide.js');
        echo $this->Html->script('../html/plugins/jquery.browser.js');

        echo $this->Html->script('../html/plugins/SmoothScroll.js');
        echo $this->Html->script('../html/js/template.js');
        echo $this->Html->script('../html/js/custom.js');

        ?>

        <?= $this->fetch('scriptBottom') ?>

        <?php
        //echo $this->Html->script('../luvida/scripts/luna.js'); 
        
        ?>
        <?= $this->fetch('scriptFooter') ?>



  </body>
</html>



