    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
    
    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">
    
      <!-- header-container start -->
      <div class="header-container">
        
        
        <!-- header-top start -->
        <!-- classes:  -->
        <!-- "dark": dark version of header top e.g. class="header-top dark" -->
        <!-- "colored": colored version of header top e.g. class="header-top colored" -->
        <!-- ================ -->
        <div class="header-top colored ">
          <div class="container">
            <div class="row">
              <div class="col-xs-3 col-sm-6 col-md-9">
                <!-- header-top-first start -->
                <!-- ================ -->
                <div class="header-top-first clearfix">
                </div>
                <!-- header-top-first end -->
              </div>
              <div class="col-xs-9 col-sm-6 col-md-3 ">

                <!-- header-top-second start -->
                <!-- ================ -->
                <div id="header-top-second"  class="clearfix">

                  <!-- header top dropdowns start -->
                  <!-- ================ -->
                  <div class="header-top-dropdown text-right">


                                       <?php if ($this->request->session()->read('Auth.User')){ 
                                            $id = $this->request->session()->read('Auth.User.id');
                                            $name = $this->request->session()->read('Auth.User.code'); ?>

                                              <div >
                                        <a href="<?= $this->Url->build(['controller' => 'users', 'action' => 'edit', $id]) ?>"  class="btn btn-default btn-sm" >
                                           <i class="fa fa-user pr-10"></i> <?= $name ?>
                                        </a>
                                                <a href="<?= $this->Url->build(['controller' => 'users', 'action' => 'logout']) ?>"  class="btn btn-default btn-sm" >
                                           <i class="fa fa-sign-out pr-10"></i> Logout
                                        </a>



                                    
                                                </div>
                                        <?php }else{ ?>
                                          <div class="btn-group dropdown ">
                                            <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown"><i class="fa fa-lock pr-10"></i> Login</button>
                                            <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                              <li>
                                       
                                                <?= $this->Form->create('Users', ['id' => 'loginForm', 'class' => 'login-form margin-clear']) ?>
<?php
// echo $this->Form->create('Users', [
//     'url' => ['controller' => 'users', 'action' => 'login'], ['id' => 'loginForm', 'class' => 'login-form margin-clear']
// ]);
?>



                                                  <div class="form-group has-feedback">
                                                    <label class="control-label">Username</label>
                                                    <input type="text" name="code" class="form-control" placeholder="">
                                                    <i class="fa fa-user form-control-feedback"></i>
                                                  </div>
                                                  <div class="form-group has-feedback">
                                                    <label class="control-label">Password</label>
                                                    <input type="password" name="password" class="form-control" placeholder="">
                                                    <i class="fa fa-lock form-control-feedback"></i>
                                                  </div>
                                                  <button type="submit" class="btn btn-gray btn-sm">Log In</button>
                                                    <?php // $this->Flash->render(); ?>
                         
                                                </form>
                                              </li>
                                            </ul>
                                          </div>
                                        <?php } ?>







              <!--       <div class="btn-group">
                      <a href="page-signup.html" class="btn btn-default btn-sm"><i class="fa fa-user pr-10"></i> Sign Up</a>
                    </div> -->
         
                  </div>
                  <!--  header top dropdowns end -->
                </div>
                <!-- header-top-second end -->
              </div>
            </div>
          </div>
        </div>
        <!-- header-top end -->
        
        <!-- header start -->
        <!-- classes:  -->
        <!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
        <!-- "dark": dark version of header e.g. class="header dark clearfix" -->
        <!-- "full-width": mandatory class for the full-width menu layout -->
        <!-- "centered": mandatory class for the centered logo layout -->
        <!-- ================ --> 
        <header class="header  fixed    clearfix">
          
          <div class="container">
            <div class="row">
              <div class="col-md-3 ">
                <!-- header-left start -->
                <!-- ================ -->
                <div class="header-left clearfix">
                  
                  <!-- header dropdown buttons -->
           <!--        <div class="header-dropdown-buttons visible-xs">
                    <div class="btn-group dropdown">
                      <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
                      <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                        <li>
                          <form role="search" class="search-box margin-clear">
                            <div class="form-group has-feedback">
                              <input type="text" class="form-control" placeholder="Search">
                              <i class="icon-search form-control-feedback"></i>
                            </div>
                          </form>
                        </li>
                      </ul>
                    </div>
           
                  </div> -->
                  <!-- header dropdown buttons end-->
                  
                  <!-- logo -->
                  <div id="logo" class="logo">
                    <a href="<?= $this->Url->build(['controller' => 'pages']) ?>"><img id="logo_img" src="" alt="MALARIA TEXT REPORT SYSTEM"></a>
                  </div>

                  <!-- name-and-slogan -->
                  <div class="site-slogan">
                    MALARIA INITIATIVES
                  </div>

                </div>
                <!-- header-left end -->

              </div>
              <div class="col-md-9">
          
                <!-- header-right start -->
                <!-- ================ -->
                <div class="header-right clearfix">
                  
                <!-- main-navigation start -->
                <!-- classes: -->
                <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                <!-- ================ -->
                <div class="main-navigation  animated with-dropdown-buttons">

                  <!-- navbar start -->
                  <!-- ================ -->
                  <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">

                      <!-- Toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        
                      </div>

                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <!-- main-menu -->
                        <ul class="nav navbar-nav ">

                              <li class="active"><a href="<?= $this->Url->build(['controller' => 'pages']) ?>">Home</a></li>
                     

                          <?php if($this->request->session()->read('Auth.User')){ ?>

                          <li class="dropdown ">
                            <a href="#transcations" class="dropdown-toggle" data-toggle="dropdown">Transactions</a>
                            <ul class="dropdown-menu">
                               <?php if (in_array($this->request->session()->read('Auth.User.role_id'), [ 1 ])){ ?>
                               <li ><a href="<?= $this->Url->build(['controller' => 'sms-outgoings', 'action' => 'inbox']) ?>">TEXT Reports</a></li>
                               <?php } ?>
                              <li ><a href="<?= $this->Url->build(['controller' => 'mprf-cases']) ?>">MPRF Cases</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'items']) ?>">Stock Level</a></li>
               
                            </ul>
                          </li>
                           <?php if (in_array($this->request->session()->read('Auth.User.role_id'), [ 1 ])){ ?>
                          <li><a href="<?= $this->Url->build(['controller' => 'share-loads', 'action' => 'index']) ?>">Share-A-Load</a></li>
                          <li><a href="<?= $this->Url->build(['controller' => 'sms-blast', 'action' => 'index']) ?>">SMS-Blast</a></li>
                           <?php } ?>

                          <li><a href="#reports">Reports</a></li>
                           <?php if (in_array($this->request->session()->read('Auth.User.role_id'), [ 1 ])){ ?>
                          <li class="dropdown ">
                            <a href="#settings" class="dropdown-toggle" data-toggle="dropdown">Settings</a>
                            <ul class="dropdown-menu">
                              <li ><a href="<?= $this->Url->build(['controller' => 'regions']) ?>" >Regions</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'provinces']) ?>" >Provinces</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'municipalities']) ?>" >Municipalities</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'barangays']) ?>" >Barangays</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'sitios']) ?>" >Sitios</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'facilities']) ?>" >Facilities</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'designations']) ?>" >Designation</a></li>
                              <li ><a href="<?= $this->Url->build(['controller' => 'templates']) ?>">Templates</a></li>
                              <li >
                                <a href="<?= $this->Url->build(['controller' => 'users']) ?>">
                                  Users
                                </a>
                              </li>
               
                            </ul>
                          </li>
                          <?php } ?>

                            <?php } ?>

                
                        </ul>
                        <!-- main-menu end -->
       
                      </div>

                    </div>
                  </nav>
                  <!-- navbar end -->

                </div>
                <!-- main-navigation end -->
                </div>
                <!-- header-right end -->
          
              </div>
            </div>
          </div>
          
        </header>
        <!-- header end -->
      </div>

      </div>