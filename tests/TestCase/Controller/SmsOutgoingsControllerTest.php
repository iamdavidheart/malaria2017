<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SmsOutgoingsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SmsOutgoingsController Test Case
 */
class SmsOutgoingsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sms_outgoings',
        'app.groups',
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.facility_categories',
        'app.regions',
        'app.barangays',
        'app.provinces',
        'app.mprf_cases',
        'app.municipalities',
        'app.sitios',
        'app.modified_users',
        'app.items',
        'app.item_details',
        'app.company_types',
        'app.distributions',
        'app.mprf_excels',
        'app.networks',
        'app.template_notifications',
        'app.user_contacts',
        'app.receivers'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
