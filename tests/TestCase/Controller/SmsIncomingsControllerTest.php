<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SmsIncomingsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SmsIncomingsController Test Case
 */
class SmsIncomingsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.barangays',
        'app.regions',
        'app.mprf_cases',
        'app.provinces',
        'app.municipalities',
        'app.sitios',
        'app.modified_users',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_excels',
        'app.networks',
        'app.sms_outgoings',
        'app.template_notifications',
        'app.user_contacts'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
