<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UserContactsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UserContactsController Test Case
 */
class UserContactsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_contacts',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.barangays',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_cases',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.sms_incomings',
        'app.sms_outgoings',
        'app.template_notifications'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
