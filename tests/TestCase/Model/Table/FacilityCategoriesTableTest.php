<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FacilityCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FacilityCategoriesTable Test Case
 */
class FacilityCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FacilityCategoriesTable
     */
    public $FacilityCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.facility_categories',
        'app.facilities',
        'app.regions',
        'app.barangays',
        'app.provinces',
        'app.mprf_cases',
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.municipalities',
        'app.sms_outgoings',
        'app.template_notifications',
        'app.user_contacts',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FacilityCategories') ? [] : ['className' => 'App\Model\Table\FacilityCategoriesTable'];
        $this->FacilityCategories = TableRegistry::get('FacilityCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FacilityCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
