<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TemplateNotificationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TemplateNotificationsTable Test Case
 */
class TemplateNotificationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TemplateNotificationsTable
     */
    public $TemplateNotifications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.template_notifications',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.facility_categories',
        'app.regions',
        'app.barangays',
        'app.provinces',
        'app.mprf_cases',
        'app.sms_incomings',
        'app.items',
        'app.item_details',
        'app.sms_outgoings',
        'app.receiver_users',
        'app.company_types',
        'app.distributions',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.municipalities',
        'app.user_contacts',
        'app.modified_users',
        'app.templates',
        'app.template_alerts',
        'app.template_replies',
        'app.template_settings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TemplateNotifications') ? [] : ['className' => 'App\Model\Table\TemplateNotificationsTable'];
        $this->TemplateNotifications = TableRegistry::get('TemplateNotifications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TemplateNotifications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
