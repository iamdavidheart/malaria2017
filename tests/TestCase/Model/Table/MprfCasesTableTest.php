<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MprfCasesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MprfCasesTable Test Case
 */
class MprfCasesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MprfCasesTable
     */
    public $MprfCases;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mprf_cases',
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.barangays',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.sms_outgoings',
        'app.template_notifications',
        'app.user_contacts',
        'app.regions',
        'app.provinces',
        'app.municipalities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MprfCases') ? [] : ['className' => 'App\Model\Table\MprfCasesTable'];
        $this->MprfCases = TableRegistry::get('MprfCases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MprfCases);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
