<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BarangaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BarangaysTable Test Case
 */
class BarangaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BarangaysTable
     */
    public $Barangays;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.barangays',
        'app.regions',
        'app.facilities',
        'app.mprf_cases',
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.sms_outgoings',
        'app.template_notifications',
        'app.user_contacts',
        'app.provinces',
        'app.municipalities',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Barangays') ? [] : ['className' => 'App\Model\Table\BarangaysTable'];
        $this->Barangays = TableRegistry::get('Barangays', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Barangays);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
