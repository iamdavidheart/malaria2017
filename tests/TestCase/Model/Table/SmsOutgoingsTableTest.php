<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmsOutgoingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmsOutgoingsTable Test Case
 */
class SmsOutgoingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SmsOutgoingsTable
     */
    public $SmsOutgoings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sms_outgoings',
        'app.groups',
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.facility_categories',
        'app.regions',
        'app.barangays',
        'app.provinces',
        'app.mprf_cases',
        'app.municipalities',
        'app.sitios',
        'app.modified_users',
        'app.items',
        'app.item_details',
        'app.company_types',
        'app.distributions',
        'app.mprf_excels',
        'app.networks',
        'app.template_notifications',
        'app.user_contacts',
        'app.receivers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SmsOutgoings') ? [] : ['className' => 'App\Model\Table\SmsOutgoingsTable'];
        $this->SmsOutgoings = TableRegistry::get('SmsOutgoings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SmsOutgoings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
