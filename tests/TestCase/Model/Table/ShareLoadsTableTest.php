<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShareLoadsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShareLoadsTable Test Case
 */
class ShareLoadsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ShareLoadsTable
     */
    public $ShareLoads;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.share_loads',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.facility_categories',
        'app.regions',
        'app.barangays',
        'app.provinces',
        'app.mprf_cases',
        'app.sms_incomings',
        'app.items',
        'app.item_details',
        'app.sms_outgoings',
        'app.groups',
        'app.receivers',
        'app.receiver_users',
        'app.company_types',
        'app.distributions',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.municipalities',
        'app.template_notifications',
        'app.user_contacts',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ShareLoads') ? [] : ['className' => 'App\Model\Table\ShareLoadsTable'];
        $this->ShareLoads = TableRegistry::get('ShareLoads', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ShareLoads);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
