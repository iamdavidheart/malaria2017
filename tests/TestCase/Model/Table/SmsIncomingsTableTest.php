<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmsIncomingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmsIncomingsTable Test Case
 */
class SmsIncomingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SmsIncomingsTable
     */
    public $SmsIncomings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sms_incomings',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.barangays',
        'app.regions',
        'app.mprf_cases',
        'app.provinces',
        'app.municipalities',
        'app.sitios',
        'app.modified_users',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_excels',
        'app.networks',
        'app.sms_outgoings',
        'app.template_notifications',
        'app.user_contacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SmsIncomings') ? [] : ['className' => 'App\Model\Table\SmsIncomingsTable'];
        $this->SmsIncomings = TableRegistry::get('SmsIncomings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SmsIncomings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
