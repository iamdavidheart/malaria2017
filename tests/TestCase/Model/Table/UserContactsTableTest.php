<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserContactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserContactsTable Test Case
 */
class UserContactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserContactsTable
     */
    public $UserContacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_contacts',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.barangays',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_cases',
        'app.mprf_excels',
        'app.networks',
        'app.sitios',
        'app.sms_incomings',
        'app.sms_outgoings',
        'app.template_notifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserContacts') ? [] : ['className' => 'App\Model\Table\UserContactsTable'];
        $this->UserContacts = TableRegistry::get('UserContacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserContacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
