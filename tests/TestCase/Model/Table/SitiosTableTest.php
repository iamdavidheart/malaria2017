<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SitiosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SitiosTable Test Case
 */
class SitiosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SitiosTable
     */
    public $Sitios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sitios',
        'app.users',
        'app.roles',
        'app.designations',
        'app.facilities',
        'app.barangays',
        'app.regions',
        'app.mprf_cases',
        'app.sms_incomings',
        'app.provinces',
        'app.municipalities',
        'app.modified_users',
        'app.company_types',
        'app.distributions',
        'app.item_details',
        'app.items',
        'app.mprf_excels',
        'app.networks',
        'app.sms_outgoings',
        'app.template_notifications',
        'app.user_contacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sitios') ? [] : ['className' => 'App\Model\Table\SitiosTable'];
        $this->Sitios = TableRegistry::get('Sitios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sitios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
